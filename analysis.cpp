//
// Created by jl on 06.01.19.
// This file implements a tool which is performed for analysing skeletons.
//

// USE_FORK is bugged on Jonathans system
#define USE_STDTHREAD 1
#define USE_FORK 2

#define MULTIPROCESSING USE_STDTHREAD

#include "Image.h"

#if MULTIPROCESSING == USE_FORK
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#endif

#if MULTIPROCESSING == USE_STDTHREAD
#include <mutex>
#endif



// For any reason my scheduler wants to execute all threads on the same and single core.
// After researching for hours no solution has been found.
// This resulted in an sequential execution of all forked processes and threads which makes no sense.
// Even worse: the child processes of the fork solution did not notify the parent after their exit
// so the parent waited for infinity.
// It is a very strange behaviour and not intended. Maybe I made a mistake which I could not resolve. Maybe
// it is a bug. In any case I would recommend to use this function as a workaround.
int assign_cpu_core(unsigned core_id) {
    core_id = core_id % std::thread::hardware_concurrency();

    cpu_set_t cpuset;
    CPU_ZERO(&cpuset);
    CPU_SET(core_id, &cpuset);

    pthread_t current_thread = pthread_self();
    return pthread_setaffinity_np(current_thread, sizeof(cpu_set_t), &cpuset);
}

#if MULTIPROCESSING == USE_FORK
void compare_images(Image* ref_image, Image* comp_image, eSkeletonExtractionAlgorithm skele_algo) {
#endif
#if MULTIPROCESSING == USE_STDTHREAD
void compare_images(Image* ref_image, Image* comp_image, eSkeletonExtractionAlgorithm skele_algo, std::mutex* cout_mutex_ptr, int core, int* subprocess_count) {
#endif
    assign_cpu_core(core);

    std::string output;

    output += ref_image->get_name() + "\t" + comp_image->get_name() + "\t";

    Skeleton *ref_skele = nullptr;
    Skeleton *comp_skele = nullptr;

    try {
        ref_skele = ref_image->get_skeleton(skele_algo);
        comp_skele = comp_image->get_skeleton(skele_algo);
    } catch (const std::logic_error &e) {
        output += "error\terror\t" + std::string(e.what());

#if MULTIPROCESSING == USE_STDTHREAD
        cout_mutex_ptr->lock();
#endif

        std::cout << output << std::endl;

#if MULTIPROCESSING == USE_STDTHREAD
        *subprocess_count -= 1;
        cout_mutex_ptr->unlock();
#endif
        return;
    }

    double distance_hausdorff = ref_skele->compute_distance(comp_skele, eHausdorff);
    output += std::to_string(distance_hausdorff) + "\t";
    double distance_shapecontext = ref_skele->compute_distance(comp_skele, eShapeContext);
    output += std::to_string(distance_shapecontext) + "\t";

#if MULTIPROCESSING == USE_STDTHREAD
    cout_mutex_ptr->lock();
#endif

    std::cout << output << std::endl;

#if MULTIPROCESSING == USE_STDTHREAD
    *subprocess_count -= 1;
    cout_mutex_ptr->unlock();
#endif
}


int main(int argc, char *argv[]) {
    if(argc != 3) {
        std::cout << "Usage: ." << argv[0] << " <folder to files with \"out_IND_last.png\"-files> <MAX-IDX> " << std::endl;
        std::cout << "Example: ." << argv[0] << " /home/user/rendered-folder 109" << std::endl;
        return EXIT_FAILURE;
    }

    const std::string path = std::string(argv[1]);
    const int amount_of_files = std::stoi(std::string(argv[2]));

    const int CPU_COUNT = std::thread::hardware_concurrency();
    const std::string prefix = "out_";
    const std::string postfix = "_last.png";
    std::vector<Image *> vec;

    for (int i = 0; i <= amount_of_files; i++) {
        vec.emplace_back(new Image(path + prefix + std::to_string(i) + postfix));
    }

    std::cout << "Loaded " << vec.size() << " images from " << path << std::endl;
    std::cout << "Multithreading: " << CPU_COUNT << " subprocesses will spawn." << std::endl;

    std::cout << std::endl;

    const std::vector<std::string> mapping110 = {
            "Jonathan",
            "Jonathan",
            "UKM1",
            "Clemens",
            "Clemens",
            "Clemens",
            "Clemens",
            "Clemens",
            "Clemens",
            "Clemens",
            "„Kronos“",
            "„Kronos“",
            "„Kronos“",
            "„Kronos“",
            "Hidden",
            "UKF1",
            "UKM1",
            "UKF1",
            "Roman",
            "UKM2",
            "UKM3",
            "Hidden",
            "Hidden",
            "Hidden",
            "UKM3",
            "Hidden",
            "Hidden",
            "Hidden",
            "Hidden",
            "Hidden",
            "Hidden",
            "Hidden",
            "Hidden",
            "Hidden",
            "Hidden",
            "Hidden",
            "Hidden",
            "Hidden",
            "UKM2",
            "Jonas",
            "UKM2",
            "UKM2",
            "UKM2",
            "UKM2",
            "UKM2",
            "UKM2",
            "UKM2",
            "UKM2",
            "UKM2",
            "UKM2 or UKM3",
            "UKM2",
            "UKM2",
            "UKM2",
            "UKM2",
            "UKM2",
            "UKM2",
            "UKM2",
            "UKM2",
            "UKM2",
            "UKM2",
            "UKM2",
            "UKM2",
            "UKM2 or UKM3",
            "UKM2",
            "UKM2",
            "UKM2",
            "UKM2",
            "UKM2 or UKM3",
            "UKM2",
            "Jonathan",
            "Jonathan",
            "Max",
            "Jonas",
            "Jonathan",
            "Jonathan",
            "Jonathan",
            "Jonathan",
            "Mady",
            "Mady",
            "Mady",
            "Jonathan",
            "Hidden",
            "Jonathan",
            "UKF2",
            "Stefan",
            "Stefan",
            "UKM4",
            "UKM4",
            "UKM4",
            "UKM4",
            "UKM4",
            "UKM4",
            "UKM4",
            "UKM4",
            "UKM4",
            "UKM4",
            "UKM4",
            "UKM4",
            "UKM4",
            "Johannes",
            "Johannes",
            "Johannes or UKM1",
            "Johannes or UKM1",
            "UKM1",
            "Joshua",
            "Joshua",
            "Joshua",
            "Joshua",
            "Joshua",
            "Joshua"
    };

    if(mapping110.size() != vec.size()) {
        std::cout << "Coding error: mapping110.size() != vec.size()" << std::endl;
        exit(EXIT_FAILURE);
    };

    for (int i = 0; i < mapping110.size(); i++) {
        vec[i]->set_name(mapping110[i] + "(" + std::to_string(i) + ")");

        // precreate skeletons to check memory consumption
        try {
            vec[i]->get_skeleton();
        } catch (const std::logic_error &le) {
            std::cout << "Error: Image " << vec[i]->get_name() << ": " << le.what() << std::endl;
        }
    }

    eSkeletonExtractionAlgorithm skele_algo = eAdaptiveThreshold;

    std::cout << std::endl << "REF_SKELE" << "\t" << "COMP_SKELE" << "\t" << "eHausdorff" << "\t" << "eShapeContext"
              << "\t" << "Errors" << "\t" << std::endl;

#if MULTIPROCESSING == USE_STDTHREAD
    std::queue<std::thread *> tqueue;
    int tcount = 0;
    auto cout_mutex_ptr = new std::mutex();
#endif

    // current running number of subprocesses. Can be decremented.
    int subprocess_count = 0;
    // total started number of subprocesses. May *only* be incremented.
    int subprocess_count_total = 0;

    for (auto ref_image : vec) {
        for (auto comp_image : vec) {

#if MULTIPROCESSING == USE_STDTHREAD
            int assigned_core = subprocess_count_total++ % CPU_COUNT; // get current free core index
            std::thread* newthread = new std::thread(compare_images,
                    ref_image,
                    comp_image,
                    skele_algo,
                    cout_mutex_ptr,
                    assigned_core,
                    &subprocess_count);
            newthread->detach();

            cout_mutex_ptr->lock();
            subprocess_count += 1;
            cout_mutex_ptr->unlock();

            while(subprocess_count >= CPU_COUNT) {
                using namespace std::chrono_literals;
                std::this_thread::sleep_for(1000ms);
            }

#endif

#if MULTIPROCESSING == USE_FORK
            fork_count += 1;
            int pid = fork();

            if (pid == 0) { // child
                compare_skeletons_fork(ref_image, comp_image, skele_algo);
                exit(0);
            } else if (pid > 0) { // parent
                if(fork_count >= CPU_COUNT) {
                    wait(NULL); // wait for any child to terminate
                    fork_count -= 1;
                }
            } else if (pid < 0) {
                std::cout << "Error on fork(). Exiting." << std::endl;
                exit(EXIT_FAILURE);
            }
#endif
        } // endloop
    } // endloop

    std::cout << "Calculation finished. Exiting." << std::endl;

#if MULTIPROCESSING == USE_STDTHREAD
    delete cout_mutex_ptr;
#endif

    return EXIT_SUCCESS;
}