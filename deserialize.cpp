//
// Created by jl on 04.12.18.
// This file implements means to
//


#include <iostream>
#include <fstream>
#include <vector>
#include <time.h>
#include <iomanip>
#include <sys/wait.h>

#include <IRFileReader.h>
#include <ImageBuilder.h>
#include <opencv2/core/mat.hpp>
#include <opencv2/imgproc.hpp>
#include <cv.hpp>
#include <netinet/in.h>
#include <IRDevice.h>
#include <IRLogger.h>
#include <boost/filesystem/operations.hpp>
#include <boost/range.hpp>
#include <chrono>

const unsigned BUFSIZE = 221560;
const int w = 382;
const int h = 288;

using namespace evo;

#define ulli unsigned long long int

#define SHOW_WINDOW_CVWAITKEY 5
#define FPS 10
#define GETTIME_STRING_LENGTH 24
#define FILE_MAX_CHUNKS 250
#define BLOCKSIZE 221560
//define SHOW

#define LOWER_TEMP rect_max_temp - 5
#define UPPER_TEMP rect_max_temp + 1
// was -5, +1
#define START_FILE_NO 0
// was 0

class IRHandlerClient : evo::IRImagerClient {
public:
    unsigned char* _bufferRaw = new unsigned char[BUFSIZE];
    cv::VideoWriter vwriter;
    bool ENABLE_WRITE = false;
    int file_no = START_FILE_NO;
    int fps = FPS;

    IRHandlerClient(IRDevice *pDevice, IRImager *pImager) {
        pImager->setClient(this);
    }

    std::string get_current_video_filename(bool with_ending = true) {
        if(with_ending) {
            return "out_" + std::to_string(file_no) + ".avi";
        } else {
            return "out_" + std::to_string(file_no);
        }
    }

    void onRawFrame(unsigned char* data, int size) override {
        std::cout << "onRawFrame" << std::endl;
    }

    void onThermalFrame(unsigned short* thermal, unsigned int w, unsigned int h, evo::IRFrameMetadata meta, void* arg) override {
        if(! this->ENABLE_WRITE) {
            return;
        }

        // settings
        auto palette = evo::eIron;
        auto scaling_method = evo::eMinMax;

        evo::ImageBuilder imgbuilder = evo::ImageBuilder(false);
        imgbuilder.setPalette(palette);
        imgbuilder.setPaletteScalingMethod(scaling_method);

        imgbuilder.setData(w, h, thermal);

        std::vector<unsigned char> mydata  = std::vector<unsigned char>(w * h * 3);
        std::vector<unsigned char> mydata2 = std::vector<unsigned char>(w * h * 3);
        unsigned char *mydata_ptr  = &mydata[0];
        unsigned char *mydata_ptr2 = &mydata2[0];

        imgbuilder.convertTemperatureToPaletteImage(mydata_ptr, true);
        imgbuilder.setData(w, h, thermal);

        const cv::Point middle = cv::Point(imgbuilder.getWidth()/2, imgbuilder.getHeight()/2);
        float rect_max_temp = 0;
        for(int i = 0; i < imgbuilder.getWidth(); i++) {
            for(int j = 0; j < imgbuilder.getHeight(); j++) {
                const cv::Point current = cv::Point(i, j);

                // rectangle at bottom
                //if(j > imgbuilder.getHeight() * 0.75 && i > imgbuilder.getWidth() * 0.33 && i < imgbuilder.getWidth() * 0.66) {

                // middle point
                //if(cv::norm(middle-current) < 50) {

                // center line
                if(i > 200 && i < 300) {
                    float temp = imgbuilder.getTemperatureAt(i, j);
                    if(temp > rect_max_temp) {
                        rect_max_temp = temp;
                    }
                }
            }
        }

        imgbuilder.setPaletteScalingMethod(evo::eManual);
        imgbuilder.setManualTemperatureRange(LOWER_TEMP, UPPER_TEMP);
        imgbuilder.convertTemperatureToPaletteImage(mydata_ptr2, true);

        // std::cout << " - max temp rectangle: " << rect_max_temp << ", file " << filename << std::endl;

        cv::Mat cv_img(cv::Size(w, h), CV_8UC3, mydata_ptr, cv::Mat::AUTO_STEP);
        cv::Mat cv_img2(cv::Size(w, h), CV_8UC3, mydata_ptr2, cv::Mat::AUTO_STEP);
        cv::cvtColor(cv_img, cv_img, CV_BGR2RGB);
        cv::cvtColor(cv_img2, cv_img2, CV_BGR2RGB);

#ifdef SHOW
        cv::imshow("Image-Pre", cv_img);
        cv::imshow("Image-Post", cv_img2);
        cv::waitKey(SHOW_WINDOW_CVWAITKEY);
#endif

        vwriter << cv_img2;
        //cv::imwrite(filename + std::to_string(current_frame) + ".png", cv_img2);
        cv::imwrite(get_current_video_filename(false) + "_last.png", cv_img2);
    }

    void onFlagStateChange(evo::EnumFlagState, void*) override {}

    void onProcessExit(void*) override {}

    void setBufferRaw(unsigned char* ptr) {
        std::memcpy(this->_bufferRaw, ptr, BUFSIZE);
    }

    void setWrite(bool write, std::string start = "", std::string end = "") {
        // activate write (was not activated)
        if(write && !ENABLE_WRITE) {
            std::cout << "Info: writing to file " << get_current_video_filename() << " " << start << " " << end << std::endl;
            vwriter.open(get_current_video_filename(), CV_FOURCC('M', 'J', 'P', 'G'), fps, cv::Size(w, h));
        }

        // disable write (was activated)
        if(!write && ENABLE_WRITE) {
            ++file_no;
            vwriter.release();
        }

        this->ENABLE_WRITE = write;
    }

    ~IRHandlerClient() {
        delete [] _bufferRaw;
        vwriter.release();
    }
};


ulli str2unixmilli(const std::string time) {
    const std::string time_format_string = "%d-%m-%Y_%H:%M:%S"; // http://www.cplusplus.com/reference/ctime/strftime/
    std::tm tm = {};
    std::stringstream ss(time);
    ss >> std::get_time(&tm, time_format_string.c_str());
    auto tp = std::chrono::system_clock::from_time_t(std::mktime(&tm));

    ulli t = static_cast<ulli>(std::chrono::duration_cast<std::chrono::milliseconds>(tp.time_since_epoch()).count());

    // get millis
    int millis = std::stoi(time.substr(time.find_last_of('.') + 1, std::string::npos));

    return t + millis;
};


struct StringComparator
{
    bool operator()(const std::string& lhs, const std::string& rhs)
    {
        int lhs_chunk = std::stoi(lhs.substr(lhs.find_last_of('.') + 1, std::string::npos));
        int rhs_chunk = std::stoi(rhs.substr(rhs.find_last_of('.') + 1, std::string::npos));

        return lhs_chunk < rhs_chunk;
    }
} StringComparer;


int main(int argc, char* argv[]){
    if(argc != 3 && argc != 6) {
        std::cout << "Usage: " << argv[0] << " <conf.xml> <logfile>" << std::endl;
        std::cout << "Usage: " << argv[0] << " <conf.xml> <folder with thermal data-chunks> <starttimestamp> <endtimestamp> <out-file>" << std::endl;
        std::cout << "Usage: Timeformat is DD-MM-YYYY_HH-MM-SS" << std::endl;
        std::cout << "Alternative usage: " << argv[0] << " <conf.xml> <logfile in folder with thermal data-chunks>" << std::endl;
        exit(EXIT_FAILURE);
    }

    const std::string CONF = std::string(argv[1]);
    std::string FOLDER;
    std::vector<std::pair<std::string, std::string>> SESSIONS; // stores pairs of sessions

    if(argc == 3) {
        std::ifstream in_logfile(argv[2]);

        if(!in_logfile.is_open()) {
            std::cout << "Error: can not read file " << std::string(argv[2]) << std::endl;
            return EXIT_FAILURE;
        }

        std::string line;

        unsigned current_log_idx = 0;
        while(std::getline(in_logfile, line)) {
            if(line.find("START SESSION") != std::string::npos) {
                SESSIONS.emplace_back(std::pair<std::string, std::string>("", ""));
                SESSIONS.back().first = line.substr(0, GETTIME_STRING_LENGTH);
            }

            if(line.find("STOP SESSION") != std::string::npos) {
                if(! SESSIONS.back().second.empty()) {
                    std::cout << "Warning: the log is corrupted" << std::endl;
                    std::cout << "Overriding SESSIONS.back().second. The session may not have been started correctly. (START SESSION marker is missing.)" << std::endl;
                    std::cout << "Session start:\t" << SESSIONS.back().first << std::endl;
                    std::cout << "Session end:\t" << SESSIONS.back().second << std::endl;
                    std::cout << "Session end 2:\t" << line.substr(0, GETTIME_STRING_LENGTH) << std::endl;
                    std::cout << "Please check your log. Exiting." << std::endl;
                    return EXIT_FAILURE;
                }
                SESSIONS.back().second = line.substr(0, GETTIME_STRING_LENGTH);
            }
        }
    }

    if(! SESSIONS.empty()) {
        std::cout << "Info: Loaded " << SESSIONS.size() << " interactions from log " <<  std::string(argv[2]) << std::endl;
    }

    int current_log_idx = 0;
    if(argc == 6 || ! SESSIONS.empty()) {
        if(FOLDER.empty())
            FOLDER = std::string(argv[2]).substr(0, std::string(argv[2]).find_last_of('/')) + '/';
        if(SESSIONS.empty())
            SESSIONS.emplace_back(std::string(argv[3]), std::string(argv[4]));

        std::cout << "Info: Reading thermal-data-folder " << FOLDER << " ..." << std::flush;

        std::vector<std::string> files;

        boost::filesystem::path path(FOLDER);

        if (boost::filesystem::is_directory(path)) {
            for (auto &entry : boost::make_iterator_range(boost::filesystem::directory_iterator(path), {})) {
                std::string filename = entry.path().string();
                if (filename.find(".thermal.") != std::string::npos) {
                    files.push_back(filename);
                }
            }
        }

        std::sort(files.begin(), files.end(), StringComparer);

        std::vector<std::pair<std::string, std::string>> files_timestamps_vector;
        for (auto &s : files) {
            // open file
            std::ifstream current_chunk_file;
            current_chunk_file.open(s);

            // get date-string which is in the beginning of file
            char date[GETTIME_STRING_LENGTH];
            current_chunk_file.read(date, GETTIME_STRING_LENGTH);

            // convert date-string to std::string
            std::string date_str = std::string(date);

            // make pair
            files_timestamps_vector.emplace_back(s, date_str);

            // close file
            current_chunk_file.close();
        }

        files.clear();

        std::cout << "\t found " << files_timestamps_vector.size() << " thermal chunks." << std::endl;
        std::cout << "Info: First timestamp in folder at " << files_timestamps_vector.front().second;
        std::cout << ", last timestamp at " << files_timestamps_vector.back().second << "." << std::endl;

        // Set up optris
        IRLogger::setVerbosity(IRLOG_ERROR, IRLOG_OFF);
        IRDeviceParams xmlparams;
        IRDeviceParamsReader::readXML(CONF.c_str(), xmlparams);
        IRDevice *dev = IRDevice::IRCreateDevice(xmlparams);
        evo::IRImager imager;
        imager.init(&xmlparams, dev->getFrequency(), dev->getWidth(), dev->getHeight(), dev->controlledViaHID());
        IRHandlerClient client(dev, &imager);

        int cur_file_idx = 0;
        int cur_session_idx = 0;
        bool BREAK = false;
        do {
            // increment indizes
            std::ifstream current_file(files_timestamps_vector[cur_file_idx++].first);

            char current_timestamp[GETTIME_STRING_LENGTH];
            unsigned char buffer[BLOCKSIZE];

            // loop to read one file
            for (int j = 0; j < FILE_MAX_CHUNKS && !BREAK; j++) {
                if(j == 0 && cur_file_idx % 100 == 0) {
                    std::cout << "Prog: Progressing thermal chunk " << cur_file_idx << "/" << files_timestamps_vector.size() << std::endl;
                }

                if (current_file.is_open()) {
                    current_file.read(current_timestamp, GETTIME_STRING_LENGTH); // read current timestamp
                    current_file.read((char *) buffer, BLOCKSIZE); // read raw thermal data
                } else {
                    std::cout << "Current_file is not open" << std::endl;
                }

                auto current_session = SESSIONS[cur_session_idx];
                // print notification
                ulli TIME1 = str2unixmilli(current_session.first);
                ulli TIME2 = str2unixmilli(current_session.second);

                // set data in client for optris
                client.setBufferRaw(buffer);
                imager.process(client._bufferRaw);

                // print error and shorten recording if it exceeds five minutes

                if (str2unixmilli(std::string(current_timestamp)) > TIME1) {
                    // start recording
                    client.setWrite(true, current_session.first, current_session.second);
                }

                if (str2unixmilli(std::string(current_timestamp)) > TIME2) {
                    // stop recording, break loops
                    client.setWrite(false);

                    // break condition if all SESSIONS were processed
                    if(SESSIONS[cur_session_idx] == SESSIONS.back()) {
                        BREAK = true;
                        break;
                    }

                    ++cur_session_idx;
                }
            }

            current_file.close();
        } while (!BREAK);
    }

    return EXIT_SUCCESS;
}