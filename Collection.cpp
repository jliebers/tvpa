//
// Created by jl on 21.11.18.
//

#include "Collection.h"

Collection::Collection(std::string path) {
    std::vector<cv::String> dir; // std::string in opencv2.4, but cv::String in 3.0

    try {
        cv::glob(path, dir, false);
    } catch (cv::Exception e) {
        std::cerr << e.what() << std::endl
        << "Warning: Collection::Collection cv::glob() failed. Folder or file might not exist. (error 15)" << std::endl;
    }

    for(cv::String cvfile : dir) {
        // get information about file
        const std::string file = cvfile.operator std::string();
        const std::string extension = boost::filesystem::extension(file);
        const std::string name = boost::filesystem::path(path).filename().string();

        if(extension == ".avi") {
            // create a video
            Video* videoptr = new Video(file);

            if(videoptr->count_frames() == 0) {
                std::cerr << "Warning: " << file << " has zero frames " << std::endl;
            }

            m_collection.push_back(videoptr->get_frame(0)); // FIXME remove '0' against a meaningful stacked skeleton
        } else if(extension == ".png") {
            // create an image
            Image* imageptr = new Image(file);
            m_collection.push_back(imageptr);
        }

    }
}

Collection::~Collection() {
    for(auto imageptr : m_collection) {
        delete imageptr;
    }
}

unsigned Collection::get_size() {
    return static_cast<unsigned int>(m_collection.size());
}

std::vector<std::pair<double, Image*>> Collection::compare(Image* image, eDistanceCalculationAlgorithm algo) {
#ifdef COLLECTION_TIME_MEASURE
    auto start = std::chrono::system_clock::now(); // measure execution time
    std::cout << "Info: Comparing Skeleton '" << image->get_name() << "' to collection ..." << std::endl;
#endif
    // create return object
    std::vector<std::pair<double, Image*>> vec = std::vector<std::pair<double, Image*>>();

    // populate return object
    for(Image* im : m_collection) {
#ifdef COLLECTION_TIME_MEASURE
        auto start = std::chrono::system_clock::now(); // measure execution time
#endif

        double distance = image->get_skeleton()->compute_distance(im->get_skeleton(), algo);

#ifdef COLLECTION_TIME_MEASURE
        std::string algostr;
        if(algo == eHausdorff) {
            algostr = "eHausdorff";
        } else if (algo == eShapeContext) {
            algostr = "eShapeContext";
        } else if (algo == eBoth) {
            algostr = "eBoth";
        }

        auto end = std::chrono::system_clock::now();
        auto elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(end - start);
        std::cout << "  Distance calculation of " + image->get_name() +  + " to " + im->get_name() << " with "
        << algostr << " took " << elapsed.count()/1000 << " s. Result: " << distance << std::endl;
#endif

        // Create return value
        std::pair<double, Image*> p = std::pair<double, Image*>(distance, im);

        // Save return value to return vector
        vec.push_back(p);
    }

    std::sort(vec.begin(), vec.end());

#ifdef COLLECTION_TIME_MEASURE
    auto end = std::chrono::system_clock::now();
    auto elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(end - start);
    std::cout << "Comparison to collection took " << elapsed.count() /1000/60 << " min." << std::endl;
#endif

    return vec;
}
