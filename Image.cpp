//
// Created by jl on 18.11.18.
//


#include "Image.h"


Image::Image(cv::Mat image, std::string name) {
    // copy name
    this->m_name = std::move(name);

    // create new mat and copy contents
    this->m_original_image_ptr = new cv::Mat();
    image.copyTo(*this->m_original_image_ptr);

    this->m_ip_ptr = new ImageProcessor();
}


Image::Image(std::string path) : Image(cv::imread(path),
                                       std::move(boost::filesystem::path(path).filename().string())) {
    // Call to delegated Constructor `Image::Image(cv::Mat image, std::string name)`.
    // This is empty on purpose.
}


Image::~Image() {
    delete this->m_original_image_ptr;
    delete this->m_skeleton_ptr;
    delete this->m_ip_ptr;
}


std::string Image::get_name() {
    return this->m_name;
}


void Image::set_name(std::string new_name) {
    this->m_name = std::move(new_name);
}


cv::Mat* Image::get_image() {
    return this->m_original_image_ptr;
}


Skeleton* Image::get_skeleton(eSkeletonExtractionAlgorithm algorithm) {
    // if no skeleton was created yet
    if(this->m_skeleton_ptr == nullptr) {
        // lazily create Skeleton upon first call

        cv::Mat skeleton_mat;

        try {
            skeleton_mat = this->m_ip_ptr->process_frame(this->m_original_image_ptr, algorithm);
        } catch (const std::logic_error &le) {
            throw le; // pass up the stack
        }

        if(skeleton_mat.empty()) {
            throw std::logic_error("Image::get_skeleton() m_ip_ptr created an empty skeleton_mat from frame (error 06)");
        } else {
            // store pointer in member variable
            // skeleton will not be created twice
            this->m_skeleton_ptr = new Skeleton(&skeleton_mat, m_name);
        }
    }

    return this->m_skeleton_ptr;
}

cv::Mat Image::get_debug_mat(eSkeletonExtractionAlgorithm algorithm) {
    if(this->m_skeleton_ptr == nullptr) {
        this->get_skeleton(algorithm);
    }

    cv::Mat ret = m_ip_ptr->get_diag_mat();

    if(ret.empty()) {
        throw std::logic_error("Image::get_debug_mat() would return an empty mat (errror 16)");
    }

    return ret;
}
