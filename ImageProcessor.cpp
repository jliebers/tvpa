//
// Created by jl on 19.11.18.
//


#include "ImageProcessor.h"


cv::Mat ImageProcessor::process_frame(const cv::Mat* image, eSkeletonExtractionAlgorithm algorithm) {
    image->copyTo(m_debug_mat);

    // WILL STORE THE CENTER OF THE CIRCLE (AKA ROI AKA MIDDLE_POINT) AND THE CENTER OF THE KNUCKLES
    cv::Point2i center_of_circle, center_of_knuckles;

    // HOLDS THE USERS HAND SUBTRACTED FROM THE BACKGROUND
    cv::Mat hand_mask;
    cv::Mat hand_roi = this->extract_hand(image, &hand_mask);

    // EXTRACTS THE CIRCULAR ROI FROM THE BACKGROUND
    cv::Mat roi = this->extract_roi(&hand_roi, &hand_mask, &center_of_circle, &center_of_knuckles);

    // CREATES A SKELETON
    // IF SKIP_PROCESSING IS SET TO TRUE THE FURTHER PROCESSING WILL BE SKIPPED
    // THIS IS ONLY USED FOR EDGEDETECTION FOR NOW
    bool skip_processing = false;
    cv::Mat fat_skeleton = this->extract_skeleton(&roi, algorithm, &skip_processing);

    cv::Mat thinned_skeleton;

    // WE MAY NEED TO SKIP FURTHER PROCESSING IN CASE OF EDGEDETECTION
    if(! skip_processing) { // IF PROCESSING IS NOT SKIPPED
        // PROCEED TO FILTER THE SKELETON WHEN USING EADAPTIVETHRESHOLD OR EGLOBALTHRESHOLD
        cv::Mat filter_skeleton = this->filter_skeleton(&fat_skeleton);
        // APPLY THINNING
        thinned_skeleton = this->thin_skeleton(&filter_skeleton);
    } else { // IF PROCESSING IS SKIPPED
        // NO FILTERING AND THINNING NEEDED, SIMPLY APPLY THE FAT_SKELETON TO THIN_SKELETON IN ORDER TO HAVE IT IN THE
        // RIGHT VARIABLE FOR ALIGN_SKELETON()
        thinned_skeleton = fat_skeleton;
    }

    cv::Mat aligned_skeleton;
    try {
        // ALIGN THE SKELETON IE CENTER, ZOOM AND ROTATE
        aligned_skeleton = this->align_skeleton(&thinned_skeleton,
                                                        std::pair<cv::Point2i, cv::Point2i>(
                                                                center_of_circle,   // center
                                                                center_of_knuckles  // knuckles
                                                        )
        );
    } catch (const std::logic_error &le) {
        throw; // pass the Exception up the stack
    }

    // CREATE DEBUG MAT
    for (int i = 0; i < thinned_skeleton.rows; i++) { // 288
        for (int j = 0; j < thinned_skeleton.cols; j++) { // 382
            if (thinned_skeleton.at<uchar>(i, j) == 255) {
                m_debug_mat.at<cv::Vec3b>(i, j) = cv::Vec3b(255, 255, 255);
            }
        }
    }

    // After align_skeleton, we need to make sure that we have a thinned binary image indeed
    // This is due to warpAffine transforming the thinned skeleton into a grayscale image with nonzero
    // Thus we just threshold and thin it again
    cv::Mat bin_aligned, ret;
    cv::threshold(aligned_skeleton, bin_aligned, 25, 255, cv::THRESH_BINARY);
    ret = thin_skeleton(&bin_aligned);

#ifdef DEBUG_DRAW_OUTPUT
    cv::imshow("original", *image);
    cv::imshow("m_debug_mat", m_debug_mat);
    cv::imshow("adaptiveThreshold (fat_skeleton)", fat_skeleton);
    cv::imshow("aligned_skeleton", aligned_skeleton);
    cv::imshow("ret", ret);
    while(cv::waitKey(1) != int('q'));
#endif

    return ret;
}


cv::Mat ImageProcessor::extract_hand(const cv::Mat *rgb_image, cv::Mat *hand_mask) {
    // perform a threshold on Mat based on a colored upper and lower bound:
    cv::Scalar lowerb = HAND_LOWER_RGB;
    cv::Scalar upperb = HAND_UPPER_RGB;
    cv::Mat mask;
    cv::inRange(*rgb_image, lowerb, upperb, mask);

    // create return object
    cv::Mat ret;
    rgb_image->copyTo(ret, mask); // apply mask

    if(hand_mask != nullptr) {
        mask.copyTo(*hand_mask);
    }

    return ret;
}


cv::Mat ImageProcessor::extract_roi(const cv::Mat* rgb_hand_image,
                                    const cv::Mat* hand_mask,
                                    cv::Point2i* center_of_circle,
                                    cv::Point2i* center_of_knuckles) {
    // get contours:
    std::vector<cv::Vec4i> hierarchy;
    std::vector<std::vector<cv::Point>> contours;
    cv::findContours(*hand_mask, contours, hierarchy, cv::RETR_LIST, cv::CHAIN_APPROX_SIMPLE);

    // check on error:
    if(contours.empty()) {
        throw std::logic_error("ImageProcessor::extract_roi contours is empty (error 07)");
    }

    // find largest contour:
    double largest_area = 0;
    int largest_contour_idx = 0;
    cv::Rect bounding_rect;
    for (int i = 0; i < contours.size(); i++) { // iterate through each contour.
        double a = contourArea(contours[i], false);  //  Find the area of contour
        if (a > largest_area) {
            largest_area = a;
            largest_contour_idx = i;                 //Store the index of largest contour
            bounding_rect = boundingRect(contours[i]); // Find the bounding rectangle for biggest contour
        }
    }

    // define raw_thermal_data structures for contours:
    std::vector<std::vector<cv::Point>> hulls(contours.size());
    std::vector<std::vector<int>> hullsInt(contours.size());
    std::vector<std::vector<cv::Vec4i>> defects(contours.size());

    // Find all hulls and defects:
    for (int i = 0; i < contours.size(); i++) {
        cv::convexHull(cv::Mat(contours[i]), hulls[i], false);
        cv::convexHull(cv::Mat(contours[i]), hullsInt[i], false);
        if (hullsInt[i].size() > 3) {
            convexityDefects(contours[i], hullsInt[i], defects[i]);
        }
    }

    // Get points near joints of fingers
    std::vector<cv::Point> defect_points_vec;
    {
        for (const auto &defect : defects) {
            for (const auto &d : defect) {
                int farthest_pt_index = d.val[2];
                cv::Point pt_far(contours[largest_contour_idx][farthest_pt_index]);
                double depth = double (d.val[3]) / 256;

                if(depth < this->DEFECT_MIN_DEPTH) {
                    continue;
                }

                defect_points_vec.push_back(pt_far);

                // stop after a maximum of five points
                if(defect_points_vec.size() >= 5) {
                    goto hell; // sorry - we break both loops here :)
                }
            }
        }
    }

hell:
    // get center of defects
    // sum up all points and divide through amount
    cv::Point defect_center_pt = cv::Point2i(0, 0);
    for(auto point : defect_points_vec) {
        defect_center_pt.x += point.x;
        defect_center_pt.y += point.y;
    }

    if(defect_points_vec.empty()) {
        throw std::logic_error("Error: No Defects found in image (could not detect a hand) (error 16)");
    }

    defect_center_pt.x /= defect_points_vec.size();
    defect_center_pt.y /= defect_points_vec.size();
    // set member variable
    if(center_of_knuckles != nullptr) {
        *center_of_knuckles = defect_center_pt;
    }

    // find circular mask:
    struct Circle { double radius = 0; int center_x = 0; int center_y = 0; };
    //std::atomic<Circle> circle;
    Circle circle;

#ifdef DEBUG_MEASURE_TIME
    auto start = std::chrono::system_clock::now(); // measure execution time
#endif


#ifdef USE_THREADED_PPT
    std::vector<std::thread*> tlist;
#endif

    for (int i = 0; i < hand_mask->rows; i++) { // 288
        for (int j = 0; j < hand_mask->cols; j++) { // 382
            if(hand_mask->at<uchar>(i, j) == 255) {
#ifdef USE_THREADED_PPT
                tlist.emplace_back(new std::thread([&circle, i, j, contours, largest_contour_idx] () {
#endif
                double ppt = cv::pointPolygonTest(contours[largest_contour_idx], cv::Point2i(j, i), true);
                //if(ppt > circle.load().radius) {
                if(ppt > circle.radius) {
                    circle = {ppt, j, i};
                }
#ifdef USE_THREADED_PPT
                }));
#endif
            }
        }
    }
#ifdef USE_THREADED_PPT
    for (auto t : tlist) {
        t->join();
    }
#endif

    // apply crop factor:
    //circle = {circle.load().radius * this->CIRCLE_CROP_FACTOR, circle.load().center_x, circle.load().center_y};
    circle = {circle.radius * this->CIRCLE_CROP_FACTOR, circle.center_x, circle.center_y};

#ifdef DEBUG_MEASURE_TIME
    auto end = std::chrono::system_clock::now();
    auto elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(end - start);
    std::cout << "PolygonPointTest (PPT) took " << elapsed.count() << " ms." << std::endl;
#endif

    // draw debug roi circle
    /*cv::circle(m_debug_mat,
               cv::Point2i(circle.load().center_x, circle.load().center_y),
               (int) (circle.load().radius),
               CV_RGB(255, 255, 255));*/
    if(enable_detailed_debug) {
        cv::circle(m_debug_mat,
                   cv::Point2i(circle.center_x, circle.center_y),
                   (int) (circle.radius),
                   CV_RGB(255, 255, 255));
    }

    // create mask:
    cv::Mat circle_mask = cv::Mat::zeros(cv::Size(rgb_hand_image->cols, rgb_hand_image->rows), CV_8UC1);
    /*cv::circle(circle_mask,
               cv::Point2i(circle.load().center_x, circle.load().center_y),
               (int) (circle.load().radius),
               255,
               -1);*/
    cv::circle(circle_mask,
               cv::Point2i(circle.center_x, circle.center_y),
               (int) (circle.radius),
               255,
               -1);

    // create return object and copy it by mask:
    cv::Mat ret;
    rgb_hand_image->copyTo(ret, circle_mask);

    if(center_of_circle != nullptr) {
        //*center_of_circle = cv::Point2i(circle.load().center_x, circle.load().center_y);
        *center_of_circle = cv::Point2i(circle.center_x, circle.center_y);
    }


    if(enable_detailed_debug) {
        // DRAW DEBUG INFO
        // draw the contour and its associated defect defect_points_vec (blue circles)
        cv::drawContours(m_debug_mat, contours, largest_contour_idx, CV_RGB(0, 0, 255), 1, 8, std::vector<cv::Vec4i>(),
                         0, cv::Point());
        cv::circle(m_debug_mat, defect_center_pt, 3, CV_RGB(0, 0, 255));
        // draw contour in green
        cv::drawContours(m_debug_mat, hulls, largest_contour_idx, CV_RGB(0, 255, 0), 1, 8, std::vector<cv::Vec4i>(), 0,
                         cv::Point());

        for (const auto &defect_point : defect_points_vec) {
            cv::circle(m_debug_mat, defect_point, 5, CV_RGB(0, 0, 255), 2, 8);
        }
        //cv::circle(m_debug_mat, cv::Point2i(circle.load().center_x, circle.load().center_y), 3, CV_RGB(255, 255, 255));
        cv::circle(m_debug_mat, cv::Point2i(circle.center_x, circle.center_y), 3, CV_RGB(255, 255, 255));
        // END DRAW DEBUG INFO
    }

    return ret;
}


cv::Mat ImageProcessor::extract_skeleton(const cv::Mat *rgb_roi,
        eSkeletonExtractionAlgorithm algorithm,
        bool* skip_processing) {
    // create return object
    cv::Mat skeleton = cv::Mat::zeros(cv::Size(rgb_roi->cols, rgb_roi->rows), CV_8UC1);


    // DECLARATIONS FOR CASE 1 AND 2
    cv::Mat ngs_roi; // will hold the grayscale of *rgb_roi
    cv::cvtColor(*rgb_roi, ngs_roi, cv::COLOR_RGB2GRAY);
    cv::equalizeHist(ngs_roi, ngs_roi);

    // DECLARATIONS FOR CASE 3
    cv::Mat edge_detection;
    std::vector<std::vector<cv::Point> > canny_contours;

    switch(algorithm) {
        case eGlobalThreshold:
            cv::threshold(ngs_roi, skeleton, globalthresh_thresh, globalthresh_maxval, globalthresh_type);
            break;
        case eAdaptiveThreshold:
            cv::adaptiveThreshold(ngs_roi, // src
                                  skeleton, // dst
                                  adaptive_maxvalue, // maxValue
                                  adaptivemethod, // adaptiveMethod
                                  adaptive_thresholdtype,  // thresholdType
                                  adaptive_blocksize, // blocksize
                                  adaptive_c); // C
            break;
        case eEdgeDetection:
            if(skip_processing != nullptr) {
                *skip_processing = true;
            }

            cv::blur(ngs_roi, edge_detection, cv::Size(edgedetection_kSize, edgedetection_kSize));
            for(int i = 0; edgedetection_blurTimes > 0 && i < edgedetection_blurTimes-1; i++) {
                cv::blur(edge_detection, edge_detection, cv::Size(edgedetection_kSize, edgedetection_kSize));
            }

            cv::Canny(edge_detection,
                      edge_detection,
                      edgedetection_threshold1,
                      edgedetection_threshold2,
                      edgedetection_cannyKernelsize);

            cv::findContours(edge_detection, canny_contours, cv::RETR_LIST, cv::CHAIN_APPROX_NONE);

            for(int i = 0; i < canny_contours.size(); i++) {
                if(cv::contourArea(canny_contours[i]) > edgedetection_noiseremoval_minareasize) {
                    cv::drawContours(edge_detection, canny_contours, i, CV_RGB(0, 0, 0), cv::FILLED);
                }
            }

            {
                // now we remove the circle from the blurred image
                cv::Moments m = cv::moments(edge_detection, true);
                // get points of center of skeleton and center of mat
                const cv::Point centerpt(m.m10/m.m00, m.m01/m.m00);

                // paint circle to center point
                //cv::circle(edge_detection, centerpt, 5, CV_RGB(255, 255, 255), 1);

                // iterate image to get the point furthest away from center (radius)
                double max_dist = 0;
                for(int i = 0; i < edge_detection.rows; i++) {
                    for(int j = 0; j < edge_detection.cols; j++) {
                        if(edge_detection.at<uchar>(i, j) == 255) {
                            double distance = pt_distance(centerpt.y, centerpt.x, i, j);
                            if (distance > max_dist) {
                                max_dist = distance;
                            }
                        }
                    }
                }

                if(max_dist == 0) {
                    return edge_detection;
                }

                // set all pixels to black if they exceed radius*0.9
                for(int i = 0; i < edge_detection.rows; i++) {
                    for(int j = 0; j < edge_detection.cols; j++) {
                        if(pt_distance(centerpt.y, centerpt.x, i, j) > (max_dist * 0.8) ){
                            edge_detection.at<uchar>(i, j) = 0;
                        }
                    }
                }
            }

            return edge_detection;
        default:
            throw std::logic_error("ImageProcessor::extract_skeleton() fed with an unknown algorithm param (error 17)");
    }

    return skeleton;
}


cv::Mat ImageProcessor::thin_skeleton(const cv::Mat* fat_skeleton) {
    VoronoiThinner vt;

    if(! vt.thin(*fat_skeleton, "zhang_suen", false)) {
        throw std::logic_error("Code error - vt.thin() was called with an unknown algorithm in file ImageProcessor.cpp. (error 18)");
    }

    return vt.get_skeleton();
}


cv::Mat ImageProcessor::filter_skeleton(cv::Mat* thin_skeleton) {
    cv::Mat ret;

    // remove noise
    cv::Mat kernel = cv::getStructuringElement(cv::MORPH_CROSS, cv::Size(3, 3));
    cv::Mat kernel2 = cv::getStructuringElement(cv::MORPH_CROSS, cv::Size(5, 5));

    cv::medianBlur(*thin_skeleton, ret, 3);

    cv::erode(ret, ret, kernel2);
    cv::dilate(ret, ret, kernel2);

    //cv::dilate(skeleton, skeleton, kernel2);
    //cv::erode(skeleton, skeleton, kernel2);

    cv::medianBlur(ret, ret, 5);

    return ret;
}


cv::Mat ImageProcessor::align_skeleton(const cv::Mat* thin_filtered_skeleton,
                                       std::pair<cv::Point2i, cv::Point2i> centerline) {
    // return object
    cv::Mat ret;

    // extract line
    cv::Point2i center = centerline.first;
    cv::Point2i front = centerline.second;

    // error handling
    if(center.x == 0 || center.y == 0 || front.x == 0 || front.y == 0) {
        throw std::logic_error("ImageProcessor::align_skeleton() centerline has points whose components are zero"
        " (error 09) // "
        + std::to_string(center.x) + " "
        + std::to_string(center.y) + " "
        + std::to_string(front.x) + " "
        + std::to_string(front.y));
    } else if (thin_filtered_skeleton->empty()) {
        throw std::logic_error("ImageProcessor::align_skeleton() thin_filtered_skeleton is empty (error 10)");
    }

    thin_filtered_skeleton->copyTo(ret);

    // PERFORM CENTERING
    if(enable_centering){
        cv::Moments m = cv::moments(ret, true);

        // get points of center of skeleton and center of mat
        cv::Point2i center_skeleton((int) (m.m10/m.m00), (int) (m.m01/m.m00));
        cv::Point2i center_mat(ret.cols/2, ret.rows/2);

        // calculate offset between center of mat and center of skeleton
        int offsetx = center_mat.x - center_skeleton.x;
        int offsety = center_mat.y - center_skeleton.y;

        // perform translation
        cv::Mat trans_mat = (cv::Mat_<double>(2,3) << 1, 0, offsetx, 0, 1, offsety);
        cv::warpAffine(*thin_filtered_skeleton, ret, trans_mat, ret.size());
    } // `ret` is now translated to the middle of the Mat.


    // PERFORM ROTATION
    if(enable_rotation) {
        const std::vector<int> nullvector = {centerline.first.x - centerline.first.x,
                                             10 - centerline.first.y}; // faces northwards
        const std::vector<int> linevector = {centerline.second.x - centerline.first.x,
                                             centerline.second.y - centerline.first.y}; // vector of centerline

        // Calculate angle between both vectors:
        double lambda = ((nullvector[0] * linevector[0]) + (nullvector[1] * linevector[1]))
                        / ((std::sqrt(std::pow(nullvector[0], 2) + std::pow(nullvector[1], 2)))
                           * (std::sqrt(std::pow(linevector[0], 2) + std::pow(linevector[1], 2))));
        double angle = std::acos(lambda) * 180.0 / M_PI;

        // device whether to go clockwise or anti-clockwise
        // if linevector is x-negative (quadrant II + III), change sign of angle to rotate anti-clockwise
        if (linevector[0] < 0) {
            angle = -angle;
        }

        // Draw debug
        if (enable_detailed_debug) {
            cv::arrowedLine(m_debug_mat, centerline.first, centerline.second, CV_RGB(0, 0, 255));
        }

        cv::Point2f ptCp(static_cast<float>(thin_filtered_skeleton->cols * 0.5),
                         static_cast<float>(thin_filtered_skeleton->rows * 0.5));

        cv::Mat M = cv::getRotationMatrix2D(ptCp, angle, 1.0);
        cv::Mat local_mat_since_warpAffine_is_not_able_to_operate_in_place_duh;
        ret.copyTo(local_mat_since_warpAffine_is_not_able_to_operate_in_place_duh);
        cv::warpAffine(local_mat_since_warpAffine_is_not_able_to_operate_in_place_duh, ret, M,
                       thin_filtered_skeleton->size(), cv::INTER_CUBIC);
    } // `ret` is now rotated.


    // PERFORM ZOOMING (SCALING)
    if(enable_zooming){
        // will contain the most-left, most-top, most-right, and most-bottom non-null pixel in the image.
        cv::Point right, bottom, left, top;

        for(int col = 0; (col < ret.cols); col++) {
            for(int row = 0; (row < ret.rows); row++) {
                if(ret.at<uchar>(row, col) > 0){
                    right = cv::Point(col, row);
                }
            }
        }
        for(int row = 0; (row < ret.rows); row++) {
            for(int col = 0; (col < ret.cols); col++) {
                if(ret.at<uchar>(row, col) > 0){
                    bottom = cv::Point(col, row);
                }
            }
        }
        for(int col = ret.cols -1; (col >= 0); col--) {
            for(int row = ret.rows -1; (row >= 0); row--) {
                if(ret.at<uchar>(row, col) > 0){
                    left = cv::Point(col, row);
                }
            }
        }
        for(int row = ret.rows -1; (row >= 0); row--) {
            for(int col = ret.cols -1; (col >= 0); col--) {
                if(ret.at<uchar>(row, col) > 0){
                    top = cv::Point(col, row);
                }
            }
        }

        const int height = ret.rows;
        const int width = ret.cols;

        int size_v = bottom.y - top.y;
        int size_h = right.x - left.x;

        int gap_left = left.x;
        int gap_top = top.y;
        int gap_right = width - right.x;
        int gap_bottom = height - bottom.y;

        int min_dist_horizontal = std::min(gap_left, gap_right);
        int min_dist_vertical = std::min(gap_bottom, gap_top);
        double scaling_factor;

        if(min_dist_horizontal < min_dist_vertical) {
            scaling_factor = (double) width / (double) size_h;
        } else {
            scaling_factor = (double) height / (double) size_v;
        }

        cv::Rect rect(cv::Point(right.x, bottom.y), cv::Point(left.x, top.y));

        cv::Mat zoom(ret, rect);

        cv::resize(zoom, zoom, cv::Size(), scaling_factor, scaling_factor);

        // `zoom` is now zoomed but has lost its original proportions
        // we must bring the proportions back now
        cv::Mat filler_mat_h = cv::Mat::zeros(height, 1, CV_8UC1);
        cv::Mat filler_mat_v = cv::Mat::zeros(1, width, CV_8UC1);

        if(zoom.cols < ret.cols) { // fill horizontally with black:
            int to_fill = ret.cols - zoom.cols;

            cv::Mat filler_mat_front;
            cv::Mat filler_mat_end;

            if(to_fill % 2 == 0) { // even
                filler_mat_front = cv::Mat::zeros(height, to_fill/2, CV_8UC1);
                filler_mat_end = cv::Mat::zeros(height, (to_fill/2), CV_8UC1);
            } else { // odd
                filler_mat_front = cv::Mat::zeros(height, to_fill/2, CV_8UC1);
                filler_mat_end = cv::Mat::zeros(height, (to_fill/2)+1, CV_8UC1);
            }
            cv::Mat arr[] = { filler_mat_front, zoom, filler_mat_end };
            cv::hconcat(arr, 3, zoom);
        } else if(zoom.rows < ret.rows) { // fill vertically with black:
            int to_fill = ret.rows - zoom.rows;

            cv::Mat filler_mat_front;
            cv::Mat filler_mat_end;

            if(to_fill % 2 == 0) { // even
                filler_mat_front = cv::Mat::zeros(to_fill/2, width, CV_8UC1);
                filler_mat_end = cv::Mat::zeros(to_fill/2, width, CV_8UC1);
            } else { // odd
                filler_mat_front = cv::Mat::zeros(to_fill/2, width, CV_8UC1);
                filler_mat_end = cv::Mat::zeros((to_fill/2)+1, width, CV_8UC1);
            }
            cv::Mat arr[] = { filler_mat_front, zoom, filler_mat_end };
            cv::vconcat(arr, 3, zoom);
        }

        // Check if zoom has produced a good result. If no, log error. If yes, copy result to `ret`.
        if(zoom.cols == ret.cols || zoom.rows == ret.rows) {
            zoom.copyTo(ret);
        } else {
            std::cerr << "Warning: ImageProcessor::align_skeleton() `zoom` has a wrong Size " << zoom.size <<
                      ". Skeleton `ret` was not updated from zoom." << std::endl;
        }
    }

    return ret;
}

cv::Mat ImageProcessor::get_diag_mat() {
    return m_debug_mat;
}

double ImageProcessor::pt_distance(int p1_x, int p1_y, int p2_x, int p2_y) {
    return std::sqrt(std::pow(p2_x - p1_x, 2) + std::pow(p2_y - p1_y, 2));
}