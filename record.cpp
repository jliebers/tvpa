//
// Created by jl on 02.12.18.
//

#define DEBUG

#include <opencv2/videoio.hpp>
#include <cv.hpp>
#include <chrono>
#include <iostream>
#include <signal.h>
#include <iomanip>

#include <IRLogger.h>
#include <IRDeviceParams.h>
#include <IRDevice.h>
#include <IRImager.h>

#include <ImageBuilder.h>
#include "Image.h"

#define BLOCKSIZE 221560
#define GETTIME_STRING_LENGTH 24

evo::IRDeviceParams params;
evo::IRDevice *device;
evo::IRImager imager;

int w, h; // w and h of imager
auto start = std::chrono::system_clock::now();

Image* current_image_ptr = nullptr;
cv::VideoWriter vwriter;
int MIN_TEMP = 30, MAX_TEMP = 40;
bool EXIT = false, THROTTLE = false, RECORD = false, AUTOTEMP = false;
std::string USER = "";

std::string WINDOW_NAME = "tvpa-record";


std::string get_time(bool delim = true) {
    using namespace std::chrono;
    // get current time
    auto now = system_clock::now();
    // get number of milliseconds for the current second
    // (remainder after division into seconds)
    auto ms = duration_cast<milliseconds>(now.time_since_epoch()) % 1000;
    // convert to std::time_t in order to convert to std::tm (broken time)
    auto timer = system_clock::to_time_t(now);
    // convert to broken time
    std::tm bt = *std::localtime(&timer);
    std::ostringstream oss;
    oss << std::put_time(&bt, "%d-%m-%Y_%H:%M:%S");
    oss << '.' << std::setfill('0') << std::setw(3) << ms.count();
    if(delim) {
        return oss.str() + "   ";
    } else {
        return oss.str();
    }
}

void record_thermal() {
    unsigned long CHUNK = 0;
    std::vector<std::pair<std::string, unsigned char *>> thermalvector;
    imager.forceFlagEvent(0);

    while (true) {
        using namespace std::chrono_literals;
        if (THROTTLE) {
            std::this_thread::sleep_for(40ms); // approx 25 fps
        }

        unsigned char *bufferRaw = new unsigned char[BLOCKSIZE];

        double timestamp;
        if (device->getFrame(bufferRaw, &timestamp) == evo::IRIMAGER_SUCCESS) {
            imager.process(bufferRaw);

            auto pair = std::pair<std::string, unsigned char *>();
            pair.first = get_time(false);
            pair.second = bufferRaw;

            thermalvector.push_back(pair);
        } else {
            std::cout << get_time() << " Error: getFrame(bufferRaw) did not succeed" << std::endl;
        }

        if (thermalvector.size() == 250) {
            std::cout << get_time() << " Info: Flushing " + std::to_string(thermalvector.size()) + " vectors at chunk " << CHUNK << std::endl;

            // dump thermal data
            std::ofstream myfile;
            const std::string filename = "./thermal_" + USER + "_raw." + std::to_string(thermalvector.size()) + "." + std::to_string(CHUNK);
            myfile.open(filename);

            // write to disk
            for (auto &i : thermalvector) {
                myfile.write(i.first.c_str(), GETTIME_STRING_LENGTH);
                myfile.write((char *) &i.second[0], BLOCKSIZE);
            }
            myfile.close();

            CHUNK += 1;

            // delete other stuff which has been written to file by now
            for (auto buf : thermalvector) {
                delete[] buf.second;
            }

            thermalvector.clear();

            if(EXIT) {
                vwriter.release();
                break;
            }
        }
    }
}

void wait_key() {
    if(current_image_ptr != nullptr) {
        cv::imshow(WINDOW_NAME, *current_image_ptr->get_image());

        if(RECORD) {
            vwriter << *current_image_ptr->get_image();
        }
    }

    switch(cv::waitKey(5)) {
        case int('q'):
            EXIT = true;
            break;
        case int('r'):
            if(!RECORD) {
                start = std::chrono::system_clock::now();
                vwriter.open("./thermal_" + USER + "_video.avi", CV_FOURCC('M', 'J', 'P', 'G'), 25, cv::Size(w, h));
            }
            RECORD = true;
            break;
        case int('t'):
            THROTTLE = !THROTTLE;
            break;
        case int('s'):
            imager.forceFlagEvent(0);
            break;
        case int('a'):
            AUTOTEMP = true;
            break;
        default:
            break;
    }

    if(!RECORD && !EXIT) {
        cv::displayOverlay(WINDOW_NAME, "Record: off (press 'r' to record, 'q' to quit)", 0);
    }
    if(RECORD && !EXIT) {
        cv::displayOverlay(WINDOW_NAME, "Record: on (press 'q' to finish)", 0);
    }
    if(EXIT) {
        cv::displayOverlay(WINDOW_NAME, "Record: on (EXITING.)", 0);
    }
}

void onThermalFrame(unsigned short* thermal, unsigned int w, unsigned int h, evo::IRFrameMetadata meta, void* arg) {
    delete current_image_ptr;

    auto palette = evo::eIron;

    evo::ImageBuilder imgbuilder = evo::ImageBuilder(false);
    imgbuilder.setPalette(palette);
    imgbuilder.setPaletteScalingMethod(evo::eManual);
    imgbuilder.setManualTemperatureRange(MIN_TEMP, MAX_TEMP);

    imgbuilder.setData(w, h, thermal);

    std::vector<unsigned char> mydata = std::vector<unsigned char>(w * h * 3);

    imgbuilder.convertTemperatureToPaletteImage(&mydata[0], true);
    imgbuilder.setData(w, h, thermal);

    const cv::Point middle = cv::Point(imgbuilder.getWidth()/2, imgbuilder.getHeight()/2);
    float rect_max_temp = 0, global_max_temp = 0;
    for(int i = 0; i < imgbuilder.getWidth(); i++) {
        for(int j = 0; j < imgbuilder.getHeight(); j++) {
            cv::Point current = cv::Point(i, j);

            if(imgbuilder.getTemperatureAt(i, j) > global_max_temp) {
                global_max_temp = imgbuilder.getTemperatureAt(i, j);
            }

            //if(j > imgbuilder.getHeight() * 0.75 && i > imgbuilder.getWidth() * 0.33 && i < imgbuilder.getWidth() * 0.66) { // RECTANGLE DEFINITION
            if(cv::norm(middle-current) < 50) { // Distance from middle point
                float temp = imgbuilder.getTemperatureAt(i, j);
                if(temp > rect_max_temp) {
                    rect_max_temp = temp;
                }
            }
        }
    }

    if(AUTOTEMP){
        AUTOTEMP = false;
        MAX_TEMP = global_max_temp + 1;
        MIN_TEMP = global_max_temp - 7;
        cv::setTrackbarPos("MAX_TEMP", WINDOW_NAME, MAX_TEMP);
        cv::setTrackbarPos("MIN_TEMP", WINDOW_NAME, MIN_TEMP);
    }

    if(!RECORD) {
        cv::displayStatusBar(WINDOW_NAME, "Middlepoint: " + std::to_string((int) rect_max_temp) + "°, Global: " +
                                     std::to_string((int) global_max_temp) + "°", 0);
    } else {
        auto time = std::chrono::duration_cast<std::chrono::seconds>(std::chrono::system_clock::now() - start);
        std::string time_str = std::to_string(time.count());
        cv::displayStatusBar(WINDOW_NAME, "Middlepoint: " + std::to_string((int) rect_max_temp) + "°, Global: " +
                                     std::to_string((int) global_max_temp) + "°, Rec: " + time_str + "s", 0);
    }

    cv::Mat cv_img(cv::Size(w, h), CV_8UC3, &mydata[0], cv::Mat::AUTO_STEP);
    cv::cvtColor(cv_img, cv_img, CV_BGR2RGB);

    current_image_ptr = new Image(cv_img, "live");
    wait_key();
}


/**
 * This function initializes the thermal imager due to a given config file.
 * Furthermore, a callback function is set which is called on each new thermal frame.
 * The code is based on an example of libirimager.
 * @param conf: a path on the filesystem to the conf.xml-file, which is downloaded by `$ ir_download_calibration`.
 */
void init_optris(std::string conf) {
    evo::IRLogger::setVerbosity(evo::IRLOG_ERROR, evo::IRLOG_OFF);
    evo::IRDeviceParamsReader::readXML(conf.c_str(), params);
    device = evo::IRDevice::IRCreateDevice(params);

    if(imager.init(&params,
                   device->getFrequency(),
                   device->getWidth(),
                   device->getHeight(),
                   device->controlledViaHID())) {
        w = imager.getWidth();
        h = imager.getHeight();

        if (w == 0 || h == 0) {
            std::cout
                    << "Error: Image streams not available or wrongly configured. "
                    <<   "Check connection of camera and config file."
                    << std::endl;
            return;
        }
        std::cout << "Connected camera, serial: " << device->getSerial()
                  << ", HW(Rev.): " << imager.getHWRevision()
                  << ", FW(Rev.): " << imager.getFWRevision() << std::endl;
        std::cout << "Thermal channel: " << w << "x" << h << "@" << params.framerate << "Hz" << std::endl;
    } else {
        std::cerr << "initialization error" << std::endl;
    }

    if(imager.hasBispectralTechnology()) {
        std::cout << "Visible channel: " << imager.getVisibleWidth() << "x" << imager.getVisibleHeight() << "@"
                  << params.framerate << "Hz" << std::endl;
    }

    if(device->startStreaming() != 0) {
        std::cerr << "Error occurred in starting stream ... aborting. "
                     "You may need to reconnect the camera." << std::endl;
        exit(-1);
    } else {
        std::cout << "Starting stream ... OK" << std::endl;
    }

    imager.setThermalFrameCallback(onThermalFrame);
}

void update_trackbar_min_temp(int new_min_temp, void* userarg){
    MIN_TEMP = new_min_temp;
}

void update_trackbar_max_temp(int new_max_temp, void* userarg) {
    MAX_TEMP = new_max_temp;
}

void init_gui() {
    cv::Mat emptymat = cv::Mat::zeros(h, w, CV_8UC1);
    cv::imshow(WINDOW_NAME, emptymat);

    cv::createTrackbar("MIN_TEMP", WINDOW_NAME, &MIN_TEMP, 100, update_trackbar_min_temp);
    cv::createTrackbar("MAX_TEMP", WINDOW_NAME, &MAX_TEMP, 100, update_trackbar_max_temp);
}


int main(int argc, char** argv) {
    if(argc > 4){
        std::cout << "Usage: " << argv[0] << " <file: conf.xml> <optional: username>" << std::endl;
        return EXIT_FAILURE;
    }

    std::cout << "Hotkeys: " << std::endl;
    std::cout << "  q: quit" << std::endl;
    std::cout << "  r: record" << std::endl;
    std::cout << "  t: throttle (unbind 25 fps)" << std::endl;
    std::cout << "  s: shutter flag" << std::endl;
    std::cout << "  a: auto exposure setting" << std::endl << std::endl;

    if(argc == 1) {
        std::cout << "Notice: Assuming that a \"conf.xml\" exists in this directory." << std::endl << std::endl;
        std::cout << "Please enter a name for the file to save: " << std::flush;
        std::getline(std::cin, USER);

        init_optris(std::string("conf.xml"));
        init_gui();

        record_thermal();
    }

    if(argc == 2) {
        std::cout << "Loading conf.xml: " << std::string([argv[1]]) << std::endl;
        std::cout << "Please enter a name for the file to save: " << std::flush;
        std::getline(std::cin, USER);
    }

    if(argc == 3) {
        USER = std::string(argv[2]);
    }

    init_optris(std::string(argv[1]));
    init_gui();
    record_thermal();

    return 0;
}