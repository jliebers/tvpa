//
// Created by jl on 19.11.18.
//


#ifndef TVPA_IMAGEPROCESSOR_H
#define TVPA_IMAGEPROCESSOR_H

// STL
#include <iostream>
#include <thread>

// OPENCV
#include <cv.hpp>
#include <opencv2/core/mat.hpp>

// CUSTOM
#include "Image.h"
#include "Voronoi.h"

// DEFINEs
//#define DEBUG_DRAW_OUTPUT
//#define USE_THREADED_PPT
//#define DEBUG_MEASURE_TIME

// Forward declaration of enum as of C++11
enum eSkeletonExtractionAlgorithm : short;

#define HAND_LOWER_RGB CV_RGB(50, 0, 0)
#define HAND_UPPER_RGB CV_RGB(255, 255, 255)

#define ENABLE_ROTATION true
#define ENABLE_CENTERING true
#define ENABLE_ZOOMING true
#define ENABLE_DETAILED_DEBUG true

class ImageProcessor {
public:
    /**
     * Processes a frame into a Skeleton. Might throw a std::logic_error on error.
     * @param image a full-width, colored image taken from the Optris in palette `eIron`
     * @param algorithm An algorithm of enum eSkeletonExtractionAlgorithm.
     * @throws std::logic_error
     * @return A skeleton
     */
    cv::Mat process_frame(const cv::Mat* image, eSkeletonExtractionAlgorithm algorithm);

    cv::Mat get_diag_mat();

    cv::Mat thin_skeleton(const cv::Mat* fat_skeleton);
private:
    /**
     * Extracts a hand from the given image and sets the background to black.
     * @param rgb_image IN An image from the Optris PI 450 in RGB colors and the palette `eIron`.
     * @param hand_mask OUT The mask of the hand.
     * @return The extracted hand on a black background.
     */
    cv::Mat extract_hand(const cv::Mat* rgb_image, cv::Mat* hand_mask = nullptr);

    /**
     *
     * @param rgb_hand_image
     * @param hand_mask
     * @param center_of_circle
     * @param center_of_knuckles
     * @return
     */
    cv::Mat extract_roi(const cv::Mat* rgb_hand_image,
                        const cv::Mat* hand_mask,
                        cv::Point2i* center_of_circle = nullptr,
                        cv::Point2i* center_of_knuckles = nullptr);

    cv::Mat extract_skeleton(const cv::Mat* rgb_roi, eSkeletonExtractionAlgorithm algorithm, bool* skip_processing = nullptr);

    cv::Mat filter_skeleton(cv::Mat* thin_skeleton);

    /**
     * Zooms, centers and rotates the skeleton so the fingertips face upwards by given centerline.
     * @param thin_filtered_skeleton The skeleton to rotate.
     * @param centerline A line. First is the center of the skeleton, second is near the fingers.
     * @return
     */
    cv::Mat align_skeleton(const cv::Mat* thin_filtered_skeleton, std::pair<cv::Point2i, cv::Point2i> centerline);

    /**
     * Crops the ROI circle by percent. Must be within [0; 1].
     */
    const double CIRCLE_CROP_FACTOR = 0.95;
    const int DEFECT_MIN_DEPTH = 10;

    cv::Mat m_debug_mat;

    double pt_distance(int p1_x, int p1_y, int p2_x, int p2_y);

    const bool enable_rotation = ENABLE_ROTATION;
    const bool enable_centering = ENABLE_CENTERING;
    const bool enable_zooming = ENABLE_ZOOMING;
    const bool enable_detailed_debug = ENABLE_DETAILED_DEBUG;

    // Settings for Global Threshold
    const int globalthresh_maxval = 255;
    const int globalthresh_thresh = (int) (255 * 0.6);
    const int globalthresh_type = cv::THRESH_BINARY;

    // Settings for Adaptive Threshold
    const int adaptive_maxvalue = 255;
    const int adaptivemethod = cv::ADAPTIVE_THRESH_GAUSSIAN_C;
    const int adaptive_thresholdtype = cv::THRESH_BINARY;
    const int adaptive_blocksize = 47;
    const int adaptive_c = 0;

    // Settings for Edge Detection
    const int edgedetection_threshold1 = 0;
    const int edgedetection_threshold2 = 5;
    const int edgedetection_blurTimes = 20;
    const int edgedetection_kSize = 3;
    const int edgedetection_cannyKernelsize = 3;
    const int edgedetection_noiseremoval_minareasize = 4;
};


#endif //TVPA_IMAGEPROCESSOR_H
