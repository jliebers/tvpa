//
// Created by jl on 18.11.18.
//

#ifndef TVPA_VIDEO_H
#define TVPA_VIDEO_H


#include <string>
#include <vector>
#include <opencv2/videoio.hpp>
#include "Image.h"

#include <boost/filesystem.hpp>

class Video {
public:
    explicit Video(std::string path);
    ~Video();

    Image* get_frame(int index);

    size_t count_frames();

    void do_stack(int start, int stop, int ratio);

    cv::Mat *get_stack();

private:
    std::string m_path;

    size_t m_video_frames;

    std::vector<Image*>* m_image_ptr_vector;

    cv::VideoCapture m_cap;

    void init_cap(std::string path);

    cv::Mat m_stack;
};


#endif //TVPA_VIDEO_H
