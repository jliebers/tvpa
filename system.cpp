//
// Created by jl on 19.04.19.
//

#include <vector>
#include <string>
#include <utility>
#include <iostream>
#include "Image.h"

// Defines if a strict threshold is met.
// If this is commented, a lax threshold is set.
#define STRICT_THRESHOLD
#define ML_CERTAINTY 75

std::string SHAPECOMPARISON_CSV_CACHE = "";
std::string MACHINELEARNING_JSON_CACHE = "";
std::string DATASET = "";

bool PRINT_INFO = false; // Prints information such as progress

class dataset {
public:
    std::vector<std::string> registrations;
    std::vector<std::string> testings;

    dataset(){
        registrations = std::vector<std::string>(0);
        testings = std::vector<std::string>(0);
    }
};

std::string algo2str(eDistanceCalculationAlgorithm algo) {
    switch(algo) {
        case eHausdorff:
            return "HausdorffDistance";
        case eShapeContext:
            return "ShapeContext";
        case eJaccardDistance:
            return "JaccardDist";
        case eHuMoments1:
            return "HU1";
        case eHuMoments2:
            return "HU2";
        case eHuMoments3:
            return "HU3";
        default:
            return "";
    }
}

class experiment {
private:
    unsigned tp = 0, fp = 0, tn = 0, fn = 0, sum = 0;
    double threshold = 0;
    double tpr = 0, fpr = 0, tnr = 0, fnr = 0;
    double precision = 0; //ppv
    eDistanceCalculationAlgorithm method;

public:
    experiment(eDistanceCalculationAlgorithm method) {
        this->method = method;
    }

    experiment(eDistanceCalculationAlgorithm method, double threshold, unsigned tp, unsigned fp, unsigned tn, unsigned fn) {
        this->method = method;
        this->threshold = threshold;

        this->tp = tp;
        this->fp = fp;
        this->tn = tn;
        this->fn = fn;

        // Calculate rates
        this->update();
    }

    void inc_tp() {
        tp++;
        update();
    }

    void inc_fp() {
        fp++;
        update();
    }

    void inc_tn() {
        tn++;
        update();
    }

    void inc_fn() {
        fn++;
        update();
    }

    double get_tpr() {
        if(tp+fn == 0) {
            return -1;
        }

        update();
        return tpr;
    }

    double get_fpr() {
        if(fp+tn == 0) {
            return -1;
        }

        update();
        return fpr;
    }

    double get_tnr() {
        if(tn+fp == 0) {
            return -1;
        }

        update();
        return tnr;
    }

    double get_fnr() {
        if(fn+tp == 0) {
            return -1;
        }

        update();
        return fnr;
    }

    unsigned get_tp() {
        return tp;
    }

    unsigned get_fp() {
        return fp;
    }

    unsigned get_tn() {
        return tn;
    }

    unsigned get_fn() {
        return fn;
    }

    double get_precision() {
        update();
        return precision;
    }

    void update() {
        this->tpr = tp / (double) (tp + fn);
        this->fpr = fp / (double) (fp + tn);
        this->tnr = tn / (double) (tn + fp);
        this->fnr = fn / (double) (fn + tp);
        this->sum = tp + fp + tn + fn;
        this->precision = tp / (double) (tp+fp);
    }

    double get_threshold() {
        return this->threshold;
    }

    void set_threshold(double t) {
        this->threshold = t;
    }

    eDistanceCalculationAlgorithm get_method() {
        return this->method;
    }

    void print() {
        std::cout << " ----- " << std::endl;
        std::cout << "Method: " << algo2str(method) << ", Threshold: " << threshold << std::endl;
        std::cout << "TP:  " << tp << ", TN:  " << tn << ", FP:  " << fp << ", FN:  " << fn << std::endl;
        std::cout << "TPR: " << tpr << ", TNR: " << tnr << ", FPR: " << fpr << ", FNR: " << fnr << std::endl;
        std::cout << " ----- " << std::endl;
    }
};

std::vector<std::vector<experiment>> EXP_RESULTS;
std::vector<experiment> ALL_EXPERIMENTS_V1, ALL_EXPERIMENTS_V2, ALL_EXPERIMENTS_V3, ALL_EXPERIMENTS_V4;

dataset* get_dataset(int which) {
    dataset* g1 = new dataset();
    //dataset* g2 = new dataset();
    //dataset* g3 = new dataset();
    //dataset* all = new dataset();

    // PAPER
    g1->registrations.push_back("P20R5");
    g1->registrations.push_back("P22L5");
    g1->registrations.push_back("P24R5");
    g1->registrations.push_back("P26L5");
    g1->registrations.push_back("P27L6");
    g1->registrations.push_back("P28L6");
    g1->registrations.push_back("P29L6");
    g1->registrations.push_back("P31R6");
    g1->registrations.push_back("P34L5");
    g1->registrations.push_back("P35L5");
    g1->registrations.push_back("P37L5");
    g1->registrations.push_back("P38L5");
    g1->testings.push_back("P24R6");
    g1->testings.push_back("P26L6");
    g1->testings.push_back("P27L5");
    g1->testings.push_back("P28L5");
    g1->testings.push_back("P29L5");
    g1->testings.push_back("P35L6");
    g1->testings.push_back("P22R6");
    g1->testings.push_back("P38R6");
    g1->testings.push_back("P34R6");
    g1->testings.push_back("P31L6");
    g1->testings.push_back("P27R6");
    g1->testings.push_back("P28R6");
    // PAPER


    /*
    // ------------------------------------ g1

    g1.registrations.emplace_back("P02L2");
    g1.registrations.emplace_back("P06L2");
    g1.testings.emplace_back("P02L4");
    g1.testings.emplace_back("P02R2");
    g1.testings.emplace_back("P02R4");
    g1.testings.emplace_back("P06L4");
    g1.testings.emplace_back("P06R2");
    g1.testings.emplace_back("P06R4");

    // ------------------------------------ g2

    for(auto s : g1->registrations) {
        g2->registrations.push_back(s);
    }
    g2.registrations.emplace_back("P01R2");
    g2.registrations.emplace_back("P08R2");

    for(auto s : g1->testings) {
        g2->testings.push_back(s);
    }
    g2.testings.emplace_back("P01R4");
    g2.testings.emplace_back("P08L2");
    g2.testings.emplace_back("P08L4");
    g2.testings.emplace_back("P08R4");
    g2.testings.emplace_back("P09L2");
    g2.testings.emplace_back("P09L4");
    g2.testings.emplace_back("P09R1");
    g2.testings.emplace_back("P09R4");

    // ------------------------------------ g3

    for(auto s : g2->registrations) {
        g3->registrations.push_back(s);
    }

    g3.registrations.emplace_back("P05L2");
    g3.registrations.emplace_back("P05R2");
    g3.registrations.emplace_back("P11L2");

    for(auto s : g2->testings) {
        g3->testings.push_back(s);
    }

    g3.testings.emplace_back("P01L2");
    g3.testings.emplace_back("P01L4");
    g3.testings.emplace_back("P05L4");
    g3.testings.emplace_back("P05R4");
    g3.testings.emplace_back("P07R2");
    g3.testings.emplace_back("P07R4");
    g3.testings.emplace_back("P11L4");
    g3.testings.emplace_back("P11R2");
    g3.testings.emplace_back("P11R3");

    // ------------------------------------ g3

    for(auto s : g3->registrations) {
        all->registrations.push_back(s);
    }

    all.registrations.emplace_back("P03R2");
    all.registrations.emplace_back("P04R1");
    all.registrations.emplace_back("P07L2");
    all.registrations.emplace_back("P12L1");
    all.registrations.emplace_back("P12R1");
    all.registrations.emplace_back("P13L1");
    all.registrations.emplace_back("P14R1");
    all.registrations.emplace_back("P15R1");

    for(auto s : g3->testings) {
        all->testings.push_back(s);
    }

    all.testings.emplace_back("P03L1");
    all.testings.emplace_back("P03L3");
    all.testings.emplace_back("P03R3");
    all.testings.emplace_back("P04L1");
    all.testings.emplace_back("P04L3");
    all.testings.emplace_back("P04R3");
    all.testings.emplace_back("P07L3");
    all.testings.emplace_back("P10L1");
    all.testings.emplace_back("P10L4");
    all.testings.emplace_back("P10R1");
    all.testings.emplace_back("P10R4");
    all.testings.emplace_back("P12L3");
    all.testings.emplace_back("P12R3");
    all.testings.emplace_back("P13L4");
    all.testings.emplace_back("P13R1");
    all.testings.emplace_back("P13R3");
    all.testings.emplace_back("P14L1");
    all.testings.emplace_back("P14L3");
    all.testings.emplace_back("P14R3");
    all.testings.emplace_back("P15L1");
    all.testings.emplace_back("P15L4");
    all.testings.emplace_back("P15R3");
    all.testings.emplace_back("P16L1");
    all.testings.emplace_back("P16L3");
    all.testings.emplace_back("P16R1");
    all.testings.emplace_back("P16R4");*/


    return g1;

    /*switch(which) {
        case 1:
            return g1;
        case 2:
            return g2;
        case 3:
            return g3;
        default:
            return all;
    }*/
}

std::vector<std::string> string_split(const std::string &str, const std::string &delim = "\t") {
    std::vector<std::string> tokens;
    size_t prev = 0, pos = 0;
    do
    {
        pos = str.find(delim, prev);
        if (pos == std::string::npos) pos = str.length();
        std::string token = str.substr(prev, pos-prev);
        if (!token.empty()) tokens.push_back(token);
        prev = pos + delim.length();
    }
    while (pos < str.length() && prev < str.length());
    return tokens;
}

bool string_contains(const std::string &haystack, const std::string &needle){
    return haystack.find(needle) != std::string::npos;
}



double phase1_shapecomparison(std::string a, std::string b, eDistanceCalculationAlgorithm algo) {
    if (SHAPECOMPARISON_CSV_CACHE.empty()) {
        throw std::logic_error("Error: Cached function called but no cache provided (no csv given).");
    } else {
        // get values from cache csv file
        std::ifstream csvin(SHAPECOMPARISON_CSV_CACHE);
        unsigned count = 0;
        for (std::string line; getline(csvin, line); ++count) {
            // split line by tab delimiters
            std::vector<std::string> spl = string_split(line);

            std::string method = spl[0];
            std::string skel_a = spl[1];
            std::string skel_b = spl[2];
            std::string value = spl[3];
            std::string same_gt = spl[4];
            std::string same_id = spl[5];
            std::replace(value.begin(), value.end(), ',', '.');

            if(count == 0 && method == "Method") {  // skip title line in csv file
                continue;
            }
            if(same_id == "WAHR") {
                continue; // skip identity if it exists
            }

            if(string_contains(skel_a, a) && string_contains(skel_b, b) && method == algo2str(algo)) { // found line in csv
                //std::cout << line << std::endl;
                return std::stod(value);
            }
        }
    }

    throw std::logic_error("No value determined in phase1_shapecomparison");
}

/**
 * This is an overlaod to be used with Images. It is prepared but not yet tested, as this file currently is only
 * tested to work on the cached data, for now.
 * But in theory, this should work just as well.
 */
double phase1_shapecomparison(Image a, Image b, eDistanceCalculationAlgorithm algo) {
    if (SHAPECOMPARISON_CSV_CACHE.empty()) { // if cache is not used do comparison
        return a.get_skeleton()->compute_distance(b.get_skeleton(), algo);
    } else { // else fetch data from cache
        return phase1_shapecomparison(a.get_name(), b.get_name(), algo);
    }
}

bool phase2_machinelearning() {
    return false;
}

void print_results(std::vector<experiment> vec, int i) {
    std::cout << "--- VARIANT " << i << " --- " << std::endl << std::endl;

    for(auto v : vec) {
        std::cout << "Method:\t" << algo2str(v.get_method()) << std::endl;
        std::cout << "TP:\t" << v.get_tp() << "\t TPR:\t" << v.get_tpr() << std::endl;
        std::cout << "FP:\t" << v.get_fp() << "\t FPR:\t" << v.get_fpr() << std::endl;
        std::cout << "TN:\t" << v.get_tn() << "\t TNR:\t" << v.get_tnr() << std::endl;
        std::cout << "FN:\t" << v.get_fn() << "\t FNR:\t" << v.get_fnr() << std::endl;
        std::cout << "Thresh: " << v.get_threshold() << ", Precision: " << v.get_precision() << std::endl;
        std::cout << std::endl << std::endl;
    }

    std::cout << "--- END --- " << std::endl << std::endl;
}

void print_tex_graphs(int which = -1) {
    // per variant
    // ds0, ds0 stack, ds1, ds1 stack, ds2, ds2 stack, ds3, ds3 stack
    // per: hd, sc, jc, hu1, hu2, hu3

    std::vector<std::vector<experiment>> ds0_v1, ds0stack_v1, ds1_v1, ds1stack_v1, ds2_v1, ds2stack_v1, ds3_v1, ds3stack_v1;
    std::vector<std::vector<experiment>> ds0_v2, ds0stack_v2, ds1_v2, ds1stack_v2, ds2_v2, ds2stack_v2, ds3_v2, ds3stack_v2;
    std::vector<std::vector<experiment>> ds0_v3, ds0stack_v3, ds1_v3, ds1stack_v3, ds2_v3, ds2stack_v3, ds3_v3, ds3stack_v3;

    std::vector<std::vector<std::vector<experiment>>*> all_exp_v1;
    all_exp_v1.push_back(&ds0_v1);
    all_exp_v1.push_back(&ds0stack_v1);
    all_exp_v1.push_back(&ds1_v1);
    all_exp_v1.push_back(&ds1stack_v1);
    all_exp_v1.push_back(&ds2_v1);
    all_exp_v1.push_back(&ds2stack_v1);
    all_exp_v1.push_back(&ds3_v1);
    all_exp_v1.push_back(&ds3stack_v1);
    for(int i = 0; i < 6; i++) {
        ds0_v1.emplace_back(std::vector<experiment>());
        ds0stack_v1.emplace_back(std::vector<experiment>());
        ds1_v1.emplace_back(std::vector<experiment>());
        ds1stack_v1.emplace_back(std::vector<experiment>());
        ds2_v1.emplace_back(std::vector<experiment>());
        ds2stack_v1.emplace_back(std::vector<experiment>());
        ds3_v1.emplace_back(std::vector<experiment>());
        ds3stack_v1.emplace_back(std::vector<experiment>());
    }

    std::vector<std::vector<std::vector<experiment>>*> all_exp_v2;
    all_exp_v2.push_back(&ds0_v2);
    all_exp_v2.push_back(&ds0stack_v2);
    all_exp_v2.push_back(&ds1_v2);
    all_exp_v2.push_back(&ds1stack_v2);
    all_exp_v2.push_back(&ds2_v2);
    all_exp_v2.push_back(&ds2stack_v2);
    all_exp_v2.push_back(&ds3_v2);
    all_exp_v2.push_back(&ds3stack_v2);
    for(int i = 0; i < 6; i++) {
        ds0_v2.emplace_back(std::vector<experiment>());
        ds0stack_v2.emplace_back(std::vector<experiment>());
        ds1_v2.emplace_back(std::vector<experiment>());
        ds1stack_v2.emplace_back(std::vector<experiment>());
        ds2_v2.emplace_back(std::vector<experiment>());
        ds2stack_v2.emplace_back(std::vector<experiment>());
        ds3_v2.emplace_back(std::vector<experiment>());
        ds3stack_v2.emplace_back(std::vector<experiment>());
    }

    std::vector<std::vector<std::vector<experiment>>*> all_exp_v3;
    all_exp_v3.push_back(&ds0_v3);
    all_exp_v3.push_back(&ds0stack_v3);
    all_exp_v3.push_back(&ds1_v3);
    all_exp_v3.push_back(&ds1stack_v3);
    all_exp_v3.push_back(&ds2_v3);
    all_exp_v3.push_back(&ds2stack_v3);
    all_exp_v3.push_back(&ds3_v3);
    all_exp_v3.push_back(&ds3stack_v3);
    for(int i = 0; i < 6; i++) {
        ds0_v3.emplace_back(std::vector<experiment>());
        ds0stack_v3.emplace_back(std::vector<experiment>());
        ds1_v3.emplace_back(std::vector<experiment>());
        ds1stack_v3.emplace_back(std::vector<experiment>());
        ds2_v3.emplace_back(std::vector<experiment>());
        ds2stack_v3.emplace_back(std::vector<experiment>());
        ds3_v3.emplace_back(std::vector<experiment>());
        ds3stack_v3.emplace_back(std::vector<experiment>());
    }

    auto cmethod = ALL_EXPERIMENTS_V1[0].get_method();
    auto it = ALL_EXPERIMENTS_V1.begin();
    while(it != ALL_EXPERIMENTS_V1.end()) {
        for(int i = 0; i < all_exp_v1.size(); i++) {
            for (int j = 0; j < 6; j++) {
                while (cmethod == it->get_method()) {
                    all_exp_v1[i]->at(j).push_back(*it);
                    ++it;
                }
                cmethod = it->get_method();
            }
        }
    }

    cmethod = ALL_EXPERIMENTS_V2[0].get_method();
    it = ALL_EXPERIMENTS_V2.begin();
    while(it != ALL_EXPERIMENTS_V2.end()) {
        for(int i = 0; i < all_exp_v2.size(); i++) {
            for (int j = 0; j < 6; j++) {
                while (cmethod == it->get_method()) {
                    all_exp_v2[i]->at(j).push_back(*it);
                    ++it;
                }
                cmethod = it->get_method();
            }
        }
    }

    cmethod = ALL_EXPERIMENTS_V3[0].get_method();
    it = ALL_EXPERIMENTS_V3.begin();
    while(it != ALL_EXPERIMENTS_V3.end()) {
        for(int i = 0; i < all_exp_v3.size(); i++) {
            for (int j = 0; j < 6; j++) {
                while (cmethod == it->get_method()) {
                    all_exp_v3[i]->at(j).push_back(*it);
                    ++it;
                }
                cmethod = it->get_method();
            }
        }
    }

    auto print_graph = [](std::vector<std::pair<std::string, std::vector<experiment>>> pair) -> void {
        const std::vector<std::string> clist = {"red", "green", "blue", "cyan", "magenta", "yellow", "black", "gray",
                                                "brown", "lime", "olive", "orange", "purple", "teal", "violet"};
        int col = 0;

        std::cout << "\\begin{tikzpicture}\n"
                     "\\begin{axis}[\n"
                     "    width=10cm,\n"
                     "    height=10cm,\n"
                     "\txlabel={False positive rate},\n"
                     "\tylabel={True positive rate},\n"
                     "\tytick={0,0.2,0.4,0.6,0.8,1},\n"
                     "\txtick={0,0.2,0.4,0.6,0.8,1},\n"
                     "\t%grid=major,\n"
                     "    %major grid style={line width=.2pt,draw=gray!30},  \n"
                     "    %ymajorgrids,\n"
                     "    legend cell align={left},\n"
                     "    legend pos=outer north east,\n"
                     "]\n"
                     "\n";

        std::cout << "\\addplot[mark=, black, dashed] coordinates {\n"
                     "(0,0)\n"
                     "(1,1)\n"
                     "};" << std::endl;

        double linewidth = 0.33 * (double) (pair.size() + 1);


        for(auto p : pair) {
            double avg_dist = 0;
            linewidth -= 0.33;
            std::cout << "\\addplot[const plot,mark=, " << clist[col++ % clist.size()] << ", line width=" << linewidth << "pt] coordinates {\n";
            std::cout << "(0,0)\n";
            for(auto exp : p.second) {
                std::cout << "(";
                std::cout << exp.get_fpr();
                std::cout << ",";
                std::cout << exp.get_tpr();
                std::cout << ")" << std::endl;
                avg_dist += std::sqrt(std::pow((exp.get_fpr() -0), 2) + std::pow((exp.get_tpr() -1), 2));
            }
            std::cout << "(1,1)\n";
            std::cout << "};" << "% avg euclidean dist to (0,1): " << avg_dist / pair.size() << std::endl << std::endl;
        }

        // print legend
        std::cout << "\\legend{,";  // place comma to skip first dashed line in legend
        for(auto p : pair) {
            std::cout << "\\scriptsize " << p.first << ",";
        }
        std::cout << "}\n"
                     "\\end{axis}\n"
                     "\\end{tikzpicture}" << std::endl;
    };

    std::vector<std::pair<std::string, std::vector<experiment>>> plot0, plot1, plot2, plot3;

    const int Hausdorff = 0;
    const int Shapecontext = 1;
    const int Jaccard = 2;
    const int Hu1 = 3;
    const int Hu2 = 4;
    const int Hu3 = 5;

#ifdef STRICT_THRESHOLD
    plot0.emplace_back(std::make_pair("I./All Hausdorff-distance", ds0_v1[Hausdorff]));
    plot0.emplace_back(std::make_pair("II./All Hu1",               ds0_v2[Hu1]));
    plot0.emplace_back(std::make_pair("II./All Hu3",               ds0_v2[Hu3]));
    plot0.emplace_back(std::make_pair("II./All$\\Delta$ Hu2",      ds0stack_v2[Hu2]));
    plot0.emplace_back(std::make_pair("III./All Hu1",              ds0_v3[Hu1]));
    plot0.emplace_back(std::make_pair("III./All Hu3",              ds0_v3[Hu3]));
    plot0.emplace_back(std::make_pair("III./All$\\Delta$ Hu2",     ds0stack_v3[Hu2]));

    plot1.emplace_back(std::make_pair("I./G1 Hausdorff",             ds1_v1[Hausdorff]));
    plot1.emplace_back(std::make_pair("I./G1$\\Delta$ Shapecontext", ds1stack_v1[Shapecontext]));
    plot1.emplace_back(std::make_pair("II./G1 Shapecontext",         ds1_v2[Shapecontext]));
    plot1.emplace_back(std::make_pair("II./G1 Hu2",                  ds1_v2[Hu2]));
    plot1.emplace_back(std::make_pair("II./G1$\\Delta$ Jaccard",     ds1stack_v2[Jaccard]));
    plot1.emplace_back(std::make_pair("III./G1 Shapecontext",        ds1_v3[Shapecontext]));
    plot1.emplace_back(std::make_pair("III./G1 Hu2",                 ds1_v3[Hu2]));
    plot1.emplace_back(std::make_pair("III./G1$\\Delta$ Jaccard",    ds1stack_v3[Jaccard]));

    plot2.emplace_back(std::make_pair("I./G2 Jaccard",               ds2_v1[Jaccard]));
    plot2.emplace_back(std::make_pair("I./G2$\\Delta$ Shapecontext", ds2stack_v1[Shapecontext]));
    plot2.emplace_back(std::make_pair("I./G2$\\Delta$ Jaccard",      ds2stack_v1[Jaccard]));
    plot2.emplace_back(std::make_pair("II./G2 Shapecontext",         ds2_v2[Shapecontext]));
    plot2.emplace_back(std::make_pair("III./G2 Shapecontext",        ds2_v3[Shapecontext]));

    plot3.emplace_back(std::make_pair("I./G3 Jaccard",               ds3_v1[Jaccard]));
    plot3.emplace_back(std::make_pair("I./G3$\\Delta$ Shapecontext", ds3stack_v1[Shapecontext]));
    plot3.emplace_back(std::make_pair("I./G3$\\Delta$ Jaccard",      ds3stack_v1[Jaccard]));
    plot3.emplace_back(std::make_pair("II./G3 Shapecontext",         ds3_v2[Shapecontext]));
    plot3.emplace_back(std::make_pair("II./G3$\\Delta$ Hu3",         ds3stack_v2[Hu3]));
    plot3.emplace_back(std::make_pair("III./G3 Shapecontext",        ds3_v3[Shapecontext]));
    plot3.emplace_back(std::make_pair("III./G3$\\Delta$ Hu3",        ds3stack_v3[Hu3]));
#else
    plot0.emplace_back(std::make_pair("I./All Hausdorff-distance",               ds0_v1[Hausdorff]));
    plot0.emplace_back(std::make_pair("I./All$\\Delta$ Shapecontext-distance",   ds0stack_v1[Shapecontext]));
    plot0.emplace_back(std::make_pair("I./All$\\Delta$ Jaccard-distance",        ds0stack_v1[Jaccard]));
    plot0.emplace_back(std::make_pair("II./All Hausdorff-distance",              ds0_v2[Hausdorff]));
    plot0.emplace_back(std::make_pair("II./All$\\Delta$ Shapecontext-distance",  ds0stack_v2[Shapecontext]));
    plot0.emplace_back(std::make_pair("II./All$\\Delta$ Jaccard-distance",       ds0stack_v2[Jaccard]));
    plot0.emplace_back(std::make_pair("III./All Hu1-distance",                   ds0_v3[Hu1]));
    plot0.emplace_back(std::make_pair("III./All Hu3-distance",                   ds0_v3[Hu3]));
    plot0.emplace_back(std::make_pair("III./All$\\Delta$ Shapecontext-distance", ds0stack_v3[Shapecontext]));

    plot1.emplace_back(std::make_pair("I./G1 Shapecontext",            ds1_v1[Shapecontext]));
    plot1.emplace_back(std::make_pair("I./G1 Hu2",                     ds1_v1[Hu2]));
    plot1.emplace_back(std::make_pair("I./G1$\\Delta$ Shapecontext",   ds1stack_v1[Shapecontext]));
    plot1.emplace_back(std::make_pair("I./G1$\\Delta$ Hu2",            ds1stack_v1[Hu2]));
    plot1.emplace_back(std::make_pair("I./G1$\\Delta$ Hu3",            ds1stack_v1[Hu3]));
    plot1.emplace_back(std::make_pair("II./G1 Shapecontext",           ds1_v2[Shapecontext]));
    plot1.emplace_back(std::make_pair("II./G1 Hu2",                    ds1_v2[Hu2]));
    plot1.emplace_back(std::make_pair("II./G1$\\Delta$ Shapecontext",  ds1stack_v2[Shapecontext]));
    plot1.emplace_back(std::make_pair("II./G1$\\Delta$ Hu1",           ds1stack_v2[Hu1]));
    plot1.emplace_back(std::make_pair("II./G1$\\Delta$ Hu2",           ds1stack_v2[Hu2]));
    plot1.emplace_back(std::make_pair("II./G1$\\Delta$ Hu3",           ds1stack_v2[Hu3]));
    plot1.emplace_back(std::make_pair("III./G1 Shapecontext",          ds1_v3[Shapecontext]));
    plot1.emplace_back(std::make_pair("III./G1 Hu2",                   ds1_v3[Hu2]));
    plot1.emplace_back(std::make_pair("III./G1$\\Delta$ Shapecontext", ds1stack_v3[Shapecontext]));
    plot1.emplace_back(std::make_pair("III./G1$\\Delta$ Hu1",          ds1stack_v3[Hu1]));
    plot1.emplace_back(std::make_pair("III./G1$\\Delta$ Hu2",          ds1stack_v3[Hu2]));
    plot1.emplace_back(std::make_pair("III./G1$\\Delta$ Hu3",          ds1stack_v3[Hu3]));

    plot2.emplace_back(std::make_pair("I./G2 Hausdorff",               ds2_v1[Hausdorff]));
    plot2.emplace_back(std::make_pair("I./G2 Jaccard",                 ds2_v1[Jaccard]));
    plot2.emplace_back(std::make_pair("I./G2$\\Delta$ Hausdorff",      ds2stack_v1[Hausdorff]));
    plot2.emplace_back(std::make_pair("I./G2$\\Delta$ Shapecontext",   ds2stack_v1[Shapecontext]));
    plot2.emplace_back(std::make_pair("I./G2$\\Delta$ Jaccard",        ds2stack_v1[Jaccard]));
    plot2.emplace_back(std::make_pair("II./G2 Hausdorff",              ds2_v2[Hausdorff]));
    plot2.emplace_back(std::make_pair("II./G2 Jaccard",                ds2_v2[Jaccard]));
    plot2.emplace_back(std::make_pair("II./G2$\\Delta$ Hausdorff",     ds2stack_v2[Hausdorff]));
    plot2.emplace_back(std::make_pair("II./G2$\\Delta$ Shapecontext",  ds2stack_v2[Shapecontext]));
    plot2.emplace_back(std::make_pair("II./G2$\\Delta$ Jaccard",       ds2stack_v2[Jaccard]));
    plot2.emplace_back(std::make_pair("III./G2 Hausdorff",             ds2_v3[Hausdorff]));
    plot2.emplace_back(std::make_pair("III./G2 Jaccard",               ds2_v3[Jaccard]));
    plot2.emplace_back(std::make_pair("III./G2$\\Delta$ Hausdorff",    ds2stack_v3[Hausdorff]));
    plot2.emplace_back(std::make_pair("III./G2$\\Delta$ Shapecontext", ds2stack_v3[Shapecontext]));
    plot2.emplace_back(std::make_pair("III./G2$\\Delta$ Jaccard",      ds2stack_v3[Jaccard]));

    plot3.emplace_back(std::make_pair("I./G3 Hausdorff",               ds3_v1[Hausdorff]));
    plot3.emplace_back(std::make_pair("I./G3$\\Delta$ Shapecontext",   ds3stack_v1[Shapecontext]));
    plot3.emplace_back(std::make_pair("I./G3$\\Delta$ Jaccard",        ds3stack_v1[Jaccard]));
    plot3.emplace_back(std::make_pair("II./G3 Hausdorff",              ds3_v2[Hausdorff]));
    plot3.emplace_back(std::make_pair("II./G3$\\Delta$ Shapecontext",  ds3stack_v2[Shapecontext]));
    plot3.emplace_back(std::make_pair("II./G3$\\Delta$ Jaccard",       ds3stack_v2[Jaccard]));
    plot3.emplace_back(std::make_pair("III./G3 Hausdorff",             ds3_v3[Hausdorff]));
    plot3.emplace_back(std::make_pair("III./G3 Shapecontext",          ds3_v3[Shapecontext]));
    plot3.emplace_back(std::make_pair("III./G3$\\Delta$ Shapecontext", ds3stack_v3[Shapecontext]));
#endif

    if(which == -1 || which == 0)
        print_graph(plot0);
    if(which == -1 || which == 1)
        print_graph(plot1);
    if(which == -1 || which == 2)
        print_graph(plot2);
    if(which == -1 || which == 3)
        print_graph(plot3);

#ifdef STRICT_THRESHOLD
    std::cout << std::endl << "% End - Strict Threshold" << std::endl;
#else
    std::cout << std::endl << "% End - Lax Threshold" << std::endl;
#endif
}

void print_tex_results() {
    std::cout << std::fixed;
    std::cout << std::setprecision(2);
    const std::string d = " & "; // delim
    const std::string end = " \\\\ ";

    auto print_row = [d, end] (std::string s, std::vector<experiment> e, int offset = 0) -> void {
        std::cout << s;
        for(int i = 0+offset; i <= 2+offset; i++) {
            std::cout << d << e[i].get_tp()  << d << e[i].get_fp()  << d << e[i].get_tn()  << d << e[i].get_fn();
            std::cout << d << e[i].get_tpr() << d << e[i].get_fpr() << d << e[i].get_tnr() << d << e[i].get_fnr();
        }
        std::cout << end  << "% PPVs are: " << e[0+offset].get_precision() << ", " << e[1+offset].get_precision() << ", " << e[2+offset].get_precision();
        std::cout << std::endl;
    };

    // hd, sc, jc
    std::cout << "\\begin{sidewaystable}\n"
                 "\\footnotesize\n"
                 "\\centering\n"
                 "\\scalebox{0.925}{%\n"
                 "\\begin{tabular}{lcccccccccccccccccccccccc}\n"
                 "\\toprule\n"
                 "\n"
                 "& \\multicolumn{8}{c}{\\textbf{Hausdorff-distance}} & \\multicolumn{8}{c}{\\textbf{Shapecontext-distance}} & \\multicolumn{8}{c}{\\textbf{Jaccard-distance}} \\\\\n"
                 "\n"
                 "\\cmidrule(r){2-9}\\cmidrule(lr){5-7}\\cmidrule(lr){10-17}\\cmidrule(lr){18-25}\n"
                 "\n"
                 "\\textbf{Dataset} & \\textbf{TP} & \\textbf{FP} & \\textbf{TN} & \\textbf{FN} & \\textbf{TPR} & \\textbf{FPR} & \\textbf{TNR} & \\textbf{FNR} & \\textbf{TP} & \\textbf{FP} & \\textbf{TN} & \\textbf{FN} & \\textbf{TPR} & \\textbf{FPR} & \\textbf{TNR} & \\textbf{FNR} & \\textbf{TP} & \\textbf{FP} & \\textbf{TN} & \\textbf{FN} & \\textbf{TPR} & \\textbf{FPR} & \\textbf{TNR} & \\textbf{FNR} \\\\\n"
                 "\n"
                 "\\midrule" << std::endl;

    print_row("I. / All", EXP_RESULTS[0]);
    print_row("I. / All$\\Delta$", EXP_RESULTS[3]);
    print_row("I. / G1", EXP_RESULTS[6]);
    print_row("I. / G1$\\Delta$", EXP_RESULTS[9]);
    print_row("I. / G2", EXP_RESULTS[12]);
    print_row("I. / G2$\\Delta$", EXP_RESULTS[15]);
    print_row("I. / G3", EXP_RESULTS[18]);
    print_row("I. / G3$\\Delta$", EXP_RESULTS[21]);
    std::cout << "\\midrule" << std::endl;
    print_row("II. / All", EXP_RESULTS[1]);
    print_row("II. / All$\\Delta$", EXP_RESULTS[4]);
    print_row("II. / G1", EXP_RESULTS[7]);
    print_row("II. / G1$\\Delta$", EXP_RESULTS[10]);
    print_row("II. / G2", EXP_RESULTS[13]);
    print_row("II. / G2$\\Delta$", EXP_RESULTS[16]);
    print_row("II. / G3", EXP_RESULTS[19]);
    print_row("II. / G3$\\Delta$", EXP_RESULTS[22]);
    std::cout << "\\midrule" << std::endl;
    print_row("III. / All", EXP_RESULTS[2]);
    print_row("III. / All$\\Delta$", EXP_RESULTS[5]);
    print_row("III. / G1", EXP_RESULTS[8]);
    print_row("III. / G1$\\Delta$", EXP_RESULTS[11]);
    print_row("III. / G2", EXP_RESULTS[14]);
    print_row("III. / G2$\\Delta$", EXP_RESULTS[17]);
    print_row("III. / G3", EXP_RESULTS[20]);
    print_row("III. / G3$\\Delta$", EXP_RESULTS[23]);

    std::cout << "\\bottomrule \n"
                 "\\end{tabular}\n"
                 "}\n"
                 "\n"
                 "\\caption{TODO CAPTION}\n"
                 "\\label{tab:TODOLABEL}\n"
                 "\\end{sidewaystable}" << std::endl;


    std::cout << std::endl << std::endl << std::endl << std::endl;

    // hu1, hu2, hu3
    std::cout << "\\begin{sidewaystable}\n"
                 "\\footnotesize\n"
                 "\\centering\n"
                 "\\scalebox{0.925}{%\n"
                 "\\begin{tabular}{lcccccccccccccccccccccccc}\n"
                 "\\toprule\n"
                 "\n"
                 "& \\multicolumn{8}{c}{\\textbf{Hu1-distance}} & \\multicolumn{8}{c}{\\textbf{Hu2-distance}} & \\multicolumn{8}{c}{\\textbf{Hu3-distance}} \\\\\n"
                 "\n"
                 "\\cmidrule(r){2-9}\\cmidrule(lr){5-7}\\cmidrule(lr){10-17}\\cmidrule(lr){18-25}\n"
                 "\n"
                 "\\textbf{Dataset} & \\textbf{TP} & \\textbf{FP} & \\textbf{TN} & \\textbf{FN} & \\textbf{TPR} & \\textbf{FPR} & \\textbf{TNR} & \\textbf{FNR} & \\textbf{TP} & \\textbf{FP} & \\textbf{TN} & \\textbf{FN} & \\textbf{TPR} & \\textbf{FPR} & \\textbf{TNR} & \\textbf{FNR} & \\textbf{TP} & \\textbf{FP} & \\textbf{TN} & \\textbf{FN} & \\textbf{TPR} & \\textbf{FPR} & \\textbf{TNR} & \\textbf{FNR} \\\\\n"
                 "\t\n"
                 "\\midrule" << std::endl;

    print_row("I. / All", EXP_RESULTS[0], 3);
    print_row("I. / All$\\Delta$", EXP_RESULTS[3], 3);
    print_row("I. / G1", EXP_RESULTS[6], 3);
    print_row("I. / G1$\\Delta$", EXP_RESULTS[9], 3);
    print_row("I. / G2", EXP_RESULTS[12], 3);
    print_row("I. / G2$\\Delta$", EXP_RESULTS[15], 3);
    print_row("I. / G3", EXP_RESULTS[18], 3);
    print_row("I. / G3$\\Delta$", EXP_RESULTS[21], 3);
    std::cout << "\\midrule" << std::endl;
    print_row("II. / All", EXP_RESULTS[1], 3);
    print_row("II. / All$\\Delta$", EXP_RESULTS[4], 3);
    print_row("II. / G1", EXP_RESULTS[7], 3);
    print_row("II. / G1$\\Delta$", EXP_RESULTS[10], 3);
    print_row("II. / G2", EXP_RESULTS[13], 3);
    print_row("II. / G2$\\Delta$", EXP_RESULTS[16], 3);
    print_row("II. / G3", EXP_RESULTS[19], 3);
    print_row("II. / G3$\\Delta$", EXP_RESULTS[22], 3);
    std::cout << "\\midrule" << std::endl;
    print_row("III. / All", EXP_RESULTS[2], 3);
    print_row("III. / All$\\Delta$", EXP_RESULTS[5], 3);
    print_row("III. / G1", EXP_RESULTS[8], 3);
    print_row("III. / G1$\\Delta$", EXP_RESULTS[11], 3);
    print_row("III. / G2", EXP_RESULTS[14], 3);
    print_row("III. / G2$\\Delta$", EXP_RESULTS[17], 3);
    print_row("III. / G3", EXP_RESULTS[20], 3);
    print_row("III. / G3$\\Delta$", EXP_RESULTS[23], 3);

    std::cout << "\\bottomrule \n"
                 "\\end{tabular}\n"
                 "}\n"
                 "\\caption{TODO CAPTION}\n"
                 "\\label{tab:TODOLABEL}\n"
                 "\\end{sidewaystable}" << std::endl;
}



/**
 * Quick and dirty json parser, works only with intended json.
 * In prod, the system shall instead call DIGITS on the fly.
 * For now, only the cache from a val.txt-file and its resulting json is tested.
 * @param a
 * @param b
 * @return
 */
std::pair<std::string, double> phase2_imclassification(const std::string &s) {
    if (MACHINELEARNING_JSON_CACHE.empty()) {
        throw std::logic_error("Error: Cached function called but no cache provided (no json given).");
    } else {
        // get values from cache csv file
        std::ifstream csvin(MACHINELEARNING_JSON_CACHE);
        unsigned long count = 0;
        for (std::string line; getline(csvin, line); ++count) {
            if(string_contains(line, s)) {
                // skip next line
                getline(csvin, line);

                std::string cl, val;
                getline(csvin, cl);
                getline(csvin, val);

                char sanitize[] = " \",";
                for (unsigned int i = 0; i < strlen(sanitize); ++i) {
                    // you need include <algorithm> to use general algorithms like std::remove()
                    cl.erase(std::remove(cl.begin(), cl.end(), sanitize[i]), cl.end());
                    val.erase(std::remove(val.begin(), val.end(), sanitize[i]), val.end());
                }

                //std::cout << cl << std::endl;
                //std::cout << stod(val) << std::endl << std::endl;
                return std::pair<std::string, double>(cl, std::stod(val));
            }
        }
    }

    throw std::logic_error("No value determined in phase2_imclassification for " + s + ". Check json file if it exists.");
}

double phase2_imclassification(Image a, Image b) {
    throw std::logic_error("phase2_imclassification for images is not implemented yet.");
    /*
     * In PROD, here shall be a code that calls the ReST-API of DIGITS.
     * For now, only the version working with cached data is implemented.
     */
}

void remove_duplicates(std::vector<double> &v) {
    auto end = v.end();
    for (auto it = v.begin(); it != end; ++it) {
        end = std::remove(it + 1, end, *it);
    }

    v.erase(end, v.end());
}

/**
 * Prints a bash command to symlink val and train in DIGITS
 * @param ds a dataset
 */
void print_ln(dataset ds) {
    std::cout << "Reg: " << std::endl;
    std::cout << "for f in ";
    for(auto s : ds.registrations) {
        std::cout << s << " ";
    }
    std::cout << "; do mkdir -p ${f:0:4} && ln -s ../../../../${f}/frames/skeleton ${f:0:4}/${f}; done" << std::endl;

    std::cout << "Val: " << std::endl;
    std::cout << "for f in ";
    for(auto s : ds.testings) {
        std::cout << s << " ";
    }
    std::cout << "; do mkdir -p ${f:0:4} && ln -s ../../../../${f}/frames/skeleton ${f:0:4}/${f}; done" << std::endl;
}

void plot_fnr_far_curves(std::vector<experiment> exps) {
    std::vector<experiment> exps_hd;
    std::vector<experiment> exps_sc;
    std::vector<experiment> exps_jc;
    std::vector<experiment> exps_hu1;
    std::vector<experiment> exps_hu2;
    std::vector<experiment> exps_hu3;

    for(auto e : exps) {
     switch(e.get_method()){
         case eHausdorff:
             exps_hd.push_back(e);
             break;
         case eShapeContext:
             exps_sc.push_back(e);
             break;
         case eJaccardDistance:
             exps_jc.push_back(e);
             break;
         case eHuMoments1:
             exps_hu1.push_back(e);
             break;
         case eHuMoments2:
             exps_hu2.push_back(e);
             break;
         case eHuMoments3:
             exps_hu3.push_back(e);
             break;
         default:
             return;
     }
    }

    // TODO plot graphs

    // unused
}

void calculate(dataset* dsptr) {
    auto ds = *dsptr;
    std::vector<std::tuple<std::string, std::string, double>> phase1_results_hausdorff;
    std::vector<std::tuple<std::string, std::string, double>> phase1_results_shapecontext;
    std::vector<std::tuple<std::string, std::string, double>> phase1_results_jaccard;
    std::vector<std::tuple<std::string, std::string, double>> phase1_results_hu1;
    std::vector<std::tuple<std::string, std::string, double>> phase1_results_hu2;
    std::vector<std::tuple<std::string, std::string, double>> phase1_results_hu3;

    std::vector<double> phase1_compvalues_hausdorff;
    std::vector<double> phase1_compvalues_shapecontext;
    std::vector<double> phase1_compvalues_jaccard;
    std::vector<double> phase1_compvalues_hu1;
    std::vector<double> phase1_compvalues_hu2;
    std::vector<double> phase1_compvalues_hu3;

    // generate all possible experiments to find all possible thresholds
    unsigned cnt = 0;
    for (const auto &reg : ds.registrations) {
        for (const auto &test : ds.testings) {
            auto p1_c1 = std::tuple<std::string, std::string, double>(reg, test, phase1_shapecomparison(reg, test, eHausdorff));
            auto p1_c2 = std::tuple<std::string, std::string, double>(reg, test, phase1_shapecomparison(reg, test, eShapeContext));
            auto p1_c3 = std::tuple<std::string, std::string, double>(reg, test, phase1_shapecomparison(reg, test, eJaccardDistance));
            auto p1_c4 = std::tuple<std::string, std::string, double>(reg, test, phase1_shapecomparison(reg, test, eHuMoments1));
            auto p1_c5 = std::tuple<std::string, std::string, double>(reg, test, phase1_shapecomparison(reg, test, eHuMoments2));
            auto p1_c6 = std::tuple<std::string, std::string, double>(reg, test, phase1_shapecomparison(reg, test, eHuMoments3));

            phase1_results_hausdorff.push_back(p1_c1);
            phase1_results_shapecontext.push_back(p1_c2);
            phase1_results_jaccard.push_back(p1_c3);
            phase1_results_hu1.push_back(p1_c4);
            phase1_results_hu2.push_back(p1_c5);
            phase1_results_hu3.push_back(p1_c6);

            phase1_compvalues_hausdorff.push_back(std::get<2>(p1_c1));
            phase1_compvalues_shapecontext.push_back(std::get<2>(p1_c2));
            phase1_compvalues_jaccard.push_back(std::get<2>(p1_c3));
            phase1_compvalues_hu1.push_back(std::get<2>(p1_c4));
            phase1_compvalues_hu2.push_back(std::get<2>(p1_c5));
            phase1_compvalues_hu3.push_back(std::get<2>(p1_c6));

            if (PRINT_INFO) {
                std::cout << "Info: Processing " << cnt << "/" << ds.registrations.size() * ds.testings.size() <<
                          " - " << (cnt / (double) (ds.registrations.size() * ds.testings.size())) * 100 << " %"
                          << std::endl;
            }
            ++cnt;
        }
    }

    // remove duplicates
    if (PRINT_INFO)
        std::cout << "Info: removing duplicates ... " << std::flush;
    remove_duplicates(phase1_compvalues_hausdorff);
    remove_duplicates(phase1_compvalues_shapecontext);
    remove_duplicates(phase1_compvalues_jaccard);
    remove_duplicates(phase1_compvalues_hu1);
    remove_duplicates(phase1_compvalues_hu2);
    remove_duplicates(phase1_compvalues_hu3);
    if (PRINT_INFO)
        std::cout << "done." << std::endl;


    // sort
    if(PRINT_INFO)
        std::cout << "Info: sorting all ... " << std::flush;
    std::sort(phase1_compvalues_hausdorff.begin(), phase1_compvalues_hausdorff.end());
    std::sort(phase1_compvalues_shapecontext.begin(), phase1_compvalues_shapecontext.end());
    std::sort(phase1_compvalues_jaccard.begin(), phase1_compvalues_jaccard.end());
    std::sort(phase1_compvalues_hu1.begin(), phase1_compvalues_hu1.end());
    std::sort(phase1_compvalues_hu2.begin(), phase1_compvalues_hu2.end());
    std::sort(phase1_compvalues_hu3.begin(), phase1_compvalues_hu3.end());
    if(PRINT_INFO)
        std::cout << "done." << std::endl;

    // Determine EER for all six methods:
    eDistanceCalculationAlgorithm algos[6] = {eHausdorff, eShapeContext, eJaccardDistance, eHuMoments1, eHuMoments2,
                                              eHuMoments3};
    std::vector<std::tuple<std::string, std::string, double>> *experiment_vecs[6] = {
            &phase1_results_hausdorff,
            &phase1_results_shapecontext,
            &phase1_results_jaccard,
            &phase1_results_hu1,
            &phase1_results_hu2,
            &phase1_results_hu3
    };
    std::vector<double> *threshold_vecs[6] = {
            &phase1_compvalues_hausdorff,
            &phase1_compvalues_shapecontext,
            &phase1_compvalues_jaccard,
            &phase1_compvalues_hu1,
            &phase1_compvalues_hu2,
            &phase1_compvalues_hu3
    };

    std::vector<experiment> exps;
    std::vector<experiment> optimal_precision_exps;

    auto reg_contains = [ds](std::string candidate) -> bool {
        for (auto reg : ds.registrations) {
            auto reg_gt = reg.substr(0, 4);
            if (reg_gt == candidate.substr(0, 4)) {
                return true;
            }
        }
        return false;
    };

    if(PRINT_INFO)
        std::cout << "Info: generating experiments ... " << std::endl;

    // determine optimal eer from possible thresholds
    for (int i = 0; i < 6; i++) { // for each method
        auto method = algos[i];
        auto experiments = experiment_vecs[i];

        for (auto tested_threshold : *threshold_vecs[i]) {
            unsigned tp = 0, fp = 0, tn = 0, fn = 0;
            unsigned tp_v1 = 0, fp_v1 = 0, tn_v1 = 0, fn_v1 = 0;
            unsigned tp_v2 = 0, fp_v2 = 0, tn_v2 = 0, fn_v2 = 0;
            unsigned tp_v3 = 0, fp_v3 = 0, tn_v3 = 0, fn_v3 = 0;
            unsigned tp_v4 = 0, fp_v4 = 0, tn_v4 = 0, fn_v4 = 0;

            for (const auto &test : ds.testings) {
                std::string reg_candidate;
                double distance = std::numeric_limits<double>::max();
                for (const auto &reg : ds.registrations) {

                    // for test matched onto reg, find closest distance as reg candidate
                    for (auto e : *experiments) {
                        std::string col1 = std::get<0>(e);
                        std::string col2 = std::get<1>(e);
                        double col3 = std::get<2>(e);

                        if (col1 == reg && col2 == test) {
                            if (col3 < distance) {
                                distance = col3;
                                reg_candidate = reg;
                            }
                        }
                    }
                }
                // candidate found
                auto gt_test = test.substr(0, 4);
                auto gt_reg = reg_candidate.substr(0, 4);

                // find candidate phase 2 (for graph plots)
                auto pair = phase2_imclassification(test);
                auto reg_candidate_phase2 = pair.first;
                auto candidate_phase2_certainty = pair.second;

                if (distance <= tested_threshold) {
                    if (reg_contains(gt_test)) {
                        ++tp;
                    } else {
                        ++fp;
                    }
                } else {
                    if (gt_test == gt_reg) {
                        ++fn;
                    } else {
                        ++tn;
                    }
                }

                // ++++++++++
                if (distance <= tested_threshold) {
                    // mode 1
#ifdef STRICT_THRESHOLD
                    if (gt_reg == gt_test) {
#else
                    if (reg_contains(gt_test)) {
#endif
                        ++tp_v1;
                    } else {
                        ++fp_v1;
                    }

                    // mode 2
#ifdef STRICT_THRESHOLD
                    if (reg_candidate_phase2 == gt_test) {
#else
                    if (reg_contains(gt_test)) {
#endif
                        ++tp_v2;
                    } else {
                        ++fp_v2;
                    }

                    // mode 3
#ifdef STRICT_THRESHOLD
                    if (candidate_phase2_certainty >= ML_CERTAINTY) {
                    if (reg_candidate_phase2 == gt_test) {
#else
                    if (candidate_phase2_certainty >= ML_CERTAINTY) {
                        if (reg_contains(gt_test)) {
#endif
                            ++tp_v3;
                        } else {
                            ++fp_v3;
                        }
                    } else { // rejection since certainty is low
//#ifdef STRICT_THRESHOLD
//                    if (reg_candidate_phase2 == tested_input_gt) {
//#else
                        if (reg_contains(gt_test)) {
//#endif
                            ++fn_v3;
                        } else {
                            ++tn_v3;
                        }
                    }

                } else { // threshold below
                    // mode 1
                    if (reg_contains(gt_test)) {
                        ++fn_v1;
                        ++fn_v2;
                        ++fn_v3;
                    } else {
                        ++tn_v1;
                        ++tn_v2;
                        ++tn_v3;
                    }
                }
                // ++++++++++

                // mode 4
                {
                    auto p2 = phase2_imclassification(test);
                    std::string candidate_phase2 = p2.first;
                    double distance_ = phase1_shapecomparison(gt_reg, candidate_phase2, method);

                    if(distance_ <= tested_threshold) { // acceptance
#ifdef STRICT_THRESHOLD
                        if (reg_candidate_phase2 == gt_test) {
#else
                        if(reg_contains(gt_test)) {
#endif
                            ++tp_v4;
                        } else {
                            ++fp_v4;
                        }
                    } else {
                        if (reg_contains(test.substr(0,4))) {
                            ++fn_v4;
                        } else {
                            ++tn_v4;
                        }
                    }
                    }
                }

            exps.emplace_back(experiment(method, tested_threshold, tp, fp, tn, fn));

            // ONLY FOR GRAPH PLOTS
            ALL_EXPERIMENTS_V1.emplace_back(method, tested_threshold, tp_v1, fp_v1, tn_v1, fn_v1);
            ALL_EXPERIMENTS_V2.emplace_back(experiment(method, tested_threshold, tp_v2, fp_v2, tn_v2, fn_v2));
            ALL_EXPERIMENTS_V3.emplace_back(method, tested_threshold, tp_v3, fp_v3, tn_v3, fn_v3);
            ALL_EXPERIMENTS_V4.emplace_back(method, tested_threshold, tp_v4, fp_v4, tn_v4, fn_v4);

            if (tp + fp + tn + fn != ds.testings.size()) {
                throw std::logic_error("Sizes do not match.");
            }
        }
    }
    if(PRINT_INFO)
        std::cout << "    ... done." << std::endl;

    if(PRINT_INFO)
        std::cout << "Info: determining optimal EER ... " << std::endl;

    for (int a = 0; a < 6; a++) {
        bool found = false;
        double minimal_eer = std::numeric_limits<double>::max();
        experiment *eptr = nullptr;
        for (int i = 0; i < exps.size(); i++) {
            if (exps[i].get_method() == algos[a]) { // skip all other elements
                double fnr = exps[i].get_fnr();
                double fpr = exps[i].get_fpr();
                double thresh = exps[i].get_threshold();
                auto method = exps[i].get_method();

                //double diff = abs(fnr - fpr);
                // minimized ppv
                double diff = 1 - exps[i].get_precision();

                if (diff <= minimal_eer) {
                    eptr = &exps[i];
                    minimal_eer = diff;
                }
            }
        }
        optimal_precision_exps.push_back(*eptr);
    }

    if(PRINT_INFO)
        std::cout << " done." << std::endl;

    if (optimal_precision_exps.size() != 6) {
        throw std::logic_error("eer_exps.size() error");
    }

    // EER is now determined.
    // Run experiments

    std::vector<experiment> results_v1, results_v2, results_v3, results_v4;

    if(PRINT_INFO)
        std::cout << "Info: running experiments with optimal EER ... " << std::flush;
    for (auto e : optimal_precision_exps) { // per method
        double eer_threshold = e.get_threshold();
        auto method = e.get_method();

        // Mode 1: SC filters and states Match without ML
        // Mode 2: SC filters and ML states Match
        // Mode 3: SC filters and ML states Match at ML_CERTAINTY% certainty otherwise rejection
        // Mode 4: ML states Match at ML_CERTAINTY%, SC verifies distance
        experiment m1(method);
        experiment m2(method);
        experiment m3(method);
        experiment m4(method);

        m1.set_threshold(eer_threshold);
        m2.set_threshold(eer_threshold);
        m3.set_threshold(eer_threshold);
        m4.set_threshold(eer_threshold);


        for (const auto &test : ds.testings) { // per test

            // INPUT GT
            std::string tested_gt = test.substr(0, 4);

            // CANDIDATE PHASE 1
            std::string reg_candidate_phase1;
            double candidate_phase1_minimum_distance = std::numeric_limits<double>::max();

            // CANDIDATE PHASE 2
            std::string reg_candidate_phase2;
            double candidate_phase2_certainty = std::numeric_limits<double>::min();


            // perform phase1
            for (const auto &reg : ds.registrations) { // per register
                double distance = phase1_shapecomparison(reg, test, method);
                if (distance < candidate_phase1_minimum_distance) {
                    candidate_phase1_minimum_distance = distance;
                    reg_candidate_phase1 = reg.substr(0, 4);
                }
            }

            // perform phase2
            auto pair = phase2_imclassification(test);
            reg_candidate_phase2 = pair.first;
            candidate_phase2_certainty = pair.second;

            // perform evaluation
            std::string tested_input_gt = test.substr(0, 4);
            int v1_branch = 0, v2_branch = 0, v3_branch = 0, v4_branch = 0; // sanity check counts branch execs against errors

            if (candidate_phase1_minimum_distance <= eer_threshold) {
                // mode 1
#ifdef STRICT_THRESHOLD
                if (reg_candidate_phase1 == tested_input_gt) {
#else
                if (reg_contains(tested_input_gt)) {
#endif
                    m1.inc_tp();
                    v1_branch++;
                } else {
                    m1.inc_fp();
                    v1_branch++;
                }

                // mode 2
#ifdef STRICT_THRESHOLD
                if (reg_candidate_phase2 == tested_input_gt) {
#else
                if (reg_contains(tested_input_gt)) {
#endif
                    m2.inc_tp();
                    v2_branch++;
                } else {
                    m2.inc_fp();
                    v2_branch++;
                }

                // mode 3
#ifdef STRICT_THRESHOLD
                if (candidate_phase2_certainty >= ML_CERTAINTY) {
                    if (reg_candidate_phase2 == tested_input_gt) {
#else
                if (candidate_phase2_certainty >= ML_CERTAINTY) {
                    if (reg_contains(tested_input_gt)) {
#endif
                        m3.inc_tp();
                        v3_branch++;
                    } else {
                        m3.inc_fp();
                        v3_branch++;
                    }
                } else { // rejection since certainty is low
//#ifdef STRICT_THRESHOLD
//                    if (reg_candidate_phase2 == tested_input_gt) {
//#else
                    if (reg_contains(tested_input_gt)) {
//#endif
                        m3.inc_fn();
                        v3_branch++;
                    } else {
                        m3.inc_tn();
                        v3_branch++;
                    }
                }
            } else { // threshold below
                // mode 1
                if (reg_contains(tested_input_gt)) {
                    m1.inc_fn();
                    v1_branch++;
                } else {
                    m1.inc_tn();
                    v1_branch++;
                }

                // mode 2
                if (reg_contains(tested_input_gt)) {
                    m2.inc_fn();
                    v2_branch++;
                } else {
                    m2.inc_tn();
                    v2_branch++;
                }

                // mode 3
                if (reg_contains(tested_input_gt)) {
                    m3.inc_fn();
                    v3_branch++;
                } else {
                    m3.inc_tn();
                    v3_branch++;
                }
            }


            if (v1_branch != 1) {
                throw std::logic_error("Error in experiment m1");
            }
            if (v2_branch != 1) {
                throw std::logic_error("Error in experiment m2");
            }
            if (v3_branch != 1) {
                throw std::logic_error("Error in experiment m3");
            }


            // mode 4
            {
                double distance = phase1_shapecomparison(reg_candidate_phase2, test, method);

                if(distance <= eer_threshold) {
                    if (reg_candidate_phase2 == tested_input_gt) {
                        m4.inc_tp();
                    } else {
                        m4.inc_fp();
                    }
                } else {
                    if(reg_contains(tested_input_gt)) {
                        m4.inc_fn();
                    } else {
                        m4.inc_tn();
                    }
                }
            }
        }

        results_v1.push_back(m1);
        results_v2.push_back(m2);
        results_v3.push_back(m3);
        results_v4.push_back(m4);
    }


    if(PRINT_INFO) {
        std::cout << std::endl << std::endl << std::endl << std::endl << std::endl;
        print_results(results_v1, 1);
        print_results(results_v2, 2);
        print_results(results_v3, 3);
        print_results(results_v4, 4);
    } else {
        EXP_RESULTS.push_back(results_v1);
        EXP_RESULTS.push_back(results_v2);
        EXP_RESULTS.push_back(results_v3);
        EXP_RESULTS.push_back(results_v4);
    }

    plot_fnr_far_curves(exps);
}


/**
 * call example: "./system Labstudy_without_identities.csv2 g1.json"
 * @param argc
 * @param argv
 * @return
 */
int main(int argc, char *argv[]) {
#ifdef STRICT_THRESHOLD
    std::cout << "% Notice: Running in STRICT mode." << std::endl;
#else
    std::cout << "% Notice: Running in ANY mode." << std::endl;
#endif

    if (argc == 4) {
        SHAPECOMPARISON_CSV_CACHE = std::string(argv[1]);
        MACHINELEARNING_JSON_CACHE = std::string(argv[2]);
        DATASET = std::string(argv[3]);

        std::cout << "CSV-Cache: " << SHAPECOMPARISON_CSV_CACHE << std::endl;
        std::cout << "JSON-Cache: " << MACHINELEARNING_JSON_CACHE << std::endl;
        PRINT_INFO = true;
    } else if (argc == 2 && std::string(argv[1]) == "table") {
        // print results for TeX

        /*
        std::string folder = "/ramdisk/";
        std::vector<std::string> jsons = {folder+"g0.json",
                                          folder+"g1.json",
                                          folder+"g2.json",
                                          folder+"g3.json"};

        std::string nonstack = folder+"Labstudy_without_identities.csv2";
        std::string stack = folder+"Labstudy_all_stacks_wo_identities.csv";
         */

        std::vector<std::string> jsons = {"PAPER.json", "PAPER.json", "PAPER.json", "PAPER.json"};

        std::string nonstack = "PAPER_COMPARE.csv";
        std::string stack =    "PAPER_COMPARE_STACKED.csv";

        for(int i = 0; i <= 3; i++) {
            SHAPECOMPARISON_CSV_CACHE = nonstack;
            MACHINELEARNING_JSON_CACHE = jsons[i];
            calculate(get_dataset(i));

            SHAPECOMPARISON_CSV_CACHE = stack;
            calculate(get_dataset(i));
        }
        print_tex_results();
        exit(0);
    } else if (argc == 2 && std::string(argv[1]) == "graph") {
        // render graphs
        std::string folder = "/ramdisk/";
        std::vector<std::string> jsons = {folder+"g0.json",
                                          folder+"g1.json",
                                          folder+"g2.json",
                                          folder+"g3.json"};

        std::string nonstack = folder+"Labstudy_without_identities.csv2";
        std::string stack = folder+"Labstudy_all_stacks_wo_identities.csv";

        for(int i = 0; i <= 3; i++) {
            MACHINELEARNING_JSON_CACHE = jsons[i];

            SHAPECOMPARISON_CSV_CACHE = nonstack;
            calculate(get_dataset(i));

            SHAPECOMPARISON_CSV_CACHE = stack;
            calculate(get_dataset(i));
        }

        print_tex_graphs();
        exit(0);
    } else if (argc == 3 && std::string(argv[1]) == "graph") {
        // render graphs
        std::string folder = "/ramdisk/";
        std::vector<std::string> jsons = {folder+"g0.json",
                                          folder+"g1.json",
                                          folder+"g2.json",
                                          folder+"g3.json"};

        std::string nonstack = folder+"Labstudy_without_identities.csv2";
        std::string stack = folder+"Labstudy_all_stacks_wo_identities.csv";

        for(int i = 0; i <= 3; i++) {
            MACHINELEARNING_JSON_CACHE = jsons[i];

            SHAPECOMPARISON_CSV_CACHE = nonstack;
            calculate(get_dataset(i));

            SHAPECOMPARISON_CSV_CACHE = stack;
            calculate(get_dataset(i));
        }

        print_tex_graphs(std::stoi(argv[2]));
        exit(0);
    } else {
            exit(-1);
    }

    dataset* ds = get_dataset(stoi(DATASET));
    calculate(ds);
}
