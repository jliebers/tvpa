/*
 * This tool is used to perform calculations on thermal false images.
 * For example, it can extract a Skeleton from a specific frame in a thermal_false_color.avi.
 */


#include <string>
#include <boost/filesystem/path.hpp>
#include "Video.h"
#include <ctype.h>

std::vector<std::string> split(const std::string& s, char delimiter) {
    std::vector<std::string> tokens;
    std::string token;
    std::istringstream tokenStream(s);
    while (std::getline(tokenStream, token, delimiter))
    {
        tokens.push_back(token);
    }
    return tokens;
}

void parse_csv(std::string path) {
    std::ifstream infile(path);
    //infile.seekg(2); // skip BOM

    std::string line;
    while(std::getline(infile, line)) {
        //strip_unicode(line);

        const auto split_string = split(line, ',');
        const std::string name = split_string[0];
        const std::string file = split_string[1];
        const std::string frame = split_string[2];
        const std::string ext = boost::filesystem::extension(file);

        if(ext == ".avi") {
            Video video(file);
            size_t frames = video.count_frames();

            for(int i = 0; i < frames; i++) {

            }
        } else if (ext == ".png" || ext == ".jpg" || ext == ".jpeg") {

        } else {
            throw std::logic_error("Unknown extension of file " + file + ". Allowed: avi, png, jpg, jpeg (error 19)");
        }


        std::cout << line << std::endl;

    }

}

int main(int argc, char *argv[]) {
    // argv[0]: name of program
    // argv[1]: file
    // argv[2] (optional): skeleton algorithm

    if(argc == 1 || argc > 3) {
        std::cout << "Usage: " << argv[0] << " <file: jpg, png, avi, csv> <optional: skeleton: global|adaptive|edge|stack>" << std::endl;
        std::cout << "Usage: csv-format is: {name,file,frame,tlp,btr,percentage} where name is the proband's name, file is a file (jpg/png/avi)" << std::endl;
        std::cout << "       and frame is a frame in the video. If it's negative, it is the last frame. " << std::endl;
        std::cout << "       The frame number can also be 'all', where an intelligent detection will be tried." << std::endl;
        std::cout << "       CSV-file's encoding must be utf-8." << std::endl;
        std::cout << "       tlp must be the top left point of a forming rectangle with semicolon notation, e.g. X;Y" << std::endl;
        std::cout << "       btr must be the bottom right point of a forming rectangle. Full sample: 100;200,400;500" << std::endl;
        std::cout << "       percentage must be a number between 0 and 100, describing, to what percentage the rectangle is filled with warm pixels to include a hand." << std::endl;
        return EXIT_FAILURE;
    }

    const std::string path = std::string(argv[1]);
    const std::string extension = boost::filesystem::extension(path);
    std::string name = boost::filesystem::path(path).filename().string();

    cv::Mat skeleton, debug;
    eSkeletonExtractionAlgorithm algo = eAdaptiveThreshold;

    bool skeleton_stacking = false;

    if(argc >= 3) { // if provided optional argument
        std::string skele_algo = std::string(argv[2]);
        if(skele_algo == "global") {
            algo = eGlobalThreshold;
        } else if(skele_algo == "adaptive") {
            algo = eAdaptiveThreshold;
        } else if(skele_algo == "edge") {
            algo = eEdgeDetection;
        } else if(skele_algo == "stack") {
            skeleton_stacking = true;
        } else {
            std::cout << "Parameter 2 (skeleton) is invalid." << std::endl;
            return EXIT_FAILURE;
        }
    }

    std::vector<cv::Mat> stacks, stacksdebug;

    try {
        if (extension == ".avi") {
            int frame_num = 0;
            const int rate = 27;

            Video v(path);

            auto total_frames = v.count_frames();
            if(skeleton_stacking) {
                for(frame_num = 0; frame_num < total_frames-rate-1; frame_num+=rate) {
                    v.do_stack(frame_num, frame_num + rate, 14);
                    skeleton = *v.get_stack();
                    debug = v.get_frame(frame_num)->get_debug_mat();

                    stacks.push_back(skeleton);
                    stacksdebug.push_back(debug);

                    std::cout << "Stacking frames [" << frame_num << " - " << frame_num + rate << "] of total " << total_frames << " frames." << std::endl;
                }
            } else {
                skeleton = *v.get_frame(frame_num)->get_skeleton(algo)->get_mat();
                debug = v.get_frame(frame_num)->get_debug_mat();
            }
        } else if (extension == ".png" || extension == ".jpg" || extension == ".jpeg") {
            Image img(path);
            skeleton = *img.get_skeleton(algo)->get_mat();
            debug = img.get_debug_mat();
        } else if (extension == ".csv") {
            parse_csv(path);
            return EXIT_SUCCESS;
        }
    } catch (std::logic_error &le) {
        std::cout << "Error: Exception (std::logic_error) thrown on processing file " << name << std::endl;
        std::cout << le.what() << std::endl;
        std::cout << "Terminating with exit code -1" << std::endl;
        return -1;
    } catch (cv::Exception &ce) {
        std::cout << "Error: Exception (cv::exception) thrown on processing file " << name << std::endl;
        std::cout << ce.what() << std::endl;
        std::cout << "Terminating with exit code -2" << std::endl;
        return -2;
    }

    bool a;
    if(skeleton_stacking) {
        if(stacks.size() > 1) {
            unsigned cnt = 0;
            for(auto s : stacks) {
                a = cv::imwrite(path + "_skeletonstack_" + std::to_string(cnt++) + ".png", s);
            }
        } else {
            a = cv::imwrite(path + "_skeletonstack.png", skeleton);
        }
    } else {
        a = cv::imwrite(path + "_skeleton.png", skeleton);
    }
    bool b = cv::imwrite(path + "_debug.png", debug);

    if(a && b) {
        return EXIT_SUCCESS;
    } else {
        if(!a) {
            std::cerr << "Writing of " + path + "_skeleton.png failed." << std::endl;
        }
        if(!b) {
            std::cerr << "Writing of " + path + "_debug.png failed." << std::endl;
        }

        return EXIT_FAILURE;
    }
}
