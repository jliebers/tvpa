//
// Created by jl on 07.03.19.
//

//
// Created by jl on 04.12.18.
// This file implements means to
//


#include <iostream>
#include <fstream>
#include <vector>
#include <time.h>
#include <iomanip>
#include <sys/wait.h>

#include <IRFileReader.h>
#include <ImageBuilder.h>
#include <opencv2/core/mat.hpp>
#include <opencv2/imgproc.hpp>
#include <cv.hpp>
#include <netinet/in.h>
#include <IRDevice.h>
#include <IRLogger.h>
#include <boost/filesystem/operations.hpp>
#include <boost/range.hpp>
#include <chrono>
#include "Image.h"

const int w = 382;
const int h = 288;
const int width = w;
const int height = h;

using namespace evo;

#define ulli unsigned long long int

#define SHOW_WINDOW_CVWAITKEY 5
#define FPS 10
#define GETTIME_STRING_LENGTH 24
#define FILE_MAX_CHUNKS 250
#define BLOCKSIZE 221560


//define SHOW
const std::string WINDOW_NAME = "tvpa-viewer";
const std::string WINDOW_NAME_SKELETON = "tvpa-viewer (skeleton)";
const std::string WINDOW_NAME_DEBUG = "tvpa-viewer (debug)";
const std::string WINDOW_NAME_COLOURBAR = "tvpa-viewer (colourbar)";

bool SHOW_SKELETON = false;
bool SHOW_DEBUG = false;
bool SHOW_COLOURBAR = false;
int CIRCLE_XCOORD = w/2;
int CIRCLE_YCOORD = h/2;
int CIRCLE_RADIUS = 30;
int EXTRACT_START_FRAME = 0;
int EXTRACT_END_FRAME = 0;
int CIRCLE_TEMPOFFSET_MAX_POSITIVE = 1;
int CIRCLE_TEMPOFFSET_MAX_NEGATIVE = 0;
int CIRCLE_TEMPOFFSET_MIN_POSITIVE = 0;
int CIRCLE_TEMPOFFSET_MIN_NEGATIVE = 7;
int CURRENT_FRAME_SLIDER = 0;
int PALETTE = 5;

int MANUAL_LOWER_TEMP = 30;
int MANUAL_UPPER_TEMP = 40;
// was -5, +1
// was 0

std::vector<cv::Mat> ALL_MAT;
std::vector<cv::Mat> ALL_COLOURBAR;
std::vector<float**> ALL_TEMPS;
std::vector<std::pair<float,float>> ALL_COLOURBAR_TEMPS;

std::vector<std::string> CHUNK_PATHS;
auto imagebuilder_palette = evo::eIron;
auto imagebuilder_palette_scaling = evo::eMinMax;
auto imagebuilder_palette_scaling_circle = false;

class IRHandlerClient : evo::IRImagerClient {
public:
    unsigned char* _bufferRaw = new unsigned char[BLOCKSIZE];
    bool ENABLE_WRITE = false;
    int fps = FPS;

    IRHandlerClient(){};

    IRHandlerClient(IRDevice *pDevice, IRImager *pImager) {
        pImager->setClient(this);
    }

    void onRawFrame(unsigned char* data, int size) override {
        std::cout << "onRawFrame()" << std::endl;
    }

    void onThermalFrame(unsigned short* thermal, unsigned int w, unsigned int h, evo::IRFrameMetadata meta, void* arg) override {
        float circle_max_temp;

        float** globaltemps = new float*[width];
        for(int i = 0; i < width; ++i) {
            globaltemps[i] = new float[height];
        }

        {
            evo::ImageBuilder imgbuilder = evo::ImageBuilder(false);
            imgbuilder.setPalette(imagebuilder_palette);
            imgbuilder.setPaletteScalingMethod(imagebuilder_palette_scaling);
            imgbuilder.setManualTemperatureRange(MANUAL_LOWER_TEMP, MANUAL_UPPER_TEMP);

            imgbuilder.setData(w, h, thermal);

            std::vector<unsigned char> mydata = std::vector<unsigned char>(w * h * 3);
            unsigned char *mydata_ptr = &mydata[0];

            imgbuilder.convertTemperatureToPaletteImage(mydata_ptr, true);

            const cv::Point middle = cv::Point(CIRCLE_XCOORD, CIRCLE_YCOORD);
            circle_max_temp = 0;
            for (int i = 0; i < imgbuilder.getWidth(); i++) {
                for (int j = 0; j < imgbuilder.getHeight(); j++) {
                    const cv::Point current = cv::Point(i, j);

                    float temp = imgbuilder.getTemperatureAt(i, j);
                    globaltemps[i][j] = temp;
                    if(cv::norm(middle-current) < CIRCLE_RADIUS) {
                        if (temp > circle_max_temp) {
                            circle_max_temp = temp;
                        }
                    }
                }
            }
        }

        std::vector<unsigned char> mydata;
        unsigned char *mydata_ptr;
        cv::Mat dst_colourbar;
        {
            evo::ImageBuilder imgbuilder = evo::ImageBuilder(false);
            imgbuilder.setPalette(imagebuilder_palette);
            imgbuilder.setPaletteScalingMethod(imagebuilder_palette_scaling);

            if(imagebuilder_palette_scaling_circle) {
                imgbuilder.setManualTemperatureRange(circle_max_temp + CIRCLE_TEMPOFFSET_MIN_POSITIVE - CIRCLE_TEMPOFFSET_MIN_NEGATIVE,
                        circle_max_temp + CIRCLE_TEMPOFFSET_MAX_POSITIVE - CIRCLE_TEMPOFFSET_MAX_NEGATIVE);
            }
            else {
                imgbuilder.setManualTemperatureRange(MANUAL_LOWER_TEMP, MANUAL_UPPER_TEMP);
            }
            imgbuilder.setData(w, h, thermal);

            mydata = std::vector<unsigned char>(w * h * 3);
            mydata_ptr = &mydata[0];

            imgbuilder.convertTemperatureToPaletteImage(mydata_ptr, true);

            unsigned int wBar = 20;
            unsigned int hBar = h;
            std::vector<unsigned char> bardata = std::vector<unsigned char>(wBar * hBar * 3);
            auto bardata_ptr = &bardata[0];
            imgbuilder.getPaletteBar(wBar, hBar, bardata_ptr);
            imgbuilder.serializePPM("/tmp/palettebar.ppm", bardata_ptr, wBar, hBar); // TODO in-memory fix is desired but not available at this time

            cv::Mat bar_mat = cv::Mat(cv::Size(wBar, hBar), CV_8UC3, &bardata[0], cv::Mat::AUTO_STEP);
            //cv::Mat cloned_bar = bar_mat.clone();
            cv::Mat cloned_bar = cv::imread("/tmp/palettebar.ppm"); // TODO There was no better way found than writing and reading
            cv::Mat tmplabel_bar = cv::Mat::zeros(hBar, 55, CV_8UC3);
            tmplabel_bar = cv::Scalar(255,255,255);
            cv::putText(tmplabel_bar,
                        std::to_string(imgbuilder.getIsothermalMax()).substr(0, 4),
                        cv::Point(3,15),
                        cv::FONT_HERSHEY_COMPLEX_SMALL,
                        1.0,
                        cv::Scalar(0,0,0));
            cv::putText(tmplabel_bar,
                        std::to_string(imgbuilder.getIsothermalMin()).substr(0, 4),
                        cv::Point(3,h-5),
                        cv::FONT_HERSHEY_COMPLEX_SMALL,
                        1.0,
                        cv::Scalar(0,0,0));
            cv::hconcat(cloned_bar, tmplabel_bar, dst_colourbar);
            ALL_COLOURBAR_TEMPS.emplace_back(imgbuilder.getIsothermalMin(), imgbuilder.getIsothermalMax());
        }

        cv::Mat cv_img = cv::Mat(cv::Size(w, h), CV_8UC3, mydata_ptr, cv::Mat::AUTO_STEP);
        cv::cvtColor(cv_img, cv_img, CV_BGR2RGB);

        // Must clone since `data` points to this stack frame in cv_img's ctor
        cv::Mat cloned_mat = cv_img.clone();

        ALL_MAT.push_back(cloned_mat);
        ALL_TEMPS.push_back(globaltemps);

        cv::Mat merged_colourbar;
        cv::Mat white_space = cv::Mat::zeros(height, 5, CV_8UC3);
        white_space = cv::Scalar(255, 255, 255);
        cv::hconcat(cloned_mat, white_space, merged_colourbar);
        cv::hconcat(merged_colourbar, dst_colourbar, merged_colourbar);
        ALL_COLOURBAR.push_back(merged_colourbar);


#ifdef SHOW
        cv::imshow("Image-Pre", cv_img);
        cv::imshow("Image-Post", cv_img2);
        cv::waitKey(SHOW_WINDOW_CVWAITKEY);
#endif
    }

    void onFlagStateChange(evo::EnumFlagState, void*) override {}

    void onProcessExit(void*) override {}

    void setBufferRaw(unsigned char* ptr) {
        std::memcpy(this->_bufferRaw, ptr, BLOCKSIZE);
    }

    ~IRHandlerClient() {
        delete [] _bufferRaw;
    }
};

IRHandlerClient *clientptr;
evo::IRImager *imagerptr;

ulli str2unixmilli(const std::string time) {
    const std::string time_format_string = "%d-%m-%Y_%H:%M:%S"; // http://www.cplusplus.com/reference/ctime/strftime/
    std::tm tm = {};
    std::stringstream ss(time);
    ss >> std::get_time(&tm, time_format_string.c_str());
    auto tp = std::chrono::system_clock::from_time_t(std::mktime(&tm));

    ulli t = static_cast<ulli>(std::chrono::duration_cast<std::chrono::milliseconds>(tp.time_since_epoch()).count());

    // get millis
    int millis = std::stoi(time.substr(time.find_last_of('.') + 1, std::string::npos));

    return t + millis;
};


struct StringComparator
{
    bool operator()(const std::string& lhs, const std::string& rhs)
    {
        int lhs_chunk = std::stoi(lhs.substr(lhs.find_last_of('.') + 1, std::string::npos));
        int rhs_chunk = std::stoi(rhs.substr(rhs.find_last_of('.') + 1, std::string::npos));

        return lhs_chunk < rhs_chunk;
    }
} StringComparer;


static void onMouse( int event, int x, int y, int, void* )
{
    if( event != cv::EVENT_LBUTTONDOWN )
        return;

    cv::displayStatusBar(WINDOW_NAME, "Temperature at (" + std::to_string(x) + ", " + std::to_string(y) + ") = "
    + std::to_string(ALL_TEMPS[CURRENT_FRAME_SLIDER][x][y]).substr(0,4), 1000);
}


void process_stream() {
    ALL_MAT.clear();
    ALL_COLOURBAR.clear();
    ALL_COLOURBAR_TEMPS.clear();
    for(auto f : ALL_TEMPS) {
        for (int i = 0; i < width; ++i) {
            delete[] f[i];
        }
        delete[] f;
    }
    ALL_TEMPS.clear();

    for(const auto &current_chunk_path : CHUNK_PATHS) {
        std::ifstream current_file(current_chunk_path);

        for(int i = 0; i < FILE_MAX_CHUNKS; i++) {
            char current_timestamp_buffer[GETTIME_STRING_LENGTH];
            unsigned char current_rawimage_buffer[BLOCKSIZE];

            if (current_file.is_open()) {
                current_file.read(current_timestamp_buffer, GETTIME_STRING_LENGTH); // read current timestamp
                current_file.read((char *) current_rawimage_buffer, BLOCKSIZE); // read raw thermal data
            } else {
                std::cout << "Current_file is not open" << std::endl;
            }

            // set data in client for optris
            clientptr->setBufferRaw(current_rawimage_buffer);
            imagerptr->process(clientptr->_bufferRaw);

            if(ALL_MAT.size() % 100 == 0) {
                std::cout << "Loaded " << ALL_MAT.size() << " images ..." << std::endl;
            }
        }
    }
}

void update_gui_loop() {
    while(true) {
        switch (cv::waitKey(SHOW_WINDOW_CVWAITKEY)) {
            case int('q'):
                exit(0);
            case int('k'):
                if(CURRENT_FRAME_SLIDER < ALL_MAT.size() - 1) {
                    CURRENT_FRAME_SLIDER++;
                    cv::setTrackbarPos("Frame", WINDOW_NAME, CURRENT_FRAME_SLIDER);
                }
                break;
            case int('j'):
                if(CURRENT_FRAME_SLIDER > 0) {
                    CURRENT_FRAME_SLIDER--;
                    cv::setTrackbarPos("Frame", WINDOW_NAME, CURRENT_FRAME_SLIDER);
                }
                break;
        }

        cv::imshow(WINDOW_NAME, ALL_MAT[CURRENT_FRAME_SLIDER]);

        if(SHOW_SKELETON) {
            Image image = Image(ALL_MAT[CURRENT_FRAME_SLIDER], "");
            cv::imshow(WINDOW_NAME_SKELETON, *image.get_skeleton()->get_mat());
        }
        if(SHOW_DEBUG) {
            Image image = Image(ALL_MAT[CURRENT_FRAME_SLIDER], "");
            cv::imshow(WINDOW_NAME_DEBUG, image.get_debug_mat());
        }
        if(SHOW_COLOURBAR) {
            cv::imshow(WINDOW_NAME_COLOURBAR, ALL_COLOURBAR[CURRENT_FRAME_SLIDER]);
            cv::displayStatusBar(WINDOW_NAME_COLOURBAR, "Min: "
            + std::to_string(ALL_COLOURBAR_TEMPS[CURRENT_FRAME_SLIDER].first)
            + ", Max: " + std::to_string(ALL_COLOURBAR_TEMPS[CURRENT_FRAME_SLIDER].second), 0);
        }

        switch(PALETTE) {
            case 0:
                imagebuilder_palette = evo::eAlarmBlue;
                break;
            case 1:
                imagebuilder_palette = evo::eAlarmBlueHi;
                break;
            case 2:
                imagebuilder_palette = evo::eGrayBW;
                break;
            case 3:
                imagebuilder_palette = evo::eGrayWB;
                break;
            case 4:
                imagebuilder_palette = evo::eAlarmGreen;
                break;
            case 5:
                imagebuilder_palette = evo::eIron;
                break;
            case 6:
                imagebuilder_palette = evo::eIronHi;
                break;
            case 7:
                imagebuilder_palette = evo::eMedical;
                break;
            case 8:
                imagebuilder_palette = evo::eRainbow;
                break;
            case 9:
                imagebuilder_palette = evo::eRainbowHi;
                break;
            case 10:
                imagebuilder_palette = evo::eAlarmRed;
                break;
            default:
                imagebuilder_palette = evo::eIron;
                break;
        }

    }
}


void exp_minmax_btncallback(int state, void* v) {
    if(state == 1) {
        imagebuilder_palette_scaling = evo::eMinMax;
    }
}


void exp_sigma_btncallback(int state, void* v) {
    if(state == 1) {
        imagebuilder_palette_scaling = evo::eSigma1;
    }
}


void exp_sigma3_btncallback(int state, void* v) {
    if(state == 1) {
        imagebuilder_palette_scaling = evo::eSigma3;
    }
}


void exp_manual_btncallback(int state, void* v) {
    if(state == 1) {
        imagebuilder_palette_scaling = evo::eManual;
    }
}


void exp_circle_btncallback(int state, void* v) {
    if(state == 1) {
        imagebuilder_palette_scaling_circle = true;
    } else {
        imagebuilder_palette_scaling_circle = false;
    }
}

void show_image_debug_btncallback(int state, void* v) {
    if(state == 1) {
        SHOW_DEBUG = true;
    } else {
        SHOW_DEBUG = false;
        cv::destroyWindow(WINDOW_NAME_DEBUG);
    }
}

void show_image_skeleton_btncallback(int state, void* v) {
    if(state == 1) {
        SHOW_SKELETON = true;
    } else {
        SHOW_SKELETON = false;
        cv::destroyWindow(WINDOW_NAME_SKELETON);
    }
}

void show_image_colourbar_btncallback(int state, void* v) {
    if(state == 1) {
        SHOW_COLOURBAR = true;
    } else {
        SHOW_COLOURBAR = false;
        cv::destroyWindow(WINDOW_NAME_COLOURBAR);
    }
}

void apply_settings_btncallback(int state, void* v) {
    process_stream();
}

void extract_png_btncallback(int state, void *v) {
    for(int i = EXTRACT_START_FRAME; i < EXTRACT_END_FRAME; i++) {
        cv::imwrite("falsecolour_" + std::to_string(i) + ".png", ALL_MAT[i]);

        if (SHOW_DEBUG) {
            Image img = Image(ALL_MAT[i], "");
            cv::imwrite("debug_" + std::to_string(i) + ".png", img.get_debug_mat());
        }

        if (SHOW_SKELETON) {
            Image img = Image(ALL_MAT[i], "");
            cv::imwrite("skeleton_" + std::to_string(i) + ".png", *img.get_skeleton()->get_mat());
        }

        if (SHOW_COLOURBAR) {
            Image img = Image(ALL_MAT[i], "");
            cv::imwrite("colourbar_" + std::to_string(i) + ".png", ALL_COLOURBAR[i]);
        }
    }
}

void extract_avi_btncallback(int state, void *v) {
    cv::VideoWriter vwriter_orig;
    cv::VideoWriter vwriter_debug;
    cv::VideoWriter vwriter_skeleton;
    cv::VideoWriter vwriter_cbar;

    int fps = 25;


    vwriter_orig.open("OUTPUT_ORIG.avi", CV_FOURCC('M', 'J', 'P', 'G'), fps, cv::Size(w, h));

    /*
    if(SHOW_DEBUG) {
        vwriter_debug.open("OUTPUT_DEBUG.avi", CV_FOURCC('M', 'J', 'P', 'G'), fps, cv::Size(w, h));
    }

    if(SHOW_SKELETON) {
        vwriter_skeleton.open("OUTPUT_SKELETON.avi", CV_FOURCC('M', 'J', 'P', 'G'), fps, cv::Size(w, h));
    }

    if(SHOW_COLOURBAR) {
        vwriter_cbar.open("OUTPUT_CBAR.avi", CV_FOURCC('M', 'J', 'P', 'G'), fps, cv::Size(w, h));
    }
     */


    for(int i = EXTRACT_START_FRAME; i < EXTRACT_END_FRAME; i++) {
        vwriter_orig << ALL_MAT[i];

        /*
        if (SHOW_DEBUG) {
            Image img = Image(ALL_MAT[i], "");
            vwriter_debug << img.get_debug_mat();
        }

        if (SHOW_SKELETON) {
            Image img = Image(ALL_MAT[i], "");
            vwriter_skeleton << *img.get_skeleton()->get_mat();
        }

        if (SHOW_COLOURBAR) {
            Image img = Image(ALL_MAT[i], "");
            vwriter_cbar << ALL_COLOURBAR[i];
        }
         */
    }

    vwriter_orig.release();
    /*
    vwriter_debug.release();
    vwriter_skeleton.release();
    vwriter_cbar.release();
     */
}


int main(int argc, char* argv[]) {
    if (argc != 3) {
        std::cout << "Usage: " << argv[0]
                  << " <conf.xml> <raw_thermal_file> <optional: starttimestamp> <optional: endtimestamp>" << std::endl;
        std::cout << "Usage: Timeformat is DD-MM-YYYY_HH-MM-SS" << std::endl;
        exit(EXIT_FAILURE);
    }

    const std::string CONF = std::string(argv[1]);
    const std::string FILE = std::string(argv[2]);
    const std::string FILE_BASE = FILE.substr(0, std::string(FILE).find_last_of('.')) + ".";

    std::vector<cv::String> glob_results;
    cv::glob(FILE_BASE + '*', glob_results, false);

    if(glob_results.empty()) {
        unsigned current_chunk = 0;
        while (boost::filesystem::exists(FILE_BASE + std::to_string(current_chunk))) {
            CHUNK_PATHS.push_back(FILE_BASE + std::to_string(current_chunk++));
        }
    } else {
        for(auto gr : glob_results) {
            CHUNK_PATHS.emplace_back(std::string(gr));
        }
    }

    const unsigned long TOTAL_FRAMES = CHUNK_PATHS.size() * FILE_MAX_CHUNKS;

    std::cout << "Loaded " << CHUNK_PATHS.size() << " thermal chunks with " << TOTAL_FRAMES << " frames." << std::endl;

    // Set up optris
    IRLogger::setVerbosity(IRLOG_ERROR, IRLOG_OFF);
    IRDeviceParams xmlparams;
    IRDeviceParamsReader::readXML(CONF.c_str(), xmlparams);
    IRDevice *dev = IRDevice::IRCreateDevice(xmlparams);
    evo::IRImager imager;
    imager.init(&xmlparams, dev->getFrequency(), dev->getWidth(), dev->getHeight(), dev->controlledViaHID());
    IRHandlerClient client(dev, &imager);
    imagerptr = &imager;
    clientptr = &client;

    cv::namedWindow(WINDOW_NAME);
    cv::setMouseCallback(WINDOW_NAME, onMouse, nullptr);
    // Build GUI:

    cv::createButton("Exp: eMinMax", exp_minmax_btncallback, nullptr, cv::QT_RADIOBOX, true);
    cv::createButton("Exp: eSigma",  exp_sigma_btncallback, nullptr, cv::QT_RADIOBOX);
    cv::createButton("Exp: eSigma3", exp_sigma3_btncallback, nullptr, cv::QT_RADIOBOX);
    cv::createButton("Exp: eManual", exp_manual_btncallback, nullptr, cv::QT_RADIOBOX);
    cv::createButton("Exp: Circle",  exp_circle_btncallback, nullptr, cv::QT_RADIOBOX);
    cv::createTrackbar("Color-Palette", "", &PALETTE, 10);

    cv::createTrackbar("Manual TempMax", "", &MANUAL_UPPER_TEMP, 100);
    cv::createTrackbar("Manual TempMin", "", &MANUAL_LOWER_TEMP, 100);
    cv::createTrackbar("Circle X-Coord", "", &CIRCLE_XCOORD, w-1);
    cv::createTrackbar("Circle Y-Coord", "", &CIRCLE_YCOORD, h-1);
    cv::createTrackbar("Circle Radius", "",  &CIRCLE_RADIUS, h/2);
    cv::createTrackbar("Circle TempOffset(Max) Positive", "", &CIRCLE_TEMPOFFSET_MAX_POSITIVE, 20);
    cv::createTrackbar("Circle TempOffset(Max) Negative", "", &CIRCLE_TEMPOFFSET_MAX_NEGATIVE, 20);
    cv::createTrackbar("Circle TempOffset(Min) Positive", "", &CIRCLE_TEMPOFFSET_MIN_POSITIVE, 20);
    cv::createTrackbar("Circle TempOffset(Min) Negative", "", &CIRCLE_TEMPOFFSET_MIN_NEGATIVE, 20);

    cv::createButton("Show Img: Debug",     show_image_debug_btncallback, nullptr, cv::QT_CHECKBOX);
    cv::createButton("Show Img: Skeleton",  show_image_skeleton_btncallback, nullptr, cv::QT_CHECKBOX);
    cv::createButton("Show Img: ColourBar", show_image_colourbar_btncallback, nullptr, cv::QT_CHECKBOX);

    cv::createButton("Apply settings", apply_settings_btncallback);

    cv::createTrackbar("Extract all frames from", "", &EXTRACT_START_FRAME, TOTAL_FRAMES-1);
    cv::createTrackbar("Extract all frames to", "",   &EXTRACT_END_FRAME, TOTAL_FRAMES-1);
    cv::createButton("Extract to PNG", extract_png_btncallback);
    cv::createButton("Extract to AVI", extract_avi_btncallback);

    cv::createTrackbar("Frame", WINDOW_NAME, &CURRENT_FRAME_SLIDER, TOTAL_FRAMES - 1);

    process_stream();

    gui_loop:
    try {
        update_gui_loop();
    } catch (std::logic_error e) {
        std::cout << e.what() << std::endl;
        goto gui_loop;
    }

    return EXIT_SUCCESS;
}