//
// Created by jl on 18.11.18.
//

#include "Video.h"

Video::Video(std::string path) {
    init_cap(path);
    this->m_video_frames = (size_t) m_cap.get(cv::CAP_PROP_FRAME_COUNT);
    this->m_image_ptr_vector = new std::vector<Image*>(m_video_frames);
    m_image_ptr_vector->resize(m_video_frames, nullptr);
}

void Video::init_cap(const std::string path) {
    m_path = path;
    this->m_cap = cv::VideoCapture(path);
}

size_t Video::count_frames() {
    return m_video_frames;
}

Image *Video::get_frame(int index) {
    // Error Handling
    if(index > m_video_frames) {
        throw std::logic_error("Exception: Called Video::get_frame() with an index which does not exist: "
        + std::to_string(index) + " but only " + std::to_string(m_video_frames) + " frames exist. (error 05)");
    } else if (!m_cap.isOpened()) {
        throw std::logic_error("Exception: Video::get_frame() m_cap is not open (error 12)");
    }

    if(index < 0) {
        index = m_video_frames;
    }

    init_cap(this->m_path);
    cv::Mat mat;
    m_cap >> mat;
    for(int i = 0; i < index; i++) { // fast forward until requested frame
        m_cap >> mat;
    }

    if(mat.empty()) {
        throw std::logic_error("Exception: Video::get_frame() loaded mat is empty (error 13");
    }

    if(mat.empty()){
        throw std::logic_error("Video::count_frames() Mat at frame " + std::to_string(index) + " is empty (error 11)");
    }

    // `mat` now contains the requested frame

    std::string name = boost::filesystem::path(this->m_path).filename().string() + "@" + std::to_string(index);
    Image *imageptr = new Image(mat, name);

    m_image_ptr_vector->at(index) = imageptr;

    return imageptr;
}

Video::~Video() {
    for(Image* imageptr : *m_image_ptr_vector) {
        delete imageptr;
    }
    delete m_image_ptr_vector;
    m_cap.release();
}

cv::Mat *Video::get_stack() {
    return & this->m_stack;
}

void Video::do_stack(int start, int stop, int ratio) {
    std::vector<cv::Mat> stacks;

    int skips = 0;
    for(int i = start; i < (stop + skips); i++) {
        cv::Mat new_mat;

        try {
            this->get_frame(i)->get_skeleton()->get_mat()->copyTo(new_mat);
        } catch (std::logic_error const &le) {
            skips += 1;
            continue;
        }

        cv::dilate(new_mat, // src
                   new_mat,  // dst
                   cv::getStructuringElement(cv::MORPH_CROSS, cv::Size(10, 10))); // kernel
        stacks.push_back(new_mat);

        //cv::imshow("fat", new_mat);
        //while (cv::waitKey(1) != int('q'));
        //cv::waitKey(1);
    }

    cv::Mat skeleton_stack = cv::Mat::zeros(stacks[0].rows, stacks[0].cols , CV_8UC1);

    for(auto m : stacks) {
        for(int row = 0; (row < m.rows); row++) {
            for(int col = 0; (col < m.cols); col++) {
                if(m.at<uchar>(row, col) > 0){
                    skeleton_stack.at<uchar>(row, col) += 1;
                }
            }
        }
        //cv::imshow("skeleton_stack", skeleton_stack);
        //while (cv::waitKey(1) != int('q'));
    }


    // binarize
    for(int row = 0; (row < skeleton_stack.rows); row++) {
        for(int col = 0; (col < skeleton_stack.cols); col++) {
            if(skeleton_stack.at<uchar>(row, col) > ratio){
                skeleton_stack.at<uchar>(row, col) = 255;
            } else {
                skeleton_stack.at<uchar>(row, col) = 0;
            }
        }
    }

    //cv::imshow("skeleton_stack_result", skeleton_stack);
    //while (cv::waitKey(1) != int('q'));

    Skeleton s(&skeleton_stack, "");
    s.thin();
    s.get_mat()->copyTo(this->m_stack);
}
