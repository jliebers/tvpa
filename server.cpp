//
// Created by jl on 02.12.18.
//

#define DEBUG

#include <opencv2/videoio.hpp>
#include <cv.hpp>
#include <chrono>
#include <iostream>
#include <signal.h>
#include <iomanip>

#include <boost/filesystem/operations.hpp>

#include <IRLogger.h>
#include <IRDeviceParams.h>
#include <IRDevice.h>
#include <IRImager.h>

#include <ImageBuilder.h>
#include <sys/time.h>
#include <thread>

#define BLOCKSIZE 221560
#define GETTIME_STRING_LENGTH 24

//#define PRINT_PERFORMANCE

evo::IRDeviceParams params;
evo::IRDevice *device;
evo::IRImager imager;

int w, h; // w and h of imager

cv::VideoCapture vc0; // Logitech
cv::Mat cap0; // Logitech Mat

boost::filesystem::path CURRENT_DIR;
const int TIMEOUT = 60;

int CYCLE = 0;

bool EXIT = false;
bool SHOW_WINDOWS = false;
bool SESSION = false;


std::string get_time(bool delim = true) {
    using namespace std::chrono;
    // get current time
    auto now = system_clock::now();
    // get number of milliseconds for the current second
    // (remainder after division into seconds)
    auto ms = duration_cast<milliseconds>(now.time_since_epoch()) % 1000;
    // convert to std::time_t in order to convert to std::tm (broken time)
    auto timer = system_clock::to_time_t(now);
    // convert to broken time
    std::tm bt = *std::localtime(&timer);
    std::ostringstream oss;
    oss << std::put_time(&bt, "%d-%m-%Y_%H:%M:%S");
    oss << '.' << std::setfill('0') << std::setw(3) << ms.count();
    if(delim) {
        return oss.str() + "   ";
    } else {
        return oss.str();
    }
}

void thread_record_webcam() {
    while(! EXIT) {
        using namespace std::chrono_literals;
        std::this_thread::sleep_for(330ms);

#ifdef PRINT_PERFORMANCE
        auto start = std::chrono::system_clock::now();
#endif

        cv::Mat webcam_cap;
        vc0 >> webcam_cap;
        cv::imwrite("webcam-snapshots/webcam_" + get_time(false) + ".jpg", webcam_cap);

#ifdef PRINT_PERFORMANCE
        auto end = std::chrono::system_clock::now();
        auto elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(end - start);
        std::cout << "Info: record_webcam() took " << elapsed.count() << std::endl;
#endif
    }
}

void thread_record_thermal() {
    unsigned long CHUNK = 0;
    std::vector<std::pair<std::string, unsigned char *>> thermalvector;

    while (true) {
        using namespace std::chrono_literals;
        std::this_thread::sleep_for(40ms); // approx 25 fps

#ifdef PRINT_PERFORMANCE
        auto start = std::chrono::system_clock::now();
#endif

        unsigned char *bufferRaw = new unsigned char[BLOCKSIZE];

        double timestamp;
        if (device->getFrame(bufferRaw, &timestamp) == evo::IRIMAGER_SUCCESS) {
            imager.process(bufferRaw);

            auto pair = std::pair<std::string, unsigned char *>();
            pair.first = get_time(false);
            pair.second = bufferRaw;
            thermalvector.push_back(pair);
        } else {
            std::cout << get_time() << " Error: getFrame(bufferRaw) did not succeed" << std::endl;
        }

        if (thermalvector.size() == 250) {
#ifdef PRINT_PERFORMANCE
            auto start = std::chrono::system_clock::now();
#endif
            std::cout << get_time() << " Info: Flushing vectors at chunk " << CHUNK << std::endl;

            // dump thermal data
            std::ofstream myfile;
            const std::string filename = "thermal/raw_data.thermal." + std::to_string(CHUNK);
            myfile.open(filename);

            // write to disk
            for (auto &i : thermalvector) {
                myfile.write(i.first.c_str(), GETTIME_STRING_LENGTH);
                myfile.write((char *) &i.second[0], BLOCKSIZE);
            }
            myfile.close();

            CHUNK += 1;

            // delete other stuff which has been written to file by now
            for (auto buf : thermalvector) {
                delete[] buf.second;
            }

            const std::string command = "gzip -f " + filename + " &";
            FILE* pipe = popen(command.c_str(), "r");
            if (!pipe)
            {
                std::cerr << get_time() << "Couldn't start command " << command << std::endl;
            }

            thermalvector.clear();

            if(EXIT) {
                pclose(pipe);
                break;
            }
#ifdef PRINT_PERFORMANCE
            auto end = std::chrono::system_clock::now();
            auto elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(end - start);
            std::cout << "Info: record_thermal() (write to disk) took " << elapsed.count() << std::endl;
#endif
        }

#ifdef PRINT_PERFORMANCE
        auto end = std::chrono::system_clock::now();
        auto elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(end - start);
        std::cout << "Info: record_thermal() took " << elapsed.count() << std::endl;
#endif
    }
}


/**
 * Records a video from both cameras and saves it to the location "path".
 * @param path Path where the video is stored.
 */
void record_video_loop() {
    int raw_buffer_size = device->getRawBufferSize();
    if (raw_buffer_size != BLOCKSIZE) {
        std::cerr << get_time() << " +++ ERROR: SIZE HAS CHANGED TO " << std::to_string(raw_buffer_size) << "!!" << std::endl;
    }

    std::thread webcamthread(thread_record_webcam);
    std::thread thermalthread(thread_record_thermal);

    while(! EXIT) {
        using namespace std::chrono_literals;
        std::this_thread::sleep_for(1s);
    }

    webcamthread.join();
    thermalthread.join();
}

void signal_handler_button(int signum) {
    std::cout << get_time() << " Action: Received SIGUSR2 (Submit Button XHR)" << std::endl;
    std::cout << get_time() << " Action: ---------- STOP SESSION ----------" << std::endl << std::endl;
    SESSION = false;
    imager.forceFlagEvent(0);
}



void signal_handler_radio(int signum) {
    if(!SESSION) {
        SESSION = true;
        std::cout << std::endl << get_time() << " Action: ---------- START SESSION ---------" << std::endl;
    }

    std::cout << get_time() << " Action: Received SIGUSR1 (Radio Button XHR)" << std::endl;
    alarm(0); // clear old alarm
    alarm(TIMEOUT); // set new alarm
}



void signal_handler_alarm(int signum) {
    alarm(TIMEOUT);
    if(! SESSION) {
        std::cout << get_time() << " Info: Shuttering Optris ..." << std::endl;
        imager.forceFlagEvent(0);
    } else {
        SESSION = false;
        std::cout << get_time() << " Action: ---------- TIMEOUT ---------" << std::endl << std::endl;
        std::cout << get_time() << " Info: Shuttering Optris ..." << std::endl;
        imager.forceFlagEvent(0);
    }
}



void signal_handler_exit(int signum) {
    std::cout << get_time() << " Action: Received SIGTERM. Preparing EXIT." << std::endl;
    EXIT = true;
}



void onThermalFrame(unsigned short* thermal, unsigned int w, unsigned int h, evo::IRFrameMetadata meta, void* arg) {
    CYCLE += 1;
    if(CYCLE < 100 && ! SHOW_WINDOWS) {
        return;
    }
    CYCLE = 0;

    auto palette = evo::eIron;
    auto scaling_method = evo::eMinMax;

    evo::ImageBuilder imgbuilder = evo::ImageBuilder(false);
    imgbuilder.setPalette(palette);
    imgbuilder.setPaletteScalingMethod(scaling_method);

    imgbuilder.setData(w, h, thermal);

    std::vector<unsigned char> mydata = std::vector<unsigned char>(w * h * 3);
    std::vector<unsigned char> mydata2 = std::vector<unsigned char>(w * h * 3);
    unsigned char *mydata_ptr = &mydata[0];
    unsigned char *mydata_ptr2 = &mydata2[0];

    imgbuilder.convertTemperatureToPaletteImage(mydata_ptr, true);
    imgbuilder.setData(w, h, thermal);

    const cv::Point middle = cv::Point(imgbuilder.getWidth()/2, imgbuilder.getHeight()/2);
    float rect_max_temp = 0;
    for(int i = 0; i < imgbuilder.getWidth(); i++) {
        for(int j = 0; j < imgbuilder.getHeight(); j++) {
            cv::Point current = cv::Point(i, j);

            //if(j > imgbuilder.getHeight() * 0.75 && i > imgbuilder.getWidth() * 0.33 && i < imgbuilder.getWidth() * 0.66) { // RECTANGLE DEFINITION
            if(cv::norm(middle-current) < 50) {
                float temp = imgbuilder.getTemperatureAt(i, j);
                if(temp > rect_max_temp) {
                    rect_max_temp = temp;
                }
            }
        }
    }

    imgbuilder.setPaletteScalingMethod(evo::eManual);
    imgbuilder.setManualTemperatureRange(rect_max_temp - 5, rect_max_temp + 1);
    imgbuilder.convertTemperatureToPaletteImage(mydata_ptr2, true);

    cv::Mat cv_img(cv::Size(w, h), CV_8UC3, mydata_ptr, cv::Mat::AUTO_STEP);
    cv::Mat cv_img2(cv::Size(w, h), CV_8UC3, mydata_ptr2, cv::Mat::AUTO_STEP);
    cv::cvtColor(cv_img, cv_img, CV_BGR2RGB);
    cv::cvtColor(cv_img2, cv_img2, CV_BGR2RGB);

    if(SHOW_WINDOWS) {
        cv::imshow("LIVE_THERMAL", cv_img);
        cv::imshow("LIVE_THERMAL_2", cv_img2);

        for(int i = 0; i < 5; i++) {
            cv::waitKey(3);
        }
    } else {
        cv::imwrite("LIVE_THERMAL.jpg", cv_img);
        cv::imwrite("LIVE_THERMAL_2.jpg", cv_img2);
    }
}


/**
 * This function initializes the thermal imager due to a given config file.
 * Furthermore, a callback function is set which is called on each new thermal frame.
 * The code is based on an example of libirimager.
 * @param conf: a path on the filesystem to the conf.xml-file, which is downloaded by `$ ir_download_calibration`.
 */
void init_optris(std::string conf) {
    evo::IRLogger::setVerbosity(evo::IRLOG_ERROR, evo::IRLOG_OFF);
    evo::IRDeviceParamsReader::readXML(conf.c_str(), params);
    device = evo::IRDevice::IRCreateDevice(params);

    if(imager.init(&params,
                   device->getFrequency(),
                   device->getWidth(),
                   device->getHeight(),
                   device->controlledViaHID())) {
        w = imager.getWidth();
        h = imager.getHeight();

        if (w == 0 || h == 0) {
            std::cout
                    << "Error: Image streams not available or wrongly configured. "
                    <<   "Check connection of camera and config file."
                    << std::endl;
            return;
        }
        std::cout << "Connected camera, serial: " << device->getSerial()
                  << ", HW(Rev.): " << imager.getHWRevision()
                  << ", FW(Rev.): " << imager.getFWRevision() << std::endl;
        std::cout << "Thermal channel: " << w << "x" << h << "@" << params.framerate << "Hz" << std::endl;
    } else {
        std::cerr << "initialization error" << std::endl;
    }

    if(imager.hasBispectralTechnology()) {
        std::cout << "Visible channel: " << imager.getVisibleWidth() << "x" << imager.getVisibleHeight() << "@"
                  << params.framerate << "Hz" << std::endl;
    }

    if(device->startStreaming() != 0) {
        std::cerr << "Error occurred in starting stream ... aborting. "
                     "You may need to reconnect the camera." << std::endl;
        exit(-1);
    } else {
        std::cout << "Starting stream ... OK" << std::endl;
    }

    imager.setThermalFrameCallback(onThermalFrame);
}



int main(int argc, char** argv) {
    if(argc < 2 || argc > 3 ){
        std::cout << "Usage: " << argv[0] << " <file: conf.xml> <optional: show windows (SHOW)>" << std::endl;
        std::cout << "Run this software as wwwrun-apache2-user!" << std::endl;
    }

    if(argc == 3 && std::string(argv[2]) == "SHOW") {
        SHOW_WINDOWS = true;
    }

    // check if file exists
    if(std::ifstream("thermal/raw_data.thermal.0").good() || std::ifstream("thermal/raw_data.thermal.0.gz").good()) {
        std::cerr << "Error: thermal data exists in directory ./thermal/* !!! Exiting." << std::endl;
        exit(EXIT_FAILURE);
    }

    auto start = std::chrono::system_clock::now(); // measure execution time

    init_optris(std::string(argv[1]));

    signal(SIGUSR1, signal_handler_radio);
    signal(SIGUSR2, signal_handler_button);
    signal(SIGTERM, signal_handler_exit);
    signal(SIGALRM, signal_handler_alarm);

    // vc0 is the logitech webcam
    vc0 = cv::VideoCapture(1);
    vc0.set(CV_CAP_PROP_FPS, 25);

    vc0 >> cap0;

    auto end = std::chrono::system_clock::now();
    auto elapsed = std::chrono::duration_cast<std::chrono::milliseconds>(end - start);

    std::cout << get_time() << " Info: Server startup took " << elapsed.count() << " ms." << std::endl << std::endl;
    alarm(TIMEOUT);

    record_video_loop();

    std::cout << get_time() << " Info: Exiting gracefully" << std::endl;
    return 0;
}