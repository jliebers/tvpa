//
// Created by jl on 18.11.18.
//

#ifndef TVPA_SKELETON_H
#define TVPA_SKELETON_H


#include <opencv2/core/mat.hpp>
#include <opencv2/shape.hpp>

#include <iostream>


enum eDistanceCalculationAlgorithm { eHausdorff, eShapeContext, eBoth, eJaccardDistance, eHuMoments1, eHuMoments2, eHuMoments3 };

class Skeleton {
public:
    /**
     * Default constructor should not be used. Throws exception upon call.
     */
    Skeleton() = delete;

    /**
     * Constructs a Skeleton by copying the pointed to mat to m_skeleton.
     * Mat should be a thinned skeleton.
     */
    explicit Skeleton(cv::Mat* mat, std::string = "");

    /**
     * Constructs a new Skeleton from a given file, denoted by `path`.
     * The file should contain a thinned skeleton.
     */
     explicit Skeleton(std::string path);

    /**
     * Returns a pointer to the skeleton. Throws an exception if Skeleton is empty.
     * @return Pointer to m_skeleton
     */
    cv::Mat* get_mat();

    /**
     * Sets the Skeleton to a given Mat. Throws an exception if `mat` is empty.
     * @param mat A non-empty cv::Mat ptr.
     */
    void set_skeleton(cv::Mat *mat, std::string name = "");

    /**
     * Computes the distance between this skeleton to another skeleton.
     * @param other_skeleton_ptr A pointer to the other skeleton, to which this object shall be compared to.
     * @param algorithm An algorithm from eDistanceAlgorithm.
     * @return The distance as double.
     */
    double compute_distance(Skeleton *other_skeleton_ptr, eDistanceCalculationAlgorithm algorithm, bool shuffle = false);

    /**
     * Computes the distance between two skeletons.
     * @param first_skeleton_ptr The first skeleton which is compared to the second skeleton.
     * @param second_skeleton_ptr The second skeleton, which is compared to the first skeleton.
     * @param algorithm An algorithm from eDistanceAlgorithm.
     * @return The distance as double.
     */
    double compute_distance(Skeleton *first_skeleton_ptr, Skeleton *second_skeleton_ptr,
                            eDistanceCalculationAlgorithm algorithm, bool shuffle = false);

    /**
     * Performs a simple thinning of `this` skeleton.
     */
    void thin(std::string algorithm = "zhang_suen");

    /**
     * Counts all white pixels in binary skeleton image.
     * @return Amount of all white pixels in Skeleton
     */
    unsigned long get_white_pixel_count();

private:
    // Contains the Skeleton
    cv::Mat m_skeleton;


    /**
     * Performs a very simple ICP (Iterative Closest Point) for two vectors of points.
     * TODO describe
     * @param c1 Vector 1
     * @param c2 Vector 2
     * @return Similarity/Percentage [0; 1]  how many points from c1 are in c2.
     */
    double jaccard_index(const cv::Mat *m1_ptr, const cv::Mat *m2_ptr);
};


#endif //TVPA_SKELETON_H
