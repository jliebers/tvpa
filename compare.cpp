//
// Created by jl on 17.02.19.
// This program compares two files and returns the output on stdout.
// The output and the files are delimited by tab thus the output can be piped into an arbitrary .csv-file
// and read with a spreadsheet calculation tool.
//

#include <unistd.h>
#include <iostream>
#include <opencv2/imgcodecs.hpp>
#include "Skeleton.h"

int main(int argc, char *argv[]) {
    if(argc < 3 || argc > 4) {
        std::cout << "Usage: " << argv[0] << " <thermal-false-color-img1> <thermal-false-color-img2> <optional: PROCEDURE>" << std::endl;
        std::cout << "PROCEDURE can be a csv-string like '1,2,3' and will then only run procedures 1, 2 and 3. It is possible to omit certain procedures." << std::endl;
        std::cout << "The following procedures are available: " << std::endl;
        std::cout << "  1) eHausdorff" << std::endl;
        std::cout << "  2) eShapeContext" << std::endl;
        std::cout << "  3) eJaccardDistance" << std::endl;
        std::cout << "  4) HuMoments: CONTOURS_MATCH_I1" << std::endl;
        std::cout << "  5) HuMoments: CONTOURS_MATCH_I2" << std::endl;
        std::cout << "  6) HuMoments: CONTOURS_MATCH_I3" << std::endl;
        exit(-1);
    }

    std::string procedures = "";
    if(argc == 4) {
        procedures = std::string(argv[3]);
    }

    cv::Mat m1 = cv::imread(argv[1], cv::IMREAD_GRAYSCALE);
    cv::Mat m2 = cv::imread(argv[2], cv::IMREAD_GRAYSCALE);

    Skeleton s1(&m1);
    Skeleton s2(&m2);

    if(argc == 3 || (argc == 4 && procedures.find('1') != std::string::npos)) {
        std::cout << "HausdorffDistance\t" << argv[1] << "\t" << argv[2] << "\t" << s1.compute_distance(&s2, eHausdorff) << std::endl;
    }

    if(argc == 3 || (argc == 4 && procedures.find('2') != std::string::npos)) {
        std::cout << "ShapeContext\t" << argv[1] << "\t" << argv[2] << "\t" << s1.compute_distance(&s2, eShapeContext) << std::endl;
    }

    if(argc == 3 || (argc == 4 && procedures.find('3') != std::string::npos)) {
        std::cout << "JaccardDist\t" << argv[1] << "\t" << argv[2] << "\t" << s1.compute_distance(&s2, eJaccardDistance) << std::endl;
    }

    if(argc == 3 || (argc == 4 && procedures.find('4') != std::string::npos)) {
        std::cout << "HU1\t" << argv[1] << "\t" << argv[2] << "\t" << s1.compute_distance(&s2, eHuMoments1) << std::endl;
    }

    if(argc == 3 || (argc == 4 && procedures.find('5') != std::string::npos)) {
        std::cout << "HU2\t" << argv[1] << "\t" << argv[2] << "\t" << s1.compute_distance(&s2, eHuMoments2) << std::endl;
    }

    if(argc == 3 || (argc == 4 && procedures.find('6') != std::string::npos)) {
        std::cout << "HU3\t" << argv[1] << "\t" << argv[2] << "\t" << s1.compute_distance(&s2, eHuMoments3) << std::endl;
    }

    return 0; 
}
