#define BOOST_TEST_MODULE Test
#include <boost/test/unit_test.hpp>

// STL
#include <iostream>

// OPENCV
#include <opencv2/core/mat.hpp>
#include <opencv2/opencv.hpp>
#include <mutex>
#include <random>

// TVPA
#include "Video.h"

/**
 * This function check's two cv::Mat for equality
 * @param a cv::Mat
 * @param b cv::Mat
 * @return a == b as a boolean
 */
bool mat_equals(cv::Mat a, cv::Mat b) {
    return cv::sum(a != b) == cv::Scalar(0, 0, 0, 0);
}

std::string get_time(bool delim = true) {
    using namespace std::chrono;
    // get current time
    auto now = system_clock::now();
    // get number of milliseconds for the current second
    // (remainder after division into seconds)
    auto ms = duration_cast<milliseconds>(now.time_since_epoch()) % 1000;
    // convert to std::time_t in order to convert to std::tm (broken time)
    auto timer = system_clock::to_time_t(now);
    // convert to broken time
    std::tm bt = *std::localtime(&timer);
    std::ostringstream oss;
    oss << std::put_time(&bt, "%d-%m-%Y_%H:%M:%S");
    oss << '.' << std::setfill('0') << std::setw(3) << ms.count();
    if(delim) {
        return oss.str() + "   ";
    } else {
        return oss.str();
    }
}

BOOST_AUTO_TEST_SUITE(TVPA_TestSuite);

    /*
     * Simple test to check whether a declared cv::Mat is indeed empty.
     */
    BOOST_AUTO_TEST_CASE(OPENCV_NEW_MAT_IS_EMPTY_TEST) {
        cv::Mat m;
        BOOST_REQUIRE(m.empty());
    }

    /*
     * Performs several tests for the class `Image` defined in Image.h.
     */
    BOOST_AUTO_TEST_CASE(TVPA_IMAGE_TESTS) {
        // Check simple loading of image, getters, setters, default params, serialization
        {
            const std::string name = "capture.png";
            const std::string path = "./../../test-assets/capture.png";
            Image image(path);

            // check defaults for name
            BOOST_CHECK_EQUAL(image.get_name(), "capture.png");

            // check setters and getters for name
            BOOST_CHECK_EQUAL(image.get_name().compare(name), 0);
            image.set_name(name + "2");
            BOOST_CHECK_EQUAL(image.get_name().compare(name + "2"), 0);
            image.set_name(name);
            BOOST_CHECK_EQUAL(image.get_name().compare(name), 0);

            // check storage and retrieval of cv::Mat from disk (serialization should not impact equality)
            BOOST_CHECK(mat_equals(*image.get_image(), cv::imread(path, cv::IMREAD_COLOR)));
            cv::imwrite("/tmp/skeleton1.png", *image.get_skeleton()->get_mat());
            BOOST_CHECK(mat_equals(*image.get_skeleton()->get_mat(),
                                   cv::imread("/tmp/skeleton1.png", cv::IMREAD_GRAYSCALE)));
        }

        // Check whether distance to self always equals zero or is very near zero
        {
            Image image("./../../test-assets/capture.png");
            Skeleton *skel = image.get_skeleton();

            // Self comparison in Hausdorff results in 0
            BOOST_CHECK_EQUAL(skel->compute_distance(skel, skel, eHausdorff), 0);

            // Strangely, self comparison in ShapeContext does not result in a plain 0
            BOOST_CHECK_EQUAL(skel->compute_distance(skel, skel, eShapeContext), 0.00017477701476309448);
        }

        // Check again for another capture
        {
            Image image2("./../../test-assets/capture2.png");
            Skeleton *skel2 = image2.get_skeleton();

            // Self comparison in Hausdorff results in 0
            BOOST_CHECK_EQUAL(skel2->compute_distance(skel2, skel2, eHausdorff), 0);

            // Strangely, self comparison in ShapeContext does not result in a plain 0
            // It is another result than comparing capture1.png with itself!
            BOOST_CHECK_EQUAL(skel2->compute_distance(skel2, skel2, eShapeContext), 0.00044718722347170115);
        }
    }


    BOOST_AUTO_TEST_CASE(BUGREPORT_SHUFFLE) {
        // BEGIN REMOVE ME FOR BUGREPORT
        Image image("./../../test-assets/capture.png");
        cv::imwrite("bugreport_shapecontext_skeleton.png", *image.get_skeleton()->get_mat());
        // END REMOVE ME FOR BUGREPORT

        // Load same image two times and perform an identity check with ShapeContextExtractor and HausdorffDistanceExtractor.
        // Image has checksum sha256 246cb53b310a1b51dd5f428e061990408675798ea1f71a1bf85ad41003e6ebff
        cv::Mat m1 = cv::imread("bugreport_shapecontext_skeleton.png", cv::IMREAD_GRAYSCALE);
        cv::Mat m2 = cv::imread("bugreport_shapecontext_skeleton.png", cv::IMREAD_GRAYSCALE);

        // Create vectors to store data from findContours
        std::vector<std::vector<cv::Point>> contours1, contours2;
        std::vector<cv::Vec4i> hierarchy1, hierarchy2;

        // Find contours
        cv::findContours(m1, contours1, hierarchy1, cv::RETR_LIST, cv::CHAIN_APPROX_NONE);
        cv::findContours(m2, contours2, hierarchy2, cv::RETR_LIST, cv::CHAIN_APPROX_NONE);

        // Merge all points from obtained vectors into one vector
        std::vector<cv::Point> merged_contour1, merged_contour2;
        for(const auto& vec1 : contours1) {
            for (const auto& pt1 : vec1) {
                merged_contour1.push_back(pt1);
            }
        }
        for(const auto& vec2 : contours2) {
            for (const auto& pt2 : vec2) {
                merged_contour2.push_back(pt2);
            }
        }

        // Create distance extractors:
        cv::Ptr<cv::ShapeContextDistanceExtractor> sdeptr = cv::createShapeContextDistanceExtractor();
        cv::Ptr<cv::HausdorffDistanceExtractor> hdeptr = cv::createHausdorffDistanceExtractor(cv::NORM_L2, 0.6);

        // get distances
#ifdef MEASURE_TIME
        auto start1 = std::chrono::system_clock::now();
#endif
        const double hausdorff_val1 = hdeptr->computeDistance(merged_contour1, merged_contour2);
#ifdef MEASURE_TIME
        auto end1 = std::chrono::system_clock::now();
        auto elapsed1 = std::chrono::duration_cast<std::chrono::seconds>(end1 - start1);
        std::cout << "Info: hdeptr->computeDistance() took " << elapsed1.count() << "s" << std::endl;
#endif

        const double shapecontext_val1 = sdeptr->computeDistance(merged_contour1, merged_contour2);

        // shuffle the arrays which were given to both distanceextractors
        std::random_device rd;
        std::mt19937 g(rd());
        std::shuffle(std::begin(merged_contour1), std::end(merged_contour1), g);
        std::shuffle(std::begin(merged_contour2), std::end(merged_contour2), g);

        // get distances again
        const double hausdorff_val2 = hdeptr->computeDistance(merged_contour1, merged_contour2);

#ifdef MEASURE_TIME
        auto start2 = std::chrono::system_clock::now();
#endif
        const double shapecontext_val2 = sdeptr->computeDistance(merged_contour1, merged_contour2);
#ifdef MEASURE_TIME
        auto end2 = std::chrono::system_clock::now();
        auto elapsed2 = std::chrono::duration_cast<std::chrono::seconds>(end2 - start2);
        std::cout << "Info: sdeptr->computeDistance() took " << elapsed1.count() << "s" << std::endl;
#endif

        // Check if distances have changed.
        BOOST_CHECK_EQUAL(hausdorff_val1, hausdorff_val2); // stays zero which is correct for an identity check
        BOOST_CHECK_EQUAL(shapecontext_val1, shapecontext_val2); // is never zero for identity check and changes after shuffle
    }



    BOOST_AUTO_TEST_CASE(BUGREPORT_SHAPECONTEXT) {
        // BEGIN REMOVE ME FOR BUGREPORT
        Image image("./../../test-assets/capture.png");
        cv::imwrite("bugreport_shapecontext_skeleton.png", *image.get_skeleton()->get_mat());
        // END REMOVE ME FOR BUGREPORT

        // Load same image two times.
        // Image has checksum sha256 246cb53b310a1b51dd5f428e061990408675798ea1f71a1bf85ad41003e6ebff
        cv::Mat m1 = cv::imread("bugreport_shapecontext_skeleton.png", cv::IMREAD_GRAYSCALE);
        cv::Mat m2 = cv::imread("bugreport_shapecontext_skeleton.png", cv::IMREAD_GRAYSCALE);

        // Create vectors to store data from findContours
        std::vector<std::vector<cv::Point>> contours1, contours2;
        std::vector<cv::Vec4i> hierarchy1, hierarchy2;

        // Find contours
        cv::findContours(m1, contours1, hierarchy1, cv::RETR_LIST, cv::CHAIN_APPROX_NONE);
        cv::findContours(m2, contours2, hierarchy2, cv::RETR_LIST, cv::CHAIN_APPROX_NONE);

        // Merge all points from obtained vectors into one vector
        std::vector<cv::Point> merged_contour1, merged_contour2;
        for(auto vec1 : contours1) {
            for (auto pt1 : vec1) {
                merged_contour1.push_back(pt1);
            }
        }
        for(auto vec2 : contours2) {
            for (auto pt2 : vec2) {
                merged_contour2.push_back(pt2);
            }
        }

        // Sounds strange, but there is an `==` operator for vectors.
        BOOST_REQUIRE(merged_contour1 == merged_contour2);
        BOOST_REQUIRE(merged_contour1.size() == merged_contour2.size());

        // Calculate shapecontextdistance:
        cv::Ptr<cv::ShapeContextDistanceExtractor> sdeptr = cv::createShapeContextDistanceExtractor();

        const double calculated_shapecontextdistance = sdeptr->computeDistance(merged_contour1, merged_contour2);
        const double shapecontextdistance_on_my_x250 = 0.00076877244282513857;
        const double shapecontextdistance_on_gitlab_runner = 0.00054425356211140752;

        // Does not throw any notice on my X250, but fails on the gitlab runner
        bool enable_test_which_fails_on_gitlab_runner_but_works_on_x250 = false;
        if(enable_test_which_fails_on_gitlab_runner_but_works_on_x250) {
            BOOST_CHECK_EQUAL(calculated_shapecontextdistance, shapecontextdistance_on_my_x250);
        }
        
        // Does not throw any notice on gitlab runner with image opensuse/leap:15.0, but fails on my X250
        BOOST_CHECK_EQUAL(calculated_shapecontextdistance, shapecontextdistance_on_gitlab_runner);

        // Depending on the machine on which the shapecontextdistance is calculated on, the result changes to a small
        // degree. But both values should be equal?
    }

    // OBSOLETE

    BOOST_AUTO_TEST_CASE(BUGREPORT_HAUSDORFF_IDENTITY) {
            // Load two provided images
            // sha256sum 617c290abc816813375ffa37efab9cd58782b76897cce80e6a3106aa365977cc  1.png
            // sha256sum b5094dffc4663a438c741cbfbff6c21691f6af3786ddfbb94c9dc69371a75a96  2.png
            cv::Mat m1 = cv::imread("thermal_P13L3_video.avi_frame0001.png_skeleton.png", cv::IMREAD_GRAYSCALE);
            cv::Mat m2 = cv::imread("thermal_P03L1_video.avi_frame0001.png_skeleton.png", cv::IMREAD_GRAYSCALE);

            // Create vectors to store data from findContours
            std::vector<std::vector<cv::Point>> contours1, contours2;
            std::vector<cv::Vec4i> hierarchy1, hierarchy2;

            // Find contours
            cv::findContours(m1, contours1, hierarchy1, cv::RETR_LIST, cv::CHAIN_APPROX_NONE);
            cv::findContours(m2, contours2, hierarchy2, cv::RETR_LIST, cv::CHAIN_APPROX_NONE);

            // Merge all points from obtained vectors into one vector
            std::vector<cv::Point> merged_contour1, merged_contour2;
            for(auto vec1 : contours1) {
                for (auto pt1 : vec1) {
                    merged_contour1.push_back(pt1);
                }
            }
            for(auto vec2 : contours2) {
                for (auto pt2 : vec2) {
                    merged_contour2.push_back(pt2);
                }
            }

            // Create comparators:
            cv::Ptr<cv::ShapeDistanceExtractor> sdeptr = cv::createShapeContextDistanceExtractor();
            cv::Ptr<cv::ShapeDistanceExtractor> hdeptr = cv::createHausdorffDistanceExtractor(cv::NORM_L2, 0.6);

            BOOST_CHECK_EQUAL(hdeptr->computeDistance(merged_contour1, merged_contour2), 0.0);
            BOOST_CHECK_EQUAL(sdeptr->computeDistance(merged_contour1, merged_contour2), 0.0);
    }



    /**
     * Performs some tests on the rotation of skeletons and computes their distances.
     * Also compares two different images of the same hand (capture.png, Hand1.png) which were taken on different dates.
     */
    BOOST_AUTO_TEST_CASE(HAUSDORFF_SHAPECONTEXT_TESTS) {
        // Only enable this SHOW-bools if you run this on a system with graphical output
        const bool SHOW_ALL = false;
        const bool SHOW_MIRRORED = false;
        const bool SHOW_SMALL = false;
        const bool SHOW_OTHER = false;

        Image image("./../../test-assets/Hand1.png");
        Image image_rotated90("./../../test-assets/Hand1-rot90.png");
        Image image_rotated180("./../../test-assets/Hand1-rot180.png");
        Image image_mirrored("./../../test-assets/Hand1-mirrored.png");
        Image image_small("./../../test-assets/Hand1-less_resolution.png");
        Image image_other("./../../test-assets/capture.png");

        eSkeletonExtractionAlgorithm algo = eAdaptiveThreshold;

        // Check distances of mirrored skeleton
        BOOST_CHECK_EQUAL(image.get_skeleton()->compute_distance(image_mirrored.get_skeleton(), eHausdorff), 13.601470947265625);
        BOOST_CHECK_EQUAL(image.get_skeleton()->compute_distance(image_mirrored.get_skeleton(), eShapeContext), 251.89987182617188);
        if (SHOW_ALL || SHOW_MIRRORED) {
            cv::imshow("mirrored_skeleton_test_1 (press q to continue)", *image.get_skeleton()->get_mat());
            cv::imshow("mirrored_skeleton_test_2 (press q to continue)", *image_mirrored.get_skeleton()->get_mat());
            while (cv::waitKey(1) != int('q'));
            cv::destroyAllWindows();
        }

        // Check distances of small skeleton
        BOOST_CHECK_EQUAL(image.get_skeleton()->compute_distance(image_small.get_skeleton(), eHausdorff), 114.28035736083984);
        BOOST_CHECK_EQUAL(image.get_skeleton()->compute_distance(image_small.get_skeleton(), eShapeContext), 0.43253105878829956);
        if (SHOW_ALL || SHOW_SMALL) {
            cv::imshow("small_skeleton_test_1 (press q to continue)", *image.get_skeleton()->get_mat());
            cv::imshow("small_skeleton_test_2 (press q to continue)", *image_small.get_skeleton()->get_mat());
            while (cv::waitKey(1) != int('q'));
            cv::destroyAllWindows();
        }

        // Check distances of other skeleton
        BOOST_CHECK_EQUAL(image.get_skeleton()->compute_distance(image_other.get_skeleton(), eHausdorff), 6.0827627182006836);
        BOOST_CHECK_EQUAL(image.get_skeleton()->compute_distance(image_other.get_skeleton(), eShapeContext), 43.845077514648438);
        if (SHOW_ALL || SHOW_OTHER) {
            cv::imshow("other_skeleton_test_1 (press q to continue)", *image.get_skeleton()->get_mat());
            cv::imshow("other_skeleton_test_2 (press q to continue)", *image_other.get_skeleton()->get_mat());
            while (cv::waitKey(1) != int('q'));
            cv::destroyAllWindows();
        }

        // Check distances of rotated skeletons - 90 degrees
        // Proof, that both approaches are not rotation invariant
        BOOST_CHECK_EQUAL(image.get_skeleton()->compute_distance(image_rotated90.get_skeleton(), eHausdorff), 16.401220321655273);
        BOOST_CHECK_EQUAL(image.get_skeleton()->compute_distance(image_rotated90.get_skeleton(), eShapeContext), 56.533924102783203);
        if (SHOW_ALL || SHOW_OTHER) {
            cv::imshow("rotated90_skeleton_test1 (press q to continue)", *image.get_skeleton()->get_mat());
            cv::imshow("rotated90_skeleton_test2 (press q to continue)", *image_rotated90.get_skeleton()->get_mat());
            while (cv::waitKey(1) != int('q'));
            cv::destroyAllWindows();
        }

        // Check distances of rotated skeletons - 180 degrees
        // Proof, that both approaches are not rotation invariant
        BOOST_CHECK_EQUAL(image.get_skeleton()->compute_distance(image_rotated180.get_skeleton(), eHausdorff), 2);
        BOOST_CHECK_EQUAL(image.get_skeleton()->compute_distance(image_rotated180.get_skeleton(), eShapeContext), 4.2961678504943848);
        if (SHOW_ALL || SHOW_OTHER) {
            cv::imshow("rotated180_skeleton_test1 (press q to continue)", *image.get_skeleton()->get_mat());
            cv::imshow("rotated180_skeleton_test2 (press q to continue)", *image_rotated180.get_skeleton()->get_mat());
            while (cv::waitKey(1) != int('q'));
            cv::destroyAllWindows();
        }

        // Check distances against self
        const double dist1 = 0.00044310875819064677,
                     dist2 = 0.0095564005896449089,
                     dist3 = 0.00017477701476309448;

        const double dist1_rot = 0.00025723056751303375,
                     dist2_rot = 0.0090647274628281593,
                     dist3_rot = 0.00017796899192035198;

        BOOST_CHECK_EQUAL(image.get_skeleton()->compute_distance(image_mirrored.get_skeleton(),
                                                                 image_mirrored.get_skeleton(),
                                                                 eHausdorff), 0);
        BOOST_CHECK_EQUAL(image.get_skeleton()->compute_distance(image_mirrored.get_skeleton(),
                                                                 image_mirrored.get_skeleton(),
                                                                 eShapeContext), dist1);
        BOOST_CHECK_EQUAL(image.get_skeleton()->compute_distance(image_small.get_skeleton(),
                                                                 image_small.get_skeleton(),
                                                                 eHausdorff), 0);
        BOOST_CHECK_EQUAL(image.get_skeleton()->compute_distance(image_small.get_skeleton(),
                                                                 image_small.get_skeleton(),
                                                                 eShapeContext), dist2);
        BOOST_CHECK_EQUAL(image.get_skeleton()->compute_distance(image_other.get_skeleton(),
                                                                 image_other.get_skeleton(),
                                                                 eHausdorff), 0);
        BOOST_CHECK_EQUAL(image.get_skeleton()->compute_distance(image_other.get_skeleton(),
                                                                 image_other.get_skeleton(),
                                                                 eShapeContext), dist3);

        // Check distances against self with shuffled contour vectors
        // Proof, that shuffling the contour data does not affect eShapeContext nor eHausdorff.
        BOOST_CHECK_EQUAL(image.get_skeleton()->compute_distance(image_mirrored.get_skeleton(),
                                                                 image_mirrored.get_skeleton(),
                                                                 eHausdorff, true), 0);
        BOOST_CHECK_EQUAL(image.get_skeleton()->compute_distance(image_mirrored.get_skeleton(),
                                                                 image_mirrored.get_skeleton(),
                                                                 eShapeContext, true), dist1_rot);
        BOOST_CHECK_EQUAL(image.get_skeleton()->compute_distance(image_small.get_skeleton(),
                                                                 image_small.get_skeleton(),
                                                                 eHausdorff, true), 0);
        BOOST_CHECK_EQUAL(image.get_skeleton()->compute_distance(image_small.get_skeleton(),
                                                                 image_small.get_skeleton(),
                                                                 eShapeContext, true), dist2_rot);
        BOOST_CHECK_EQUAL(image.get_skeleton()->compute_distance(image_other.get_skeleton(),
                                                                 image_other.get_skeleton(),
                                                                 eHausdorff, true), 0);
        BOOST_CHECK_EQUAL(image.get_skeleton()->compute_distance(image_other.get_skeleton(),
                                                                 image_other.get_skeleton(),
                                                                 eShapeContext, true), dist3_rot);
    }


    BOOST_AUTO_TEST_CASE(JACCARD_TESTS) {
        const bool SHOW_ALL = false;

        Skeleton s1("../../test-assets/thermal_P01L4_video.avi_frame0001.png_skeleton.png");
        Skeleton s2("../../test-assets/thermal_P01L4_video.avi_frame0002.png_skeleton.png");
        Skeleton s_other("../../test-assets/cap_other.png");

        BOOST_CHECK_EQUAL(s1.compute_distance(&s2, eJaccardDistance), 0.89595375722543358);
        BOOST_CHECK_EQUAL(s_other.compute_distance(&s2, eJaccardDistance), 0.11607786589762076);

        if (SHOW_ALL) {
            cv::imshow("s1 (press q to continue)", *s1.get_mat());
            cv::imshow("s2 (press q to continue)", *s2.get_mat());
            while (cv::waitKey(1) != int('q'));
            cv::destroyAllWindows();
        }
    }


    BOOST_AUTO_TEST_CASE(HU_MOMENTS_TESTS) {
        Skeleton s1("../../test-assets/thermal_P01L4_video.avi_frame0001.png_skeleton.png");
        Skeleton s2("../../test-assets/thermal_P01L4_video.avi_frame0002.png_skeleton.png");
        Skeleton s_other("../../test-assets/cap_other.png");

        BOOST_CHECK_EQUAL(s1.compute_distance(&s2, eHuMoments1), 0.015677600335109593);
        BOOST_CHECK_EQUAL(s1.compute_distance(&s2, eHuMoments2), 0.068931036634043874);
        BOOST_CHECK_EQUAL(s1.compute_distance(&s2, eHuMoments3), 0.022360393420721877);

        BOOST_CHECK_EQUAL(s1.compute_distance(&s1, eHuMoments1), 0);
        BOOST_CHECK_EQUAL(s1.compute_distance(&s1, eHuMoments2), 0);
        BOOST_CHECK_EQUAL(s1.compute_distance(&s1, eHuMoments3), 0);

        BOOST_CHECK_EQUAL(s_other.compute_distance(&s1, eHuMoments1), 0.044293959469453026);
        BOOST_CHECK_EQUAL(s_other.compute_distance(&s1, eHuMoments2), 0.10053291814236442);
        BOOST_CHECK_EQUAL(s_other.compute_distance(&s1, eHuMoments3), 0.068994444201500485);

        Image hand1("../../test-assets/Hand1.png");
        Image hand1_rot45("../../test-assets/Hand1-rot45.png");;
        Image hand1_rot90("../../test-assets/Hand1-rot90.png");
        Image hand1_rot180("../../test-assets/Hand1-rot180.png");

        BOOST_CHECK_EQUAL(hand1.get_skeleton()->compute_distance(hand1_rot45.get_skeleton(), eHuMoments1), 0.0096663179759179929);
        BOOST_CHECK_EQUAL(hand1.get_skeleton()->compute_distance(hand1_rot90.get_skeleton(), eHuMoments1), 0.015621907498177268);
        BOOST_CHECK_EQUAL(hand1.get_skeleton()->compute_distance(hand1_rot180.get_skeleton(), eHuMoments1), 0.0051084153135145804);

        BOOST_CHECK_EQUAL(hand1.get_skeleton()->compute_distance(hand1_rot45.get_skeleton(), eHuMoments2), 0.021224834559968819);
        BOOST_CHECK_EQUAL(hand1.get_skeleton()->compute_distance(hand1_rot90.get_skeleton(), eHuMoments2), 0.034003886654872417);
        BOOST_CHECK_EQUAL(hand1.get_skeleton()->compute_distance(hand1_rot180.get_skeleton(), eHuMoments2), 0.011292536589735835);

        BOOST_CHECK_EQUAL(hand1.get_skeleton()->compute_distance(hand1_rot45.get_skeleton(), eHuMoments3), 0.014221401191454359);
        BOOST_CHECK_EQUAL(hand1.get_skeleton()->compute_distance(hand1_rot90.get_skeleton(), eHuMoments3), 0.022783824901973324);
        BOOST_CHECK_EQUAL(hand1.get_skeleton()->compute_distance(hand1_rot180.get_skeleton(), eHuMoments3), 0.0075664049516175396);
    }

    BOOST_AUTO_TEST_CASE(HU_MOMENTS) {

    }

    BOOST_AUTO_TEST_CASE(SHAPECONTEXT_HAUSDORFF_LATEX_TABLE) {
        Skeleton s1 ("../../test-assets/4skeletons1.png");
        Skeleton s1_("../../test-assets/4skeletons2.png");
        Skeleton s2 ("../../test-assets/4skeletons3.png");
        Skeleton s3 ("../../test-assets/4skeletons4.png");

        std::vector<Skeleton> v1 = {s1 , s1_, s2, s3}, v2 = {s1 ,s1_, s2, s3};
        std::vector<double> doubles;

        for(Skeleton &sk1 : v1) {
            for(Skeleton &sk2 : v2) {
                double d = sk1.compute_distance(&sk2, eHausdorff);
                doubles.push_back(d);
            }

            for(Skeleton &sk2 : v2) {
                double d = sk1.compute_distance(&sk2, eShapeContext);
                doubles.push_back(d);
            }
        }

        std::vector<std::string> out;
        for(double d : doubles) {
            std::stringstream stream;
            stream << std::fixed << std::setprecision(2) << d;
            out.emplace_back(stream.str());
        }

        BOOST_REQUIRE_EQUAL(out.size(), 32);

        // Copy results to latex
        std::cout << "%GENERATING LATEX OUTPUT CODE: " << std::endl;
        std::cout << "$S_{1}$"  << " & " << out[0]  << " & " << out[1]  << " & " << out[2]  << " & " << out[3]  << " & " << out[4]  << " & " << out[5]  << " & " << out[6]  << " & " << out[7]  << " \\\\" << std::endl;
        std::cout << "$S_{1'}$" << " & " << out[8]  << " & " << out[9]  << " & " << out[10] << " & " << out[11] << " & " << out[12] << " & " << out[13] << " & " << out[14] << " & " << out[15] << " \\\\" << std::endl;
        std::cout << "$S_{2}$"  << " & " << out[16] << " & " << out[17] << " & " << out[18] << " & " << out[19] << " & " << out[20] << " & " << out[21] << " & " << out[22] << " & " << out[23] << " \\\\" << std::endl;
        std::cout << "$S_{3}$"  << " & " << out[24] << " & " << out[25] << " & " << out[26] << " & " << out[27] << " & " << out[28] << " & " << out[29] << " & " << out[30] << " & " << out[31] << " \\\\" << std::endl;
        std::cout << "%END OF GENERATED LATEX OUTPUT CODE." << std::endl;
    }



    BOOST_AUTO_TEST_CASE(HU_MOMENTS_LATEX_TABLE) {
        Skeleton s1 ("../../test-assets/4skeletons1.png");
        Skeleton s1_("../../test-assets/4skeletons2.png");
        Skeleton s2 ("../../test-assets/4skeletons3.png");
        Skeleton s3 ("../../test-assets/4skeletons4.png");

        std::vector<Skeleton> v1 = {s1 , s1_, s2, s3}, v2 = {s1 ,s1_, s2, s3};
        std::vector<double> doubles;

        for(Skeleton &sk1 : v1) {
            for(Skeleton &sk2 : v2) {
                double d = sk1.compute_distance(&sk2, eHuMoments1);
                doubles.push_back(d);
            }

            for(Skeleton &sk2 : v2) {
                double d = sk1.compute_distance(&sk2, eHuMoments2);
                doubles.push_back(d);
            }

            for(Skeleton &sk2 : v2) {
                double d = sk1.compute_distance(&sk2, eHuMoments3);
                doubles.push_back(d);
            }
        }

        std::vector<std::string> out;
        for(double d : doubles) {
            std::stringstream stream;

            if(true)
                stream << std::fixed << std::setprecision(4) << d; // maybe use std::scientific instead of std::fixed
            else
                stream << std::scientific << std::setprecision(3) << "$" << d << "$";

            out.emplace_back(stream.str());
        }

        BOOST_REQUIRE_EQUAL(out.size(), 48);
        int counter = 0;

        // Copy results to latex
        std::cout << "%GENERATING LATEX OUTPUT CODE: " << std::endl;

        std::cout << "$S_{1}$";
        for(int i = 0; i < out.size()/4; i++) {
            std::cout << " & " << out[counter++];
        }
        std::cout << " \\\\" << std::endl;

        std::cout << "$S_{1'}$";
        for(int i = 0; i < out.size()/4; i++) {
            std::cout << " & " << out[counter++];
        }
        std::cout << " \\\\" << std::endl;

        std::cout << "$S_{2}$";
        for(int i = 0; i < out.size()/4; i++) {
            std::cout << " & " << out[counter++];
        }
        std::cout << " \\\\" << std::endl;

        std::cout << "$S_{3}$";
        for(int i = 0; i < out.size()/4; i++) {
            std::cout << " & " << out[counter++];
        }
        std::cout << " \\\\" << std::endl;

        std::cout << "%END OF GENERATED LATEX OUTPUT CODE." << std::endl;
    }


/*
    BOOST_AUTO_TEST_CASE(THESIS) {
        Image img("../../tmp/proc1.png");

        eSkeletonExtractionAlgorithm algo = eEdgeDetection;

        cv::imshow("Skeleton", *img.get_skeleton(algo)->get_mat());
        cv::imshow("Debug", img.get_debug_mat(algo));

        while(cv::waitKey(1) != int('q'));
    }
*/

/*    BOOST_AUTO_TEST_CASE(ADD_CBARS) {
        std::vector<std::pair<float, float>> temps;
        temps.emplace_back(std::pair<float, float>(30.0, 39.0));
        temps.emplace_back(std::pair<float, float>(30.0, 39.0));
        temps.emplace_back(std::pair<float, float>(26.0, 35.0));
        temps.emplace_back(std::pair<float, float>(26.0, 35.0));
        temps.emplace_back(std::pair<float, float>(31.0, 38.0));
        temps.emplace_back(std::pair<float, float>(31.0, 38.0));
        temps.emplace_back(std::pair<float, float>(31.0, 38.0));
        temps.emplace_back(std::pair<float, float>(31.0, 38.0));
        temps.emplace_back(std::pair<float, float>(30.0, 35.0));
        temps.emplace_back(std::pair<float, float>(31.0, 37.0));
        temps.emplace_back(std::pair<float, float>(25.0, 35.0));
        temps.emplace_back(std::pair<float, float>(25.0, 35.0));
        temps.emplace_back(std::pair<float, float>(31.0, 37.0));
        temps.emplace_back(std::pair<float, float>(31.0, 39.0));
        temps.emplace_back(std::pair<float, float>(31.0, 39.0));
        temps.emplace_back(std::pair<float, float>(30.0, 40.0));
        temps.emplace_back(std::pair<float, float>(30.0, 36.0));
        temps.emplace_back(std::pair<float, float>(30.0, 35.0));
        temps.emplace_back(std::pair<float, float>(30.0, 36.0));
        temps.emplace_back(std::pair<float, float>(30.0, 37.0));
        temps.emplace_back(std::pair<float, float>(26.0, 35.0));
        temps.emplace_back(std::pair<float, float>(26.0, 35.0));
        temps.emplace_back(std::pair<float, float>(26.0, 33.0));
        temps.emplace_back(std::pair<float, float>(26.0, 33.0));
        temps.emplace_back(std::pair<float, float>(31.0, 37.0));
        temps.emplace_back(std::pair<float, float>(31.0, 38.0));
        temps.emplace_back(std::pair<float, float>(30.0, 36.0));
        temps.emplace_back(std::pair<float, float>(30.0, 36.0));
        temps.emplace_back(std::pair<float, float>(30.0, 35.0));
        temps.emplace_back(std::pair<float, float>(30.0, 35.0));
        temps.emplace_back(std::pair<float, float>(26.0, 31.0));
        temps.emplace_back(std::pair<float, float>(25.0, 33.0));
        temps.emplace_back(std::pair<float, float>(26.0, 33.0));
        temps.emplace_back(std::pair<float, float>(26.0, 33.0));
        temps.emplace_back(std::pair<float, float>(27.0, 34.0));
        temps.emplace_back(std::pair<float, float>(27.0, 34.0));
        temps.emplace_back(std::pair<float, float>(30.0, 37.0));
        temps.emplace_back(std::pair<float, float>(30.0, 37.0));
        temps.emplace_back(std::pair<float, float>(30.0, 36.0));
        temps.emplace_back(std::pair<float, float>(30.0, 37.0));

        cv::Mat cbar = cv::imread("/tmp/cbars/palettebar.ppm");
        for(int i = 1; i <= 40; ++i) {
            std::string load = "/tmp/cbars/" + std::to_string(i) + ".png";
            std::cout << load << std::endl;

            cv::Mat m = cv::imread(load);

            cv::Mat white_space = cv::Mat::zeros(288, 5, CV_8UC3);
            white_space = cv::Scalar(255,255,255);

            cv::hconcat(m, white_space, m);
            cv::hconcat(m, cbar, m);

            cv::Mat tmplabel_bar = cv::Mat::zeros(288, 55, CV_8UC3);
            tmplabel_bar = cv::Scalar(255,255,255);

            cv::putText(tmplabel_bar,
                        std::to_string(temps[i-1].second).substr(0,4),
                        cv::Point(3,15),
                        cv::FONT_HERSHEY_COMPLEX_SMALL,
                        1.0,
                        cv::Scalar(0,0,0));
            cv::putText(tmplabel_bar,
                        std::to_string(temps[i-1].first).substr(0,4),
                        cv::Point(3,288-5),
                        cv::FONT_HERSHEY_COMPLEX_SMALL,
                        1.0,
                        cv::Scalar(0,0,0));

            cv::hconcat(m, tmplabel_bar, m);

            cv::imwrite(load, m);

            //cv::imshow("m", m);
            //cv::waitKey(1000);
        }
    } */




BOOST_AUTO_TEST_SUITE_END();