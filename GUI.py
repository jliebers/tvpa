from tkinter import Tk, messagebox, Menu, Toplevel, Button, simpledialog, filedialog, LabelFrame, Label, Entry, LEFT, RIGHT
import subprocess
import shutil
import os
import multiprocessing

root = Tk()


def command_exists(command: str) -> bool:
    if shutil.which(command) is None:
        messagebox.showinfo("Missing dependency", "Error: the command '" + command + "' is missing on your system.")
        return False
    else:
        return True


def install_run_cmake():
    print("t1")


def install_compile_all():
    print("t2")


def install_generate_conf():
    print("conf")


def record_hand():
    recordtool = './cmake-build-debug/tvpa-record'
    folder = simpledialog.askstring("Name", "Please enter name of subject.\n"
                                    "A new folder is created to store the subject's data.")

    if folder is None:
        # user cancelled simpledialog
        return

    if not os.path.isfile(recordtool):
        messagebox.showerror('The tvpa-record-tool does not exist at ./cmake-build-src/record. Was it compiled?')
        return

    try:
        os.mkdir(folder)
    except FileExistsError as fee:
        messagebox.showerror(type(fee).__name__, str(fee) + "\n(The folder probably already exists.)")
        return

    shutil.copy(recordtool, './' + folder)
    cwd = os.path.dirname(os.path.realpath(__file__)) + '/' + folder

    try:
        output = subprocess.check_output(['./tvpa-record', "../conf.xml", folder], cwd=cwd, stderr=subprocess.STDOUT)
        messagebox.showinfo("tvpa-record", "Recording OK.\n\nSaved all files to: " + cwd
                            + "\n\ntvpa-record-output:\n" + output.decode())
    except subprocess.CalledProcessError as cpe:
        messagebox.showerror("Error in tvpa-record", cpe.output.decode() + "\n\nExit code: " + str(cpe.returncode))
        shutil.rmtree(folder)
        return

    return folder, cwd


def process(cwd):
    folder = os.path.basename(cwd)
    videoname = 'thermal_' + folder + '_video.avi'
    folder = folder + '/'

    # check if video exists if not return
    if not os.path.isfile(folder+videoname):
        messagebox.showerror("Convert error", "Could not find file: " + folder + videoname + "\nDid you record a file?")
        return

    if not (command_exists('cp') and command_exists('mkdir') and command_exists('ffmpeg') and command_exists('find')):
        return

    bash_command = 'cp ../cmake-build-debug/convert . && mkdir frames && for file in *avi; do ffmpeg -loglevel panic ' \
                   '-i $file frames/${file}_frame%04d.png; done && mv convert frames/convert && cd frames && for file '\
                   'in *.png; do ./convert $file 2>&1 >> ../convert.log; done; mkdir original skeleton debug && ' \
                   'find . -name "*skeleton*.png" -exec mv -t ./skeleton/ {} + && find . -name "*debug*.png" -exec mv '\
                   '-t ./debug {} + && mv *.png original '

    try:
        messagebox.showinfo("process", "Starting convert. This might take a while.\n"
                                       "Processing starts when you press 'OK' and ends when this window closes.")
        process = subprocess.check_output(bash_command, stderr=subprocess.STDOUT, cwd=cwd, shell=True)
    except subprocess.CalledProcessError as cpe:
        print("Some errors occured ...")
        print(cpe.output.decode())
        messagebox.showwarning("process", "Finished processing. There were some warnings.\n"
                                          "Please check the created log.")
        return

    if len(process.decode()) == 0:
        messagebox.showinfo("process", "Finished processing.\nYour output is in: " + cwd)
    else:
        messagebox.showwarning("process", "Finished processing. Output: \n\n" + process.decode()
                            + "\n\nMore information can be found in the convert.log")


def record_hand_and_process():
    folder, cwd = record_hand()
    process(folder, cwd)


def pick_process_folder():
    picked = filedialog.askdirectory()
    if len(picked) == 0:  # user cancelled dialogue
        return
    process(picked)


def compare_skeletons():
    file1 = filedialog.askopenfilename()
    if len(file1) == 0:  # user cancelled dialogue
        return

    file2 = filedialog.askopenfilename()
    if len(file2) == 0:  # user cancelled dialogue
        return

    options = '1,2,3,4,5,6'

    try:
        messagebox.showinfo("tvpa-compare", "Starting comparison. This might take a while.\n\nFile1: "+file1+"\n\nFile2:"+file2+"\n\nPress 'OK' to start.")
        output = subprocess.check_output(['./cmake-build-debug/compare', file1, file2, options])
        results = output.decode()
        results = " ".join(filter(lambda x:x[0] != '/', results.split()))

        res = ""
        for i in range(0, len(results.split(" "))):
            res += results.split(" ")[i] + "   "
            if i % 2 == 1:
                res += '\n'

        messagebox.showinfo("tvpa-compare", "Finished comparison\n\nFile1: "+file1+"\n\nFile2:"+file2+"\n\n" + res)
    except subprocess.CalledProcessError as cpe:
        messagebox.showerror("Error in tvpa-compare", cpe.output.decode() + "\n\nExit code: " + str(cpe.returncode))
        return


def compare_folders():
    import glob
    folder1 = filedialog.askdirectory()
    if len(folder1) == 0:  # user cancelled dialogue
        return

    folder2 = filedialog.askdirectory()
    if len(folder2) == 0:  # user cancelled dialogue
        return

    files1 = glob.glob(folder1 + "/*.png")
    files2 = glob.glob(folder2 + "/*.png")

    options = '1,2,3,4,5,6'

    if len(files1) == 0:
        messagebox.showerror("No PNG found", "Error. No .png-files were found in " + folder1)
        return

    if len(files2) == 0:
        messagebox.showerror("No PNG found", "Error. No .png-files were found in " + folder2)
        return

    messagebox.showinfo("tvpa-compare", "Starting " + str(len(files1)*len(files2)) + " comparisons. "
                        "This will take a while. You will be notified once the file is ready. This window might freeze."
                        "Please specify location to save tab-separated CSV-file.")

    logpath = filedialog.asksaveasfilename()

    if len(logpath) == 0:
        return

    if not os.path.isfile(logpath):
        logfile = open(logpath, "w")
    else:
        messagebox.showerror("Error", "Logfile already exists. Please choose another location.")
        return

    children = 0
    print("CPUs available: " + str(multiprocessing.cpu_count()))

    for f1 in files1:
        for f2 in files2:
            try:
                if children >= multiprocessing.cpu_count():
                    os.wait()  # wait for some child to terminate so we do not overload CPU
                    children -= 1
                else:
                    newpid = os.fork()
                    children += 1

                if newpid == 0:
                    # child
                    output = subprocess.check_output(['./cmake-build-debug/compare', f1, f2, options])
                    results = output.decode()
                    logfile.write(results)
                    logfile.flush()
                    exit(0)
                else:
                    # parent
                    pids = (os.getpid(), newpid)
                    print("parent: %d spawned child: %d" % pids)
            except subprocess.CalledProcessError as cpe:
                continue

    # Wait for all other childs to terminate:
    while children > 0:
        os.wait()
        children -= 1

    logfile.close()
    messagebox.showinfo("Finished", "Calculation finished. Tab-seperated csv-file has been written to " + logpath)


def view_raw_thermal_data():
    viewer = './cmake-build-debug/tvpa-viewer'
    file = filedialog.askopenfile()

    if file is None:
        # user cancelled simpledialog
        return

    if not os.path.isfile(viewer):
        messagebox.showerror('error',
                             'The tvpa-viewer-tool does not exist at '+viewer+'. Was it compiled?')
        return

    try:
        output = subprocess.check_output([viewer, "./conf.xml", file.name])
    except subprocess.CalledProcessError as cpe:
        messagebox.showerror("Error in tvpa-viewer", cpe.output.decode() + "\n\nExit code: " + str(cpe.returncode))
        return


def setup_gui():
    def donothing():
        filewin = Toplevel(root)
        button = Button(filewin, text="Do nothing button")
        button.pack()

    menubar = Menu(root)

    installmenu = Menu(menubar, tearoff=0)
    installmenu.add_command(label="Run cmake", command=install_run_cmake)
    installmenu.add_command(label="Compile tvpa", command=install_compile_all)
    installmenu.add_command(label="Generate conf.xml", command=install_generate_conf)
    menubar.add_cascade(label="Install", menu=installmenu)

    recordmenu = Menu(menubar, tearoff=0)
    recordmenu.add_command(label="Record new hand", command=record_hand)
    recordmenu.add_command(label="Record new hand and convert results", command=record_hand_and_process)
    menubar.add_cascade(label="Record", menu=recordmenu)

    convertmenu = Menu(menubar, tearoff=0)
    convertmenu.add_command(label="Convert a folder", command=pick_process_folder)
    menubar.add_cascade(label="Convert", menu=convertmenu)

    comparemenu = Menu(menubar, tearoff=0)
    comparemenu.add_command(label="Compare two skeletons", command=compare_skeletons)
    comparemenu.add_command(label="Compare two folders", command=compare_folders)
    menubar.add_cascade(label="Compare", menu=comparemenu)

    # TODO
    rawdatamenu = Menu(menubar, tearoff=0)
    rawdatamenu.add_command(label="View raw thermal data", command=view_raw_thermal_data)
    #rawdatamenu.add_command(label="Parse a log from server", command=donothing)
    #rawdatamenu.add_command(label="Compare two folders", command=donothing)
    menubar.add_cascade(label="Raw Thermal", menu=rawdatamenu)

    #helpmenu = Menu(menubar, tearoff=0)
    #helpmenu.add_command(label="Help Index", command=donothing)
    #helpmenu.add_command(label="About...", command=donothing)
    #menubar.add_cascade(label="Help", menu=helpmenu)

    labelframe = LabelFrame(root, text="Optris exposure settings")
    labelframe.pack(fill="both", expand="yes")
    L1 = Label(labelframe, text="User Name")
    L1.pack(side=LEFT)
    E1 = Entry(labelframe, bd=5)
    E1.pack(side=RIGHT)

    root.config(menu=menubar)
    root.title('tvpa-gui')
    root.mainloop()


if __name__ == "__main__":
    setup_gui()
