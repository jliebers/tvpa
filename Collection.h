//
// Created by jl on 21.11.18.
//

#ifndef TVPA_COLLECTION_H
#define TVPA_COLLECTION_H

#define COLLECTION_TIME_MEASURE

#include "Video.h"
#include <boost/filesystem.hpp>



class Collection {
public:
    explicit Collection() = delete;
    explicit Collection(std::string path);
    ~Collection();

    unsigned get_size();
    std::vector<std::pair<double, Image*>> compare(Image* image, eDistanceCalculationAlgorithm algo);
private:
    std::vector<Image*> m_collection;
};


#endif //TVPA_COLLECTION_H
