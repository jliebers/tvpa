//
// Created by jl on 18.11.18.
//


#include <iostream>
#include <boost/filesystem/path.hpp>
#include "Image.h"
#include "Collection.h"

int main(int argc, char *argv[]) {
    // argv[0]: binary
    // argv[1]: collection-path
    // argv[2]: image/video-path

    if(argc != 3){
        std::cerr << "check arg" << std::endl;
        return EXIT_FAILURE;
    }

    const std::string param1 = std::string(argv[1]);
    const std::string param2 = std::string(argv[2]);

    Collection* vcptr = new Collection(param1);

    cv::Mat input_image = *Video(param2).get_frame(0)->get_image();

    std::string imagename = boost::filesystem::path(param2).filename().string();
    Image image(input_image, imagename);

    cv::imshow("Skeleton", *image.get_image());
    cv::waitKey(1);cv::waitKey(1);cv::waitKey(1);cv::waitKey(1);cv::waitKey(1);
    cv::imshow("Skeleton-input", *image.get_skeleton()->get_mat());
    cv::waitKey(1);cv::waitKey(1);cv::waitKey(1);cv::waitKey(1);cv::waitKey(1);


    std::vector<std::pair<double, Image*>> results_shapecontext = vcptr->compare(&image, eShapeContext);
    std::vector<std::pair<double, Image*>> results_hausdorff = vcptr->compare(&image, eHausdorff);
    std::vector<std::pair<double, Image*>> results_both = vcptr->compare(&image, eBoth);

    return EXIT_SUCCESS;
}