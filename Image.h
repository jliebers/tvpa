//
// Created by jl on 18.11.18.
//

#ifndef TVPA_IMAGE_H
#define TVPA_IMAGE_H

// OPENCV
#include <opencv2/opencv.hpp>
#include <opencv2/core/mat.hpp>

// TVPA
#include "Skeleton.h"
#include "ImageProcessor.h"

// BOOST
#include <boost/filesystem/path.hpp>

// STL
#include <utility>

// Forward declaration
class ImageProcessor;

enum eSkeletonExtractionAlgorithm : short { eGlobalThreshold, eAdaptiveThreshold, eEdgeDetection };

class Image {
public:
    explicit Image(cv::Mat image, std::string name);
    explicit Image(std::string path);
    ~Image();

    std::string get_name();
    void set_name(std::string new_name);

    cv::Mat* get_image();

    /**
     *
     * @param algorithm
     * @throw std::logic_error
     * @return
     */
    Skeleton* get_skeleton(eSkeletonExtractionAlgorithm algorithm = eAdaptiveThreshold);

    /**
     *
     * @param algorithm
     * @throw std::logic_error
     * @return
     */
    cv::Mat get_debug_mat(eSkeletonExtractionAlgorithm algorithm = eAdaptiveThreshold);

private:
    // Contains a description of the image
    std::string m_name = "unknown-image";

    // Contains the original frame
    cv::Mat* m_original_image_ptr = nullptr;

    // Contains the Skeleton:
    Skeleton* m_skeleton_ptr = nullptr;

    // Contains the ImageProcessor which transforms the given image into a Skeleton:
    ImageProcessor* m_ip_ptr = nullptr;
};


#endif //TVPA_IMAGE_H
