//
// Created by jl on 29.05.19.
//

#include <iostream>
#include <unordered_map>
#include <vector>
#include <fstream>
#include <algorithm>
#include <cstring>
#include <cmath>
#include <execinfo.h>
#include <csignal>
#include <unistd.h>

// Enable one (1) of these depending on your dataset:
//#define DATASET_STUDY_PUBLICATION
//#define DATASET_STUDY_THESIS_G1
//#define DATASET_STUDY_THESIS_G2
#define DATASET_STUDY_THESIS_G3
//#define DATASET_STUDY_THESIS_GALL


class Config {
public:
#ifdef DATASET_STUDY_PUBLICATION
    const std::string COMPARISONS_CSV = "PAPER_COMPARE.csv";
    const std::string COMPARISONS_STACKED_CSV = "PAPER_COMPARE_STACKED.csv";
    const std::string ML_JSON = "PAPER.json";
#endif

#ifdef DATASET_STUDY_THESIS_GALL
    const std::string COMPARISONS_CSV = "/home/jl/ramdisk/Labstudy_without_identities.csv2";
    const std::string COMPARISONS_STACKED_CSV = "/home/jl/ramdisk/Labstudy_all_stacks_wo_identities.csv";
    const std::string ML_JSON = "/home/jl/ramdisk/all.json";
#endif

#ifdef DATASET_STUDY_THESIS_G1
    const std::string COMPARISONS_CSV = "/home/jl/ramdisk/Labstudy_without_identities.csv2";
    const std::string COMPARISONS_STACKED_CSV = "/home/jl/ramdisk/Labstudy_all_stacks_wo_identities.csv";
    const std::string ML_JSON = "/home/jl/ramdisk/g1.json";
#endif

#ifdef DATASET_STUDY_THESIS_G2
    const std::string COMPARISONS_CSV = "/home/jl/ramdisk/Labstudy_without_identities.csv2";
    const std::string COMPARISONS_STACKED_CSV = "/home/jl/ramdisk/Labstudy_all_stacks_wo_identities.csv";
    const std::string ML_JSON = "/home/jl/ramdisk/g2.json";
#endif

#ifdef DATASET_STUDY_THESIS_G3
    const std::string COMPARISONS_CSV = "/home/jl/ramdisk/Labstudy_without_identities.csv2";
    const std::string COMPARISONS_STACKED_CSV = "/home/jl/ramdisk/Labstudy_all_stacks_wo_identities.csv";
    const std::string ML_JSON = "/home/jl/ramdisk/g3.json";
#endif

    const double LINEWIDTH_START = 1;

    const bool PLOT_FNR = true;
    const bool PLOT_FPR = true;
    const bool PLOT_TPR = false;
    const bool PLOT_TNR = false;
    const int XMIN = 0;
    const int XMAX = 20;
};



std::vector<std::string> str_split(const std::string &str, const std::string &delim = "\t") {
    std::vector<std::string> tokens;
    size_t prev = 0, pos = 0;
    do
    {
        pos = str.find(delim, prev);
        if (pos == std::string::npos) pos = str.length();
        std::string token = str.substr(prev, pos-prev);
        if (!token.empty()) tokens.push_back(token);
        prev = pos + delim.length();
    }
    while (pos < str.length() && prev < str.length());
    return tokens;
}

bool str_contains(const std::string &haystack, const std::string &needle){
    return haystack.find(needle) != std::string::npos;
}

std::string str_lower(std::string uppercaseString) {
    std::string lowercaseString = uppercaseString;
    std::transform(lowercaseString.begin(), lowercaseString.end(), lowercaseString.begin(), ::tolower);
    return lowercaseString;
}

void vec_remove_duplicates(std::vector<double> &v) {
    auto end = v.end();
    for (auto it = v.begin(); it != end; ++it) {
        end = std::remove(it + 1, end, *it);
    }

    v.erase(end, v.end());
}

inline std::string GT(std::string skeleton_id) {
    return skeleton_id.substr(0, 4);
}



enum Method {HausdorffDistance, ShapeContext, JaccardDist, HU1, HU2, HU3};
enum Outcome {TP, FP, TN, FN};



std::string Method2String (Method m) {
    switch(m){
        case HausdorffDistance:
            return "HausdorffDistance";
        case ShapeContext:
            return "ShapeContext";
        case JaccardDist:
            return "JaccardDist";
        case HU1:
            return "HU1";
        case HU2:
            return "HU2";
        case HU3:
            return "HU3";
    }
}
Method String2Method(std::string m) {
    if(str_lower(m) == str_lower("HausdorffDistance")) {
        return HausdorffDistance;
    } else if (str_lower(m) == str_lower("ShapeContext")) {
        return ShapeContext;
    } else if (str_lower(m) == str_lower("JaccardDist")) {
        return JaccardDist;
    } else if (str_lower(m) == str_lower("HU1")) {
        return HU1;
    } else if (str_lower(m) == str_lower("HU2")) {
        return HU2;
    } else if (str_lower(m) == str_lower("HU3")) {
        return HU3;
    } else {
        throw std::logic_error("String2Method not implemented for: " + m);
    }
}



class Comparison {
private:
    Method method;
    std::string a;
    std::string b;
    double value;

public:
    Comparison(Method method, std::string A, std::string B, double value) {
        this->method = method;
        this->a = A;
        this->b = B;
        this->value = value;
    }

    Comparison(Method method, std::string A, std::string B) {
        this->method = method;
        this->a = A;
        this->b = B;
        this->value = -1;
    }

    std::string get_id() {
        return Method2String(method) + "#" + a  + "#" + b;
    }

    double get_value() {
        return value;
    }
};




class Classification {
private:
    std::vector<std::pair<double, std::string>> classification;
    std::string a;

public:
    Classification(std::string A,std::vector<std::pair<double, std::string>> c){
        this->a = A;
        this->classification = c;
    }

    std::pair<double, std::string> get_top_prediction() {
        return classification.front();
    }

    std::string get_id() {
        return a;
    }
};



class CacheDB {
protected:
    std::unordered_map<std::string, Comparison>* hashmapShapeComparisonsPtr;
    std::unordered_map<std::string, Classification>* hashmapClassificationsPtr;
    bool stackedSkeletons;

    void add_entry_shapecomparison(Method M, std::string A, std::string B, double value) {
        // Store multiple addresses to same element in hashmap
        Comparison c1(M, A, B, value); // default filename
        Comparison c2(M, A.substr(0, 5), B.substr(0, 5), value); // for unstacked, e.g. `P28L6_frame0001.png_skeleton.png`
        Comparison c3(M, A.substr(8, 5), B.substr(8, 5), value); // for stacked, e.g. `thermal_P32R5_video.avi_skeletonstack.png`

        hashmapShapeComparisonsPtr->insert(std::make_pair(c1.get_id(), c1));
        hashmapShapeComparisonsPtr->insert(std::make_pair(c2.get_id(), c2));
        hashmapShapeComparisonsPtr->insert(std::make_pair(c3.get_id(), c3));
    }

    void load_csv(std::string file) {
        std::ifstream csvin(file);
        unsigned count = 0;
        for (std::string line; getline(csvin, line); ++count) {
            // split line by tab delimiters
            std::vector<std::string> spl = str_split(line);

            std::string method = spl[0];
            std::string skel_a = spl[1];
            std::string skel_b = spl[2];
            std::string value = spl[3];
            std::replace(value.begin(), value.end(), ',', '.');

            if(count == 0 && method == "Method") {  // skip title line in csv file
                continue;
            }

            try {
                add_entry_shapecomparison(String2Method(method), skel_a, skel_b, std::stod(value));
            } catch (std::invalid_argument) {
                std::cout << "Error: Invalid argument for std::stod: " << value << std::endl;
                exit(-1);
            }
        }
    }

    void add_entry_classification(std::string A, std::vector<std::pair<double, std::string>> c) {
        Classification classification(A, c);
        hashmapClassificationsPtr->insert(std::make_pair(classification.get_id(), classification));
    }

    /**
     * A quick and dirty json parser which populates `hashmapClassificationsPtr`.
     * Takes only well-formated JSONs.
     * @param file
     */
    void load_json(std::string file){
        std::ifstream csvin(file);
        unsigned long count = 0;
        const char sanitize[] = " \",";

        for (std::string line; getline(csvin, line); ++count) {
            if(str_contains(line, ": [")) {
                std::string GT = line;
                auto split = str_split(GT, "/");
                auto back = split.back();

                if(back.substr(0, 8) == "thermal_") {
                    GT = back.substr(8, 5);
                } else {
                    GT = back.substr(0, 5);
                }

                // In case of a bad underscore in the string, remove it, e.g. "P01L_2".
                if(str_contains(GT, "_")) {
                    GT = back.substr(8, 6);
                    GT.erase(std::remove(GT.begin(), GT.end(), '_'), GT.end());
                }

                // check for cases, e.g. "thermal_P09R_video.avi_frame0071.png_skeleton.png"
                if(GT.size() == 5 && GT.back() == 'v') { // GT must be 5 characters long
                    GT = split.end()[-2]; // get second last element from string split
                }

                getline(csvin, line); // skip
                std::vector<std::pair<double, std::string>> vals;

                for(int i = 0; i < 5; i++) {
                    std::string top_prediction_class, top_prediction_certainty;

                    getline(csvin, line); // Predidction
                    top_prediction_class = line;

                    getline(csvin, line); // Certainty
                    top_prediction_certainty = line;

                    getline(csvin, line); // Skip
                    getline(csvin, line); // Skip

                    for (unsigned int j = 0; j < std::strlen(sanitize); ++j) {
                        top_prediction_class.erase(std::remove(top_prediction_class.begin(), top_prediction_class.end(), sanitize[j]), top_prediction_class.end());
                        top_prediction_certainty.erase(std::remove(top_prediction_certainty.begin(), top_prediction_certainty.end(), sanitize[j]), top_prediction_certainty.end());
                    }

                    // In case less than five classifications were performed, skip the rest of this loop
                    if(top_prediction_certainty == "[" || top_prediction_certainty == "]"){
                        continue;
                    }

                    try {
                        vals.emplace_back(std::make_pair(std::stod(top_prediction_certainty), top_prediction_class));
                    } catch (std::invalid_argument) {
                        Config c;
                        std::cout << "Error: Invalid argument for std::stod -> " << top_prediction_certainty << std::endl;
                        std::cout << "Error parsing line " << count+2 << " of json: " << c.ML_JSON << std::endl;
                        exit(-1);
                    }
                }
                add_entry_classification(GT, vals);
            }
        }
    }

public:
    CacheDB (bool stackedSkeletons) {
        this->hashmapShapeComparisonsPtr = new std::unordered_map<std::string, Comparison>();
        this->hashmapClassificationsPtr  = new std::unordered_map<std::string, Classification>();
        this->stackedSkeletons = stackedSkeletons;
    }

    ~CacheDB() {
        delete hashmapShapeComparisonsPtr;
        delete hashmapClassificationsPtr;
    }

    /**
     * Compares two skeleton strings, which must have the same length.
     * @param M
     * @param A
     * @param B
     * @return
     */
    double compare(Method M, std::string A, std::string B) {
        if(A.size() != B.size()) {
            throw std::logic_error("Size of A != Size of B!");
        }

        Comparison id_helper = Comparison(M, A, B);
        return get_entry_sc(id_helper.get_id()).get_value();
    }

    void init_csv(std::string csv_file) {
        load_csv(csv_file);
    }

    void init_json(std::string json_file) {
        load_json(json_file);
    }

    Comparison get_entry_sc(std::string address) {
        try {
            return hashmapShapeComparisonsPtr->at(address);
        } catch (std::out_of_range &e) {
            std::cout << "Fatal error: cannot find \"" + address + "\" in hashmap at Classification::get_entry_sc!"
                      << std::endl;
            std::cout << "Please check your CSV. Caught exception:    " << e.what() << std::endl;
            exit(-1);
        }
    }

    Classification get_entry_ml(std::string skeleton) {
        try {
            return hashmapClassificationsPtr->at(skeleton);
        } catch (std::out_of_range &e) {
            std::cout << "Fatal error: cannot find \"" + skeleton + "\" in hashmap at Classification::get_entry_ml!"
            << std::endl;
            std::cout << "Please check your JSON. Caught exception:    " << e.what() << std::endl;
            exit(-1);
        }
    }

    bool isStacked() {
        return stackedSkeletons;
    }
};



class Comparator : public CacheDB {};



class Dataset {
public:
    std::vector<std::string> registrations;
    std::vector<std::string> testings;

    std::vector<std::string>* getRegistrationsPtr() {
        return &registrations;
    }

    std::vector<std::string>* getTestingsPtr() {
        return &testings;
    }

    bool registeredContain(std::string what) {
        for(auto reg : registrations) {
            if(GT(reg) == GT(what)) {
                return true; // if ground truth is found
            }
        }
        return false; // if ground truth is not found in registered
    }
};



class System {
    int variant;
    Comparator* cPtr;
    std::vector<std::string>* registered;
    double threshold_distance;
    Method method;

public:
    System(){};

    System(std::vector<std::string>* registeredPtr, Comparator* cPtr, double threshold, Method method, int v) {
        this->registered = registeredPtr;
        this->cPtr = cPtr;
        this->threshold_distance = threshold;
        this->variant = v;
        this->method = method;
    }


    /**
     * Authenticates the input against the system depending on the variant.
     * @param unknown_input An unknown input. The string is only required for the lut. No string comparison is applied.
     * @return Name of the identified user or empty string, if user is rejected.
     */
    std::string try_auth(std::string unknown_input){
        std::string candidate_phase1;
        double closestDistance = std::numeric_limits<double>::max();

        std::string candidate_phase2;
        double certainty = -1;

        // Determine closest candidate in phase 1
        for(auto r : *registered){
            double current_distance = cPtr->compare(method, r, unknown_input);
            if(current_distance < closestDistance) {
                closestDistance = current_distance;
                candidate_phase1 = r;
            }
        }

        // Determine image classification
        auto classification = cPtr->get_entry_ml(unknown_input).get_top_prediction();
        candidate_phase2 = classification.second;
        certainty = classification.first;

        candidate_phase1 = GT(candidate_phase1);

        // VARIANT SWITCH
        switch(variant) {
            case 1: // VARIANT 1
                if(closestDistance > threshold_distance) {
                    return "";
                } else {
                    return candidate_phase1;
                }
            case 2: // VARIANT 2
                if(closestDistance > threshold_distance) {
                    return "";
                } else {
                    return candidate_phase2;
                }
            case 3: // VARIANT 3
                if(closestDistance > threshold_distance || certainty < 75) {
                    return "";
                } else {
                    return candidate_phase2;
                }
            case 4: // VARIANT 4
                // ML returns only GT of skeleton.
                // For the comparison, we need to match it onto the one concrete 5-digits skeleton number.
                // This can be done, as the system knows the IDs of its registered skeletons.
                for(auto s : *this->registered) {
                    if(str_contains(s, candidate_phase2)) {
                        candidate_phase2 = s;
                    }
                }
                closestDistance = cPtr->compare(method, unknown_input, candidate_phase2);

                if(closestDistance > threshold_distance) {
                    return "";
                } else {
                    return GT(candidate_phase2);
                }
            default:
                throw std::logic_error("Unknown variant called in System. Allowed are int(1 to 4).");
        }
    }

    int get_variant() {
        return variant;
    }

    Method getMethod() {
        return method;
    }

    double get_threshold() {
        return threshold_distance;
    }

    bool uses_stacked_skeletons() {
        return cPtr->isStacked();
    }

    std::string uses_stacked_skeletons_str() {
        return this->uses_stacked_skeletons() ? "true" : "false";
    }
};



class SystemEvaluator {
private:
    unsigned tp = 0, fp = 0, tn = 0, fn = 0;
    double tpr = 0, fpr = 0, tnr = 0, fnr = 0;
    double ppv = 0, acc = 0;
    unsigned sum = 0;
    double threshold = 0;

    System system;

    bool correctIdNecessary = false;
    bool stackedSkeletons = false;

public:
    SystemEvaluator() {}

    SystemEvaluator(System system, bool correctIdNecessary, bool stackedSkeletons) {
        this->system = system;
        this->correctIdNecessary = correctIdNecessary;
        this->threshold = system.get_threshold();
    }

    void evaluate_system(Dataset* dsPtr) {
        for(const auto& test : dsPtr->testings) {
            std::string systemOutput = system.try_auth(test);
            if (correctIdNecessary) {
                if(! systemOutput.empty()) {
                    if(GT(test) == GT(systemOutput)) {
                        ++tp;
                    } else {
                        ++fp;
                    }
                } else {
                    if(dsPtr->registeredContain(test)) {
                        ++fn;
                    } else {
                        ++tn;
                    }
                }
            } else {
                if(! systemOutput.empty()) {
                    if(dsPtr->registeredContain(test)) {
                        ++tp;
                    } else {
                        ++fp;
                    }
                } else {
                    if(dsPtr->registeredContain(test)) {
                        ++fn;
                    } else {
                        ++tn;
                    }
                }
            }
        }
        update();
    }

    unsigned getTP() {
        return tp;
    }

    unsigned getFP() {
        return fp;
    }

    unsigned getTN() {
        return tn;
    }

    unsigned getFN() {
        return fn;
    }

    double getTPR() {
        if(tp+fn == 0) {
            return -1;
        }

        update();
        return tpr;
    }

    double getFPR() {
        if(fp+tn == 0) {
            return -1;
        }

        update();
        return fpr;
    }

    double getTNR() {
        if(tn+fp == 0) {
            return -1;
        }

        update();
        return tnr;
    }

    double getFNR() {
        if(fn+tp == 0) {
            return -1;
        }

        update();
        return fnr;
    }

    double getPPV() {
        update();
        return ppv;
    }

    double getACC() {
        update();
        return acc;
    }

    double getOptimizationMetric() {
        return getACC();
    }

    void reset() {
        tp = fp = tn = fn = 0;
        update();
    }

    System* getSystem() {
        return &system;
    }

private:
    inline void update() {
        this->tpr = tp / (double) (tp + fn);
        this->fpr = fp / (double) (fp + tn);
        this->tnr = tn / (double) (tn + fp);
        this->fnr = fn / (double) (fn + tp);
        this->sum = tp + fp + tn + fn;
        this->ppv = tp / (double) (tp+fp);
        this->acc = (tp + tn) / (double) (tp + tn + fp + fn);
    }
};



class Optimizer {
public:
    /**
     * This method does generate all possible experiments and returns two vectors.
     * The first vector contains the cases with an exact identification.
     * The second vector contains the cases without an exact identification.
     * @param m Method
     * @param ds Dataset
     * @param cPtr Comparator (e.g. CacheDB)
     * @param variant Which variant
     * @return A pair of all SystemEvaluator (first: strict, second: lax).
     */
    std::pair<std::vector<SystemEvaluator>, std::vector<SystemEvaluator>> generateAllExperiments(Method m,
                                                                                                 Dataset *ds,
                                                                                                 Comparator *cPtr,
                                                                                                 int variant) {
        auto testsPtr = ds->getTestingsPtr();
        auto regsPtr  = ds->getRegistrationsPtr();

        std::vector<double> possibleThresholds = { 0 };
        for(const auto& t : *testsPtr) {
            for(const auto& r : *regsPtr) {
                Comparison c(m, t, r);
                std::string id = c.get_id();
                double distance = cPtr->get_entry_sc(id).get_value();
                possibleThresholds.push_back(distance);
            }
        }
        std::sort(possibleThresholds.begin(), possibleThresholds.end());
        vec_remove_duplicates(possibleThresholds);

        std::vector<SystemEvaluator> vec_se_strict;
        std::vector<SystemEvaluator> vec_se_lax;

        for(double d : possibleThresholds) {
            System system = System(regsPtr, cPtr, d, m, variant);
            SystemEvaluator se_strict = SystemEvaluator(system, true, cPtr->isStacked());
            SystemEvaluator se_lax    = SystemEvaluator(system, false, cPtr->isStacked());

            se_strict.evaluate_system(ds);
            se_lax.evaluate_system(ds);

            vec_se_strict.push_back(se_strict);
            vec_se_lax.push_back(se_lax);
        }

        return std::make_pair(vec_se_strict, vec_se_lax);
    }


    /**
     * For a given method and dataset, the optimal threshold value is determined.
     * This method internally uses `generateAllExperiments`.
     * The first in the returned pair is the optimal experiment with an exact identification.
     * The second in the returned pair is without an exact identification.
     * @param m Method
     * @param ds Dataset
     * @param cPtr Comparator (e.g. CacheDB)
     * @param variant Which variant
     * @return A pair of the best SystemEvaluator (first: strict, second: lax).
     */
    std::pair<SystemEvaluator, SystemEvaluator> optimize(Method m, Dataset* ds, Comparator* cPtr, int variant) {
        auto pair = this->generateAllExperiments(m, ds, cPtr, variant);
        auto vec_se_strict = pair.first;
        auto vec_se_lax    = pair.second;

        // Find best PPV
        double ppv_se_strict = -1, ppv_se_lax = -1;
        SystemEvaluator best_se_strict;
        SystemEvaluator best_se_lax;

        for(auto se : vec_se_strict) {
            if(se.getOptimizationMetric() >= ppv_se_strict) {
                ppv_se_strict = se.getOptimizationMetric();
                best_se_strict = se;
            }
        }
        for(auto se : vec_se_lax) {
            if(se.getOptimizationMetric() >= ppv_se_lax) {
                ppv_se_lax = se.getOptimizationMetric();
                best_se_lax = se;
            }
        }

        return std::make_pair(best_se_strict, best_se_lax);
    }
};



class ExperimentClusters {
private:
    Dataset* g1;
    Dataset* g2;
    Dataset* g3;
    Dataset* all;
    Dataset* pub;

public:
    ExperimentClusters() {
        this->g1 = new Dataset();
        this->g2 = new Dataset();
        this->g3 = new Dataset();
        this->all = new Dataset();
        this->pub = new Dataset();

        pub->registrations.emplace_back("P20R5");
        pub->registrations.emplace_back("P22L5");
        pub->registrations.emplace_back("P24R5");
        pub->registrations.emplace_back("P26L5");
        pub->registrations.emplace_back("P27L6");
        pub->registrations.emplace_back("P28L6");
        pub->registrations.emplace_back("P29L6");
        pub->registrations.emplace_back("P31R6");
        pub->registrations.emplace_back("P34L5");
        pub->registrations.emplace_back("P35L5");
        pub->registrations.emplace_back("P37L5");
        pub->registrations.emplace_back("P38L5");
        pub->testings.emplace_back("P24R6");
        pub->testings.emplace_back("P26L6");
        pub->testings.emplace_back("P27L5");
        pub->testings.emplace_back("P28L5");
        pub->testings.emplace_back("P29L5");
        pub->testings.emplace_back("P35L6");
        pub->testings.emplace_back("P22R6");
        pub->testings.emplace_back("P38R6");
        pub->testings.emplace_back("P34R6");
        pub->testings.emplace_back("P31L6");
        pub->testings.emplace_back("P27R6");
        pub->testings.emplace_back("P28R6");

        // ------------------------------------ g1
    
        g1->registrations.emplace_back("P02L2");
        g1->registrations.emplace_back("P06L2");
        g1->testings.emplace_back("P02L4");
        g1->testings.emplace_back("P02R2");
        g1->testings.emplace_back("P02R4");
        g1->testings.emplace_back("P06L4");
        g1->testings.emplace_back("P06R2");
        g1->testings.emplace_back("P06R4");
    
        // ------------------------------------ g2
        for(auto s : g1->registrations) {
            g2->registrations.push_back(s);
        }
        g2->registrations.emplace_back("P01R2");
        g2->registrations.emplace_back("P08R2");
    
        for(auto s : g1->testings) {
            g2->testings.push_back(s);
        }
        g2->testings.emplace_back("P01R4");
        g2->testings.emplace_back("P08L2");
        g2->testings.emplace_back("P08L4");
        g2->testings.emplace_back("P08R4");
        g2->testings.emplace_back("P09L2");
        g2->testings.emplace_back("P09L4");
        g2->testings.emplace_back("P09R1");
        g2->testings.emplace_back("P09R4");
    
        // ------------------------------------ g3
    
        for(auto s : g2->registrations) {
            g3->registrations.push_back(s);
        }
    
        g3->registrations.emplace_back("P05L2");
        g3->registrations.emplace_back("P05R2");
        g3->registrations.emplace_back("P11L2");
    
        for(auto s : g2->testings) {
            g3->testings.push_back(s);
        }
    
        g3->testings.emplace_back("P01L2");
        g3->testings.emplace_back("P01L4");
        g3->testings.emplace_back("P05L4");
        g3->testings.emplace_back("P05R4");
        g3->testings.emplace_back("P07R2");
        g3->testings.emplace_back("P07R4");
        g3->testings.emplace_back("P11L4");
        g3->testings.emplace_back("P11R2");
        g3->testings.emplace_back("P11R3");
    
        // ------------------------------------ g3
    
        for(auto s : g3->registrations) {
            all->registrations.push_back(s);
        }
    
        all->registrations.emplace_back("P03R2");
        all->registrations.emplace_back("P04R1");
        all->registrations.emplace_back("P07L2");
        all->registrations.emplace_back("P12L1");
        all->registrations.emplace_back("P12R1");
        all->registrations.emplace_back("P13L1");
        all->registrations.emplace_back("P14R1");
        all->registrations.emplace_back("P15R1");
    
        for(auto s : g3->testings) {
            all->testings.push_back(s);
        }
    
        all->testings.emplace_back("P03L1");
        all->testings.emplace_back("P03L3");
        all->testings.emplace_back("P03R3");
        all->testings.emplace_back("P04L1");
        all->testings.emplace_back("P04L3");
        all->testings.emplace_back("P04R3");
        all->testings.emplace_back("P07L3");
        all->testings.emplace_back("P10L1");
        all->testings.emplace_back("P10L4");
        all->testings.emplace_back("P10R1");
        all->testings.emplace_back("P10R4");
        all->testings.emplace_back("P12L3");
        all->testings.emplace_back("P12R3");
        all->testings.emplace_back("P13L4");
        all->testings.emplace_back("P13R1");
        all->testings.emplace_back("P13R3");
        all->testings.emplace_back("P14L1");
        all->testings.emplace_back("P14L3");
        all->testings.emplace_back("P14R3");
        all->testings.emplace_back("P15L1");
        all->testings.emplace_back("P15L4");
        all->testings.emplace_back("P15R3");
        all->testings.emplace_back("P16L1");
        all->testings.emplace_back("P16L3");
        all->testings.emplace_back("P16R1");
        all->testings.emplace_back("P16R4");
    }

    Dataset* getCurrentGroup() {
#ifdef DATASET_STUDY_PUBLICATION
        return pub;
#endif

#ifdef DATASET_STUDY_THESIS_G1
        return g1;
#endif

#ifdef DATASET_STUDY_THESIS_G2
        return g2;
#endif

#ifdef DATASET_STUDY_THESIS_G3
        return g3;
#endif

#ifdef DATASET_STUDY_THESIS_GALL
        return all;
#endif
    }
};



class Printer {
public:
    Printer() {}

    void print_csv_header() {
        std::cout << "Variant\t";
        std::cout << "Shapecomparison-method\t";
        std::cout << "Stacked\t";
        std::cout << "Exact ID?\t";
        std::cout << "TP\t";
        std::cout << "FP\t";
        std::cout << "TN\t";
        std::cout << "FN\t";
        std::cout << "TPR\t";
        std::cout << "FPR\t";
        std::cout << "TNR\t";
        std::cout << "FNR\t";
        std::cout << "PPV\t";
        std::cout << "ACC\t";
        std::cout << "Distance-Threshold" << std::endl;
    }

    void print_csv(std::vector<std::pair<SystemEvaluator, SystemEvaluator>> vector) {
        for(auto v : vector) {
            auto se = v.first;
            std::cout << se.getSystem()->get_variant() << "\t";
            std::cout << Method2String(se.getSystem()->getMethod()) << "\t";
            std::cout << se.getSystem()->uses_stacked_skeletons_str() << "\t";
            std::cout << "true" << "\t";
            std::cout << se.getTP() << "\t";
            std::cout << se.getFP() << "\t";
            std::cout << se.getTN() << "\t";
            std::cout << se.getFN() << "\t";
            std::cout << se.getTPR() << "\t";
            std::cout << se.getFPR() << "\t";
            std::cout << se.getTNR() << "\t";
            std::cout << se.getFNR() << "\t";
            std::cout << se.getPPV() << "\t";
            std::cout << se.getACC() << "\t";
            std::cout << se.getSystem()->get_threshold() << "\t" << std::endl;

            se = v.second;
            std::cout << se.getSystem()->get_variant() << "\t";
            std::cout << Method2String(se.getSystem()->getMethod()) << "\t";
            std::cout << se.getSystem()->uses_stacked_skeletons_str() << "\t";
            std::cout << "false" << "\t";
            std::cout << se.getTP() << "\t";
            std::cout << se.getFP() << "\t";
            std::cout << se.getTN() << "\t";
            std::cout << se.getFN() << "\t";
            std::cout << se.getTPR() << "\t";
            std::cout << se.getFPR() << "\t";
            std::cout << se.getTNR() << "\t";
            std::cout << se.getFNR() << "\t";
            std::cout << se.getPPV() << "\t";
            std::cout << se.getACC() << "\t";
            std::cout << se.getSystem()->get_threshold() << "\t" << std::endl;
        }
    }

    void plot_far_fnr_eer(std::vector<std::vector<SystemEvaluator>> vectorOfExperimentVectors, std::vector<std::string> legends) {
        Config config;

        for(auto vec : vectorOfExperimentVectors) {
            std::cout << "% FAR_FNR_EER PLOT \n"
                         "\\begin{tikzpicture}\n"
                         "\\begin{axis}[\n"
                         //"    xmode=log,\n"
                         //"    log ticks with fixed point,\n"
                         "    xmin=" << config.XMIN << ",\n"
                         "    xmax=" << config.XMAX << ",\n"
                         "    width=1\\textwidth,\n"
                         "    height=6cm,\n"
                         "\txlabel={" << Method2String(vec.front().getSystem()->getMethod()) << "-distance},\n"
                                             "\tylabel={Rate},\n"
                                             "\tytick={0,0.2,0.4,0.6,0.8,1},\n"
                                             "\t%ytick={0.0,0.2,0.4,0.6,0.8,1.0}\n"
                                             "\t%grid=major,\n"
                                             "    %major grid style={line width=.2pt,draw=gray!30},  \n"
                                             "    ymajorgrids,\n"
                                             "    legend style={at={(0.813,-.10)},anchor=north west}, \n"
                                             "]" << std::endl;

            if (config.PLOT_FPR) {
                std::cout << "\\addplot[color=red,mark=,const plot] coordinates { % FPR" << std::endl;
                for (auto e : vec) {
                    std::cout << '(' << e.getSystem()->get_threshold() << ',' << e.getFPR() << ')' << std::endl;
                }
                std::cout << "};" << std::endl;
            }

            if (config.PLOT_FNR) {
                std::cout << "\\addplot[color=blue,mark=,const plot] coordinates { % FNR" << std::endl;
                for (auto e : vec) {
                    std::cout << '(' << e.getSystem()->get_threshold() << ',' << e.getFNR() << ')' << std::endl;
                }
                std::cout << "};" << std::endl;
            }

            if (config.PLOT_TPR) {
                std::cout << "\\addplot[color=black!60!green,mark=,const plot] coordinates { % TPR" << std::endl;
                for (auto e : vec) {
                    std::cout << '(' << e.getSystem()->get_threshold() << ',' << e.getTPR() << ')' << std::endl;
                }
                std::cout << "};" << std::endl;
            }

            if (config.PLOT_TNR) {
                std::cout << "\\addplot[color=black,mark=,const plot] coordinates { % TNR" << std::endl;
                for (auto e : vec) {
                    std::cout << '(' << e.getSystem()->get_threshold() << ',' << e.getTNR() << ')' << std::endl;
                }
                std::cout << "};" << std::endl;
            }

            std::cout << "\\legend{";

            if (config.PLOT_FPR) {
                std::cout << "\\footnotesize FPR (FAR),";
            }

            if (config.PLOT_FNR) {
                std::cout << "\\footnotesize FNR (FRR),";
            }

            if (config.PLOT_TPR) {
                std::cout << "\\footnotesize TPR,";
            }

            if (config.PLOT_TNR) {
                std::cout << "\\footnotesize TNR,";
            }

            std::cout << "}" << std::endl << std::endl;

            std::cout << "\\end{axis}\n"
                         "\\end{tikzpicture}" << std::endl << std::endl << std::endl;
        }
    }

    void plot_roc(std::vector<std::vector<SystemEvaluator>> vectorOfExperimentVectors, std::vector<std::string> legends) {
        if(vectorOfExperimentVectors.size() != legends.size()) {
            std::cout << "% WARNING: Legends-size does not match vectorOfExperimentVectors in plot!" << std::endl;
            std::cout << "% WARNING: One or more legends may be missing. " << std::endl << std::endl;
        }

        const std::vector<std::string> clist = {"red", "green", "blue", "cyan", "magenta", "yellow", "black", "gray",
                                                "brown", "lime", "olive", "orange", "purple", "teal", "violet"};
        int col = 0;

        // Print beginning
        std::cout << "% ROC PLOT\n"
                     "\\begin{tikzpicture}\n"
                     "\\begin{axis}[\n"
                     "    width=10cm,\n"
                     "    height=10cm,\n"
                     "\txlabel={False positive rate},\n"
                     "\tylabel={True positive rate},\n"
                     "\tytick={0,0.2,0.4,0.6,0.8,1},\n"
                     "\txtick={0,0.2,0.4,0.6,0.8,1},\n"
                     "\t%grid=major,\n"
                     "    %major grid style={line width=.2pt,draw=gray!30},  \n"
                     "    %ymajorgrids,\n"
                     "    legend cell align={left},\n"
                     "    legend pos=outer north east,\n"
                     "]\n"
                     "\n";

        // Print dashed line
        std::cout << "\\addplot[mark=, black, dashed] coordinates {\n"
                     "(0,0)\n"
                     "(1,1)\n"
                     "};" << std::endl;

        Config config;
        double linewidth = config.LINEWIDTH_START * (double) (vectorOfExperimentVectors.size() + 1);

        for(auto vector : vectorOfExperimentVectors) {
            double avg_dist = 0;
            linewidth -= 0.33;
            std::cout << "\\addplot[const plot,mark=, " << clist[col++ % clist.size()] << ", line width=" << linewidth << "pt] coordinates {\n";
            std::cout << "(0,0)\n";
            for(auto exp : vector) {
                std::cout << "(";
                std::cout << exp.getFPR();
                std::cout << ",";
                std::cout << exp.getTPR();
                std::cout << ")" << std::endl;
                avg_dist += std::sqrt(std::pow((exp.getFPR() -0), 2) + std::pow((exp.getTPR() -1), 2));
            }
            std::cout << "(1,1)\n";
            std::cout << "};" << "% avg euclidean dist to (0,1): " << avg_dist / vector.size() << std::endl << std::endl;
        }

        // print legend
        std::cout << "\\legend{,";  // place comma to skip first dashed line in legend
        for(auto s : legends) {
            std::cout << "\\scriptsize " << s << ",";
        }
        std::cout << "}\n"
                     "\\end{axis}\n"
                     "\\end{tikzpicture}" << std::endl << std::endl << std::endl;
    }

    /**
     * Prints three empty lines on stdout.
     */
    void print_empty() {
        std::cout << std::endl << std::endl << std::endl;
    }
};


/**
 * Handler for SIGSEGV, SIGABRT to print stacktraces.
 * @param sig
 */
void handler(int sig) {
    void *array[10];
    size_t size;

    // get void*'s for all entries on the stack
    size = backtrace(array, 20);

    // print out all the frames to stderr
    fprintf(stderr, "Error: signal %d:\n", sig);
    backtrace_symbols_fd(array, size, STDERR_FILENO);
    exit(1);
}



int main(int argc, char *argv[]) {
    signal(SIGABRT, handler);
    signal(SIGSEGV, handler);

    // Read only objects (config and groupings):
    Config config;
    ExperimentClusters experimentCluster;

    // Init cacheDB for data handling. We expect, that a pre-calculated CSV-file exists.
    // This comparator can be exchanged with `Image.h´.
    // It essentially only is a LUT for the pre-calculated comparison values.
    CacheDB comparator = CacheDB(false);
    CacheDB comparatorStacked = CacheDB(true);
    comparator.init_csv(config.COMPARISONS_CSV);
    comparatorStacked.init_csv(config.COMPARISONS_STACKED_CSV);
    comparator.init_json(config.ML_JSON);
    comparatorStacked.init_json(config.ML_JSON);

    // Find best thresholds based on the PPV:
    Optimizer optimizer;
    std::vector<std::pair<SystemEvaluator, SystemEvaluator>> variant1, variant2, variant3, variant4;
    std::vector<std::pair<SystemEvaluator, SystemEvaluator>> variant1stacked, variant2stacked, variant3stacked, variant4stacked;

    // Variant 1:
    int variant = 1;
    for(int i = 0; i < 6; i++) {
        Method m = static_cast<Method>(i);
        auto o        = optimizer.optimize(m, experimentCluster.getCurrentGroup(), (Comparator*) &comparator, variant);
        auto ostacked = optimizer.optimize(m, experimentCluster.getCurrentGroup(), (Comparator*) &comparatorStacked, variant);
        variant1.push_back(o);
        variant1stacked.push_back(ostacked);
    }

    // Variant 2:
    variant = 2;
    for(int i = 0; i < 6; i++) {
        Method m = static_cast<Method>(i);
        auto o        = optimizer.optimize(m, experimentCluster.getCurrentGroup(), (Comparator*) &comparator, variant);
        auto ostacked = optimizer.optimize(m, experimentCluster.getCurrentGroup(), (Comparator*) &comparatorStacked, variant);
        variant2.push_back(o);
        variant2stacked.push_back(ostacked);
    }

    // Variant 3:
    variant = 3;
    for(int i = 0; i < 6; i++) {
        Method m = static_cast<Method>(i);
        auto o        = optimizer.optimize(m, experimentCluster.getCurrentGroup(), (Comparator*) &comparator, variant);
        auto ostacked = optimizer.optimize(m, experimentCluster.getCurrentGroup(), (Comparator*) &comparatorStacked, variant);
        variant3.push_back(o);
        variant3stacked.push_back(ostacked);
    }

    // Variant 4:
    variant = 4;
    for(int i = 0; i < 6; i++) {
        Method m = static_cast<Method>(i);
        auto o = optimizer.optimize(m, experimentCluster.getCurrentGroup(), (Comparator*) &comparator, variant);
        auto ostacked = optimizer.optimize(m, experimentCluster.getCurrentGroup(), (Comparator*) &comparatorStacked, variant);
        variant4.push_back(o);
        variant4stacked.push_back(ostacked);
    }

    // Print CSV Tables:
    Printer p;
    p.print_csv_header();
    p.print_csv(variant1);
    p.print_csv(variant2);
    p.print_csv(variant3);
    p.print_csv(variant4);
    p.print_csv(variant1stacked);
    p.print_csv(variant2stacked);
    p.print_csv(variant3stacked);
    p.print_csv(variant4stacked);
    p.print_empty();

    // Print LaTeX-Table:
    //p.print_latex_table_header();
    //p.print_latex_table_entry();



    // Print ROC:
    std::vector<std::vector<SystemEvaluator>> plot1;
    std::vector<std::string> legend = {
            "III./$\\Delta$ Hausdorff (non-exact identification)",
            //"III./$\\Delta$ Hausdorff (exact identification)"
    };

    /*
    plot1.push_back(optimizer.generateAllExperiments(HausdorffDistance,
                                                     experimentCluster.getCurrentGroup(),
                                                     (Comparator*) &comparatorStacked,
                                                     3).second); // non-exact id */
    plot1.push_back(optimizer.generateAllExperiments(HausdorffDistance,
                                                     experimentCluster.getCurrentGroup(),
                                                     (Comparator*) &comparatorStacked,
                                                     4).second);
    plot1.push_back(optimizer.generateAllExperiments(HausdorffDistance,
                                                     experimentCluster.getCurrentGroup(),
                                                     (Comparator*) &comparatorStacked,
                                                     3).second);
    plot1.push_back(optimizer.generateAllExperiments(HausdorffDistance,
                                                     experimentCluster.getCurrentGroup(),
                                                     (Comparator*) &comparatorStacked,
                                                     1).second);


    p.plot_roc(plot1, legend);

    p.print_empty();

    // Print FAR FNR EER:
    p.plot_far_fnr_eer(plot1, legend);

    exit(0);
}