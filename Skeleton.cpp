#include <random>
#include <cv.hpp>

//
// Created by jl on 18.11.18.
//


#include "Skeleton.h"
#include "Voronoi.h"


Skeleton::Skeleton(cv::Mat *mat, std::string name) {
    if(mat->empty()) {
        throw std::logic_error("Exception: Skeleton::Skeleton() constructed a Skeleton from an empty Mat. (error 01)");
    } else {
        mat->copyTo(m_skeleton);
    }
}

Skeleton::Skeleton(std::string path) {
    cv::imread(path, cv::IMREAD_GRAYSCALE).copyTo(m_skeleton);
}

cv::Mat *Skeleton::get_mat() {
    if(m_skeleton.empty()) {
        throw std::logic_error("Exception: Skeleton::get_mat() would return an empty mat. (error 02)");
    } else {
        return &m_skeleton;
    }
}

void Skeleton::set_skeleton(cv::Mat *mat, std::string name) {
    if(mat->empty()) {
        throw std::logic_error("Exception: Skeleton::set_skeleton() set a Skeleton to an empty Mat. (error 03)");
    } else {
        mat->copyTo(m_skeleton);
    }
}

double Skeleton::compute_distance(Skeleton *other_skeleton_ptr, eDistanceCalculationAlgorithm algorithm, bool shuffle) {
    return compute_distance(this, other_skeleton_ptr, algorithm, shuffle);
}

double Skeleton::compute_distance(Skeleton *first_skeleton_ptr,
                                  Skeleton *second_skeleton_ptr,
                                  eDistanceCalculationAlgorithm algorithm,
                                  bool shuffle) {
    // Get pointers to Mat
    cv::Mat* mat1 = first_skeleton_ptr->get_mat();
    cv::Mat* mat2 = second_skeleton_ptr->get_mat();

    // Initialize Hausdorff:
    int distanceFlag = cv::NORM_L2;
    float rankProp = 0.6;
    cv::Ptr<cv::HausdorffDistanceExtractor> hdeptr = cv::createHausdorffDistanceExtractor(distanceFlag, rankProp);

    // Initialize ShapeContext:
    cv::Ptr<cv::ShapeContextDistanceExtractor> sdeptr = cv::createShapeContextDistanceExtractor();

    // extract contours:
    std::vector<std::vector<cv::Point>> contours1, contours2;
    std::vector<cv::Vec4i> hierarchy1, hierarchy2;

    cv::findContours(*mat1, contours1, hierarchy1, cv::RETR_LIST, cv::CHAIN_APPROX_NONE);
    cv::findContours(*mat2, contours2, hierarchy2, cv::RETR_LIST, cv::CHAIN_APPROX_NONE);

    // merge all contours into one contour (in case of disconnected skeletons):
    std::vector<cv::Point> merged_contour1, merged_contour2;
    const int THRESHOLD_MINIMUM_POINTS_IN_CONTOUR = 10;
    for(auto vec1 : contours1) {
        if(vec1.size() > THRESHOLD_MINIMUM_POINTS_IN_CONTOUR) {
            // following commented line can cause scegfault
            // merged_contour1.insert(merged_contour2.end(), vec1.begin(), vec1.end());
            for (auto pt1 : vec1) {
                merged_contour1.push_back(pt1);
            }
        }
    }
    for(auto vec2: contours2) {
        if(vec2.size() > THRESHOLD_MINIMUM_POINTS_IN_CONTOUR) {
            // merged_contour2.insert(merged_contour2.end(), vec2.begin(), vec2.end());
            for (auto pt2 : vec2) {
                merged_contour2.push_back(pt2);
            }
        }
    }

    if(shuffle) {
        auto rng = std::default_random_engine{};
        std::shuffle(std::begin(merged_contour1), std::end(merged_contour1), rng);
        std::shuffle(std::begin(merged_contour2), std::end(merged_contour2), rng);
    }

    /*
    // test
    cv::Mat tmp;
    mat1->copyTo(tmp);
    std::vector<std::vector<cv::Point>> mytmp;
    mytmp.push_back(merged_contour1);
    cv::drawContours(tmp, mytmp, 0, 255);
    cv::imshow("tmp", tmp);
    while(cv::waitKey(1) != int('q'));
    */

    double distance = -1;
    switch(algorithm) {
        case eHausdorff:
            distance = hdeptr->computeDistance(merged_contour1, merged_contour2);
            return distance;
        case eShapeContext:
            distance = sdeptr->computeDistance(merged_contour1, merged_contour2);
            return distance;
        case eBoth:
            distance = hdeptr->computeDistance(merged_contour1, merged_contour2);
            distance += sdeptr->computeDistance(merged_contour1, merged_contour2);
            return distance / 2;
        case eJaccardDistance:
            return jaccard_index(mat1, mat2);
        case eHuMoments1:
            return cv::matchShapes(*mat1, *mat2, cv::CONTOURS_MATCH_I1, 0);
        case eHuMoments2:
            return cv::matchShapes(*mat1, *mat2, cv::CONTOURS_MATCH_I2, 0);
        case eHuMoments3:
            return cv::matchShapes(*mat1, *mat2, cv::CONTOURS_MATCH_I3, 0);
        default:
            throw std::logic_error("Skeleton::compute_distance was called with invalid algorithm (error 08)");
    }
}



double Skeleton::jaccard_index(const cv::Mat *m1_ptr, const cv::Mat *m2_ptr) {
    cv::Mat m1, m2;
    m1_ptr->copyTo(m1);
    m2_ptr->copyTo(m2);
    cv::Mat m_out = cv::Mat::zeros(m1.size(), CV_8UC1);

    unsigned long long int pixel_count_m1 = 0,
            pixel_count_m2 = 0,
            pixel_count_m1_dilated = 0,
            pixel_count_m2_dilated = 0,
            pixel_count_m_out = 0,
            pixel_count_m_out_dilated = 0;

    for (int i = 0; i < m1.rows; i++) { // 288
        for (int j = 0; j < m1.cols; j++) { // 382
            if (m1.at<uchar>(i, j) == 255) {
                ++pixel_count_m1;
            }
            if (m2.at<uchar>(i, j) == 255) {
                ++pixel_count_m2;
            }
        }
    }

    cv::dilate(m1, m1, cv::getStructuringElement(cv::MORPH_CROSS, cv::Size(5, 5)));
    cv::dilate(m2, m2, cv::getStructuringElement(cv::MORPH_CROSS, cv::Size(5, 5)));

    for (int i = 0; i < m1.rows; i++) { // 288
        for (int j = 0; j < m1.cols; j++) { // 382
            if (m1.at<uchar>(i, j) == 255) {
                ++pixel_count_m1_dilated;
            }

            if (m2.at<uchar>(i, j) == 255) {
                ++pixel_count_m2_dilated;
            }

            if (m1.at<uchar>(i, j) == 255 && m2.at<uchar>(i, j) == 255) {
                m_out.at<uchar>(i, j) = 255;
                ++pixel_count_m_out_dilated;
            }
        }
    }


    Skeleton s_out(&m_out);
    s_out.thin();
    pixel_count_m_out = s_out.get_white_pixel_count();

    /*
    cv::imshow("m1", m1);
    cv::imshow("m2", m2);
    cv::imshow("m_out", *s_out.get_mat());
    while(cv::waitKey(1) != int('q'));
    */

    double result = (double) pixel_count_m_out / (double) pixel_count_m1;

    // sanity checks
    if(result < 0) {
        result = 0;
    }
    if(result > 1) {
        result = 1;
    }

    return 1 - result;
}

void Skeleton::thin(std::string algo) {
    VoronoiThinner vt;

    if(! vt.thin(*this->get_mat(), algo, false)) {
        throw std::logic_error("Code error - vt.thin() was called with an unknown algorithm in file Skeleton.cpp. (error 20)");
    }

    vt.get_skeleton().copyTo(this->m_skeleton);
}

unsigned long Skeleton::get_white_pixel_count() {
    unsigned cnt = 0;
    for (int i = 0; i < m_skeleton.rows; i++) { // 288
        for (int j = 0; j < m_skeleton.cols; j++) { // 382
            if (m_skeleton.at<uchar>(i, j) == 255) {
                ++cnt;
            }
        }
    }
    return cnt;
}

